package com.lyttleton.glimmer.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.lyttleton.glimmer.R;

public class BaseActivity extends ActionBarActivity {

    private TextView labelTv, mutualFriendsCountTv;
    private View backBt, mutualFriendsIv;
    private ImageView imageIv;

    public static void blockOrientationChange(Activity activity){
        if (activity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        else
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    public static void restoreOrientationChange(Activity activity){
        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
    }

    public static void sendSMS(Activity activity, String message){
        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
        smsIntent.setData(Uri.parse("sms:"));
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.putExtra("sms_body", message);
        activity.startActivity(smsIntent);
    }

    @Override
    public void setContentView(int layoutResID) {
        setContentView(getLayoutInflater().inflate(layoutResID, null));
    }

    @Override
    public void setContentView(View view) {
        ViewGroup fullLayout = (ViewGroup) getLayoutInflater().inflate(R.layout.base_activity,
                null);
        FrameLayout contentLayout = (FrameLayout) fullLayout.findViewById(R.id.content_layout);
        contentLayout.addView(view);

        initializeToolbar(fullLayout);
        super.setContentView(fullLayout);
    }

    public TextView getLabelText(){
        return labelTv;
    }

    public void setBaseLayout(){
        setContentView(getLayoutInflater().inflate(R.layout.base_activity, null));
    }

    private void initializeToolbar(View view){
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        backBt = toolbar.findViewById(R.id.back_bt);
        backBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        imageIv = (ImageView) toolbar.findViewById(R.id.image_iv);

        labelTv = (TextView) toolbar.findViewById(R.id.label_tv);
        try {
            String label = getString(getPackageManager().getActivityInfo(getComponentName(), 0).
                    labelRes);
            if (!TextUtils.isEmpty(label)){
                labelTv.setVisibility(View.VISIBLE);
                labelTv.setText(label);
            }
        } catch (Exception ignored) {}

        mutualFriendsIv = toolbar.findViewById(R.id.mutual_friends_iv);
        mutualFriendsCountTv = (TextView) toolbar.findViewById(R.id.mutual_friends_tv);
    }

    public static void makeFullScreen(Activity activity){
        activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public void showImageIv(){
        imageIv.setVisibility(View.VISIBLE);
    }

    public ImageView getImageIv() {
        return imageIv;
    }

    public void showBackButton(){
        backBt.setVisibility(View.VISIBLE);
    }

    public void hideBackButton(){
        backBt.setVisibility(View.GONE);
    }

    public void showLabel(){
        labelTv.setVisibility(View.VISIBLE);
    }

    public void setLabel(int resId){
        setLabel(getString(resId));
    }

    public void setLabel(String label){
        labelTv.setText(label);
    }

    public static void setFragment(FragmentActivity activity, Fragment fragment, int layoutId,
                                   boolean animation){
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        String tag = fragment.getClass().getSimpleName();
        if (fragmentManager.findFragmentByTag(tag) == null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            if (animation) fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            fragmentTransaction.replace(layoutId, fragment, tag).commit();
        }
    }

    public void setFragment(Fragment fragment, int layoutId){
        setFragment(this, fragment, layoutId, false);
    }

    public static Fragment getFragment(FragmentActivity activity, Class cl){
        return activity.getSupportFragmentManager().findFragmentByTag(cl.getSimpleName());
    }

    public Fragment getFragment(Class cl){
        return getFragment(this, cl);
    }

    public static void applyActivityFadeAnimation(Activity activity){
        activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public static void applyActivityRightAnimation(Activity activity){
        activity.overridePendingTransition(R.anim.activity_start_enter_right_side,
                R.anim.activity_start_exit_right_side);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (backBt.getVisibility() == View.VISIBLE)
            overridePendingTransition(R.anim.activity_finish_enter_right_side,
                    R.anim.activity_finish_exit_right_side);
    }

    public TextView getLabel() {
        return labelTv;
    }

    public void showMutualFriendsTv(){
        mutualFriendsCountTv.setVisibility(View.VISIBLE);
    }

    public TextView getMutualFriendsCountTv() {
        return mutualFriendsCountTv;
    }

    public void showMutualFriendsIv(){
        mutualFriendsIv.setVisibility(View.VISIBLE);
    }
}
