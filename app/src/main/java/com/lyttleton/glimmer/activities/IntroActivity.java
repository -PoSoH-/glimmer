package com.lyttleton.glimmer.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

//import com.facebook.CallbackManager;
import com.lyttleton.glimmer.App;
import com.lyttleton.glimmer.DataAccessObject.VenueORM;
import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.fragments.GenderInterestFragment;
import com.lyttleton.glimmer.fragments.IntroFragment;
import com.lyttleton.glimmer.fragments.SignUserPhotoFragment;
import com.lyttleton.glimmer.fragments.SignVerifyFragment;
import com.lyttleton.glimmer.fragments.dialogs.CameraSelectionDialog;
//import com.lyttleton.glimmer.helpers.DAO;
import com.lyttleton.glimmer.fragments.dialogs.ProgressSpinnerIOS;
import com.lyttleton.glimmer.helpers.FileHelper;
import com.lyttleton.glimmer.helpers.MediaSelector;
import com.lyttleton.glimmer.loaders.Backend;

import java.util.Random;

import static com.lyttleton.glimmer.fragments.SignTelephoneFragment.*;
import static com.lyttleton.glimmer.fragments.SignUserFragment.*;

public class IntroActivity extends BaseActivity implements OnClickListenerDialogProgress
        , OnListenerDialogProgressViewSignUp
        , SignVerifyFragment.OnListenerVerifyFragment
        , SignUserPhotoFragment.MediaFragmentListener
        , CameraSelectionDialog.MediaSelectorListener {

    public static final String STARTED_FOR_GENDER_CHOOSE = "STARTED_FOR_GENDER_CHOOSE";
    public static final String PAGE_SELECTED_IN_START = "PAGE_SELECTED_IN_START";
    public static final String CREATED = "created";
    public static final String PENDING = "pending";
    public static final String VERIFIED = "verified";
    public static final String LIVE = "live";

    public static final String PAGE_INTRO = "INTRO-FRAGMENT";
    public static final String PAGE_SIGN_UP = "SIGN-UP-SELECTED";
    public static final String PAGE_TELEPHONE = "TELEPHONE-SELECTED";
    public static final String PAGE_VERIFIED = "CODE-VERIFIED-INPUT";
    public static final String PAGE_ADD_IMAGE = "PROFILE-IMAGE-SELECTED";

    private ProgressSpinnerIOS dialog; // = new ProgressDialog(this);`

    private String _VerificationCode;
//    private String _UniqueName;
    private String _TelephoneNumber;

    private String _PageSelected = null;
    private Uri fileDirectory;

    private TextView txtLabel;

    public static void start(Activity activity, String pageSelected){
        Intent intent = new Intent(activity, IntroActivity.class);
        intent.putExtra(PAGE_SELECTED_IN_START, pageSelected);
        activity.startActivity(intent);
        BaseActivity.applyActivityFadeAnimation(activity);
    }

//    public static void startForGenderChoose(SplashScreenActivity activity) {
//        Intent intent = new Intent(activity, IntroActivity.class);
//        intent.putExtra(STARTED_FOR_GENDER_CHOOSE, true);
//        activity.startActivity(intent);
//        BaseActivity.applyActivityFadeAnimation(activity);
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_layout);

//        mediaSelector = new MediaSelector(this);
        txtLabel = (TextView) findViewById(R.id.label_tv);

        dialog = new ProgressSpinnerIOS();

        if (BaseActivity.getFragment(this, IntroFragment.class) == null
                && BaseActivity.getFragment(this, GenderInterestFragment.class) == null) {
            Fragment fragment;
            if (getIntent().getBooleanExtra(STARTED_FOR_GENDER_CHOOSE, false))
                fragment = new GenderInterestFragment();
            else fragment = new IntroFragment();
            BaseActivity.setFragment(this, fragment, R.id.fragment_container, false);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(App.logger) Log.d("glimmer", "" + MediaSelector.IMAGE_LOAD_REQUEST_CODE);

        switch (requestCode){
            case MediaSelector.IMAGE_LOAD_REQUEST_CODE:
                if(App.logger) Log.d("glimmer", "LOAD");
                if(data != null) {
                    String pathURI = FileHelper.getPath(this, data.getData());
                    addPhotoUser(pathURI);
                }
                break;
            case MediaSelector.IMAGE_CAPTURE_REQUEST_CODE:
                if(App.logger) Log.d("glimmer", "CAPTURE");
                if (fileDirectory != null) {
                    addPhotoUser(FileHelper.getPathFromUri(this, fileDirectory));
                }
                break;
        }

    }

    private void addPhotoUser(String uri){
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment != null && fragment instanceof SignUserPhotoFragment) {
            ((SignUserPhotoFragment) fragment).addPhoto(uri);
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(App.logger) Log.d("glimmer", "touch pressed");
        if(event.getAction() == MotionEvent.ACTION_DOWN){
            App.hideKeyboard(this);
        }
        return super.onTouchEvent(event);
    }

    private void createVerificationCode(){
        char []symbols = {'1','2','3','4','5','6','7','8','9','0'};
        StringBuilder idNumber = new StringBuilder();
        for(int i=0; i<4; i++){
            idNumber.append(symbols[new Random().nextInt(10)]);
        }
        if(App.logger) Log.i("glimmer", "verification code created :: " + idNumber.toString());
        _VerificationCode = idNumber.toString();
    }

    private String createCodeUniqueName(){
        char []symbols = {
//                'a','b','c','d','e','f','g','h','i','j',  //10 symbols
//                'k','k','m','n','o','p','q','r','s','t',  //10 symbols
//                'u','v','w','x','y','z',                  //6 symbols
                'A','B','C','D','E','F','G','H','I','J',  //10 symbols
                'K','L','M','N','O','P','Q','R','S','T',  //10 symbols
                'U','V','W','X','Y','Z',                  //6 symbols
                '1','2','3','4','5','6','7','8','9','0'}; //10 symbols
        int staticSymbolsCount = symbols.length;
        StringBuilder idNumber = new StringBuilder();
        for(int d=0; d<5; d++) {
            int symbolCount = 0;
            if(d == 0) symbolCount = 8;
            else if(d > 0 && d < 4 ) symbolCount = 4;
            else symbolCount = 12;
            for (int i = 0; i < symbolCount; i++) {
                idNumber.append(symbols[new Random().nextInt(staticSymbolsCount)]);
            }
            if(d < 4) idNumber.append("-");
        }
        if (App.logger) Log.d("glimmer", "Unique name code created :: " + idNumber.toString());
        return idNumber.toString();
    }

   @Override
    public void onListenerCreateVerificationCode() {
        createVerificationCode();
    }

    @Override
    public String onListenerVerificationCode() {
        return _VerificationCode;
    }

    @Override
    public String onListenerNumberPhone() {
        return this._TelephoneNumber;
    }

    @Override
    public void onListenerVerifyCode() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if(fragment != null && fragment instanceof SignUserPhotoFragment){
            _TelephoneNumber = "+" + Backend.getCurrentUser().get(Backend.TU_PREFIX).toString()
                    + Backend.getCurrentUser().get(Backend.TU_NUMBER_PHONE).toString();
            VenueORM.logger("verification number", _TelephoneNumber);
        }
    }

    @Override
    public void OnListenerDialogProgressShow() {
        dialog.show(getSupportFragmentManager(), ProgressSpinnerIOS.class.getName());
    }

    @Override
    public void OnListenerDialogProgressHide() {
        dialog.dismiss();
    }

    @Override
    public String OnListenerGetDataUniqueCode() {
        return createCodeUniqueName();
    }

    @Override
    public void onListenerChangeLabelText(String textLabel) {
        txtLabel.setVisibility(View.VISIBLE);
        txtLabel.setText(textLabel);
    }

    @Override
    public void OnListenerTelephoneNumber(String telephoneNumber) {
        this._TelephoneNumber = telephoneNumber;
    }

    @Override
    public void onClickListenerShowMediaDialog() {
        CameraSelectionDialog.show(this);
    }

    @Override
    public void setMediaUri(Uri uri) {
        Log.d("glimmer", "..URL.." + uri);
        fileDirectory = uri;
//        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
//
//        if(fragment != null && fragment instanceof SignUserPhotoFragment){
//            ((SignUserPhotoFragment)fragment).addPhoto(FileHelper.getPath(this, uri));
//        }
    }

    @Override
    public void setNumberUpdatedPhoto(int photoUpdate) {
    }
}