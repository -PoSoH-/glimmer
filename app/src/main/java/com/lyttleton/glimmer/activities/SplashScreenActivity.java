package com.lyttleton.glimmer.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.fragments.IntroFragment;
import com.lyttleton.glimmer.fragments.dialogs.GpsCheckDialog;
import com.lyttleton.glimmer.helpers.InternetHelper;
import com.lyttleton.glimmer.helpers.SharedPrefsHelper;
import com.lyttleton.glimmer.helpers.ToastHelper;
import com.lyttleton.glimmer.loaders.Backend;
import com.parse.ParseObject;

public class SplashScreenActivity extends FragmentActivity implements GpsCheckDialog.GpsDialogListener {

    public static final int SHOW_TIME = 1000;
    private boolean userGoToGpsSettings, headsUpRead;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        headsUpRead = SharedPrefsHelper.isHeadsUpRead(this);
        if (headsUpRead) setContentView(R.layout.splash_screen_layout);
        else {
            setContentView(R.layout.heads_up_layout);

            View termsTv = findViewById(R.id.terms_tv), readyBt = findViewById(R.id.ready_bt);
            readyBt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("glimmer", "...On create IntroActivity.PAGE_SIGN_UP");
                    IntroActivity.start(SplashScreenActivity.this, IntroActivity.PAGE_INTRO);
                    SharedPrefsHelper.saveHeadsUpRead(SplashScreenActivity.this);
                    finish();
                }
            });

            termsTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TermsActivity.start(SplashScreenActivity.this);
                }
            });
        }

        if (!GpsCheckDialog.isGpsEnabled(this)) GpsCheckDialog.show(this);
        else if (headsUpRead) doLogin();
    }

    private void doLogin(){
        if (Backend.hasCurrentUser()){
            final long startTime = System.currentTimeMillis();
            if (!InternetHelper.checkInternetConnection(this)) finishSplash(startTime);
            else {
                Backend.fetchCurrentUserInBackground(this, new Backend.OnSuccessResponseListener(this) {
                    @Override
                    public void onSuccess(ParseObject parseObject) {
                        finishSplash(startTime);
                    }

                    @Override
                    public void onError(String error, int errorCode) {
                        if (errorCode != Backend.CONNECTION_FAILED_ERROR_CODE)
                            ToastHelper.showErrorToast(SplashScreenActivity.this, error);
                        finishSplash(startTime);
                    }
                });
            }
        } else {
            BaseActivity.blockOrientationChange(this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.d("glimmer", "...OnLogin method IntroActivity.PAGE_SIGN_UP");
                    finish();
                    IntroActivity.start(SplashScreenActivity.this, IntroActivity.PAGE_SIGN_UP);
                }
            }, SHOW_TIME);
        }
    }

    private void finishSplash(long startTime){
        // Wait to SHOW_TIME if loading was faster
        final long loadingTime = System.currentTimeMillis() - startTime;

        BaseActivity.blockOrientationChange(SplashScreenActivity.this);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                if (loadingTime < SHOW_TIME) {
                    try {
                        Thread.sleep(SHOW_TIME - loadingTime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                BaseActivity.restoreOrientationChange(SplashScreenActivity.this);
                if (!Backend.hasCurrentUser()) {
                    IntroActivity.start(SplashScreenActivity.this, IntroActivity.PAGE_SIGN_UP);
                    finish();
                } else if (Backend.isUserStatusCreated()) {
                    IntroActivity.start(SplashScreenActivity.this, IntroActivity.PAGE_TELEPHONE);
                    finish();
                } else if(Backend.isUserStatusPending()){
                    IntroActivity.start(SplashScreenActivity.this, IntroActivity.PAGE_VERIFIED);
                    finish();
                } else if(Backend.isUserStatusVerified()){
                    IntroActivity.start(SplashScreenActivity.this, IntroActivity.PAGE_ADD_IMAGE);
                    finish();
                } else if(Backend.isUserStatusLive()){
                    MainActivity.start(SplashScreenActivity.this);
                    finish();
                }
            }
        }.execute();
    }

    @Override
    public void onCancelGpsDialog() {
        if (headsUpRead) doLogin();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (userGoToGpsSettings && headsUpRead){
            userGoToGpsSettings = false;
            BaseActivity.restoreOrientationChange(this);
            doLogin();
        }
    }

    @Override
    public void onGoToGpsSettings() {
        userGoToGpsSettings = true;
        BaseActivity.blockOrientationChange(this);
    }
}
