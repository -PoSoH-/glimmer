package com.lyttleton.glimmer.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

//import com.facebook.AccessToken;
//import com.facebook.CallbackManager;
//import com.facebook.login.LoginManager;
import com.lyttleton.glimmer.App;
import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.adapters.User;
import com.lyttleton.glimmer.fragments.dialogs.CameraSelectionDialog;
import com.lyttleton.glimmer.fragments.dialogs.ProgressSpinnerIOS;
import com.lyttleton.glimmer.fragments.dialogs.RefreshPhotosDialog;
import com.lyttleton.glimmer.helpers.FileHelper;
import com.lyttleton.glimmer.helpers.MediaSelector;
import com.lyttleton.glimmer.helpers.SharedPrefsHelper;
import com.lyttleton.glimmer.loaders.Backend;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

public class MyProfilePhotosActivity extends BaseActivity
        implements RefreshPhotosDialog.RefreshPhotosDialogListener,
        CameraSelectionDialog.MediaSelectorListener {

    public static final String PHOTOS_UPLOADING_ACTION = "com.lyttleton.glimmer.PHOTOS_UPLOADING";
    public static final String PHOTO_UPLOADING_ACTION = "com.lyttleton.glimmer.PHOTO_UPLOADING";
    public static final String PHOTO_UPLOADING_INDEX = "PHOTO_UPLOADING_INDEX";

    private View progressBar;
//    private ArrayList<String> userPhotos;
    private View profilePhotoButtons[];
    private ImageView profilePhotoIvs[], dailyProfilerIv;
    private View uploadDailyProfilerTv;

    private View editDailyProfilerBt;
//    private CallbackManager callbackManager;
    private MediaSelector mediaSelector;
    private User user;
    private Uri directory;
    private boolean photoUploading;
    private String []photosURLs = new String [] {null, null, null, null};
    private int position = -1;

    private ProgressSpinnerIOS ios = new ProgressSpinnerIOS();

    private BroadcastReceiver photosUploadingBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            progressBar.setVisibility(View.GONE);
//            UserPhotosFragment fragment = getUserPhotosFragment();
//            userPhotos = Backend.makeUser(Backend.getCurrentUser()).getPhotos();
//            if (fragment != null) fragment.updatePhotos(userPhotos);
        }
    };

    private BroadcastReceiver photoUploadingBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            progressBar.setVisibility(View.GONE);
            user = Backend.makeUser(Backend.getCurrentUser());
            updateDailyProfileTv();
            showPhotos();
        }
    };

    public static void finishPhotosUploadingByBroadcast(Context context){
        Intent intent = new Intent(PHOTOS_UPLOADING_ACTION);
        context.sendBroadcast(intent);
    }

    public static void start(Activity activity){
        Intent intent = new Intent(activity, MyProfilePhotosActivity.class);
        activity.startActivity(intent);
        BaseActivity.applyActivityRightAnimation(activity);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mediaSelector.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_profile_photos_layout);
        View loadingLayout = getLayoutInflater().inflate(R.layout.loading_layout, null);
        loadingLayout.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        ((ViewGroup)findViewById(R.id.layout)).addView(loadingLayout);
//        changePhotosOrderBt = findViewById(R.id.change_photos_order_bt);

        Context context = getApplicationContext();

        ((View)findViewById(R.id.txt_first_start)).setVisibility(View.GONE);

        if (savedInstanceState != null)
            mediaSelector = MediaSelector.restoreFromInstanceState(savedInstanceState, context);
        else mediaSelector = new MediaSelector(context);

        editDailyProfilerBt = findViewById(R.id.edit_daily_profiler_bt);
        uploadDailyProfilerTv = findViewById(R.id.upload_daily_profiler_tv);
        progressBar = findViewById(R.id.progress_bar);

        profilePhotoButtons = new View[]{findViewById(R.id.profile_photo_bt_1),
                findViewById(R.id.profile_photo_bt_2),
                findViewById(R.id.profile_photo_bt_3)};

        for(View txt : profilePhotoButtons){
            txt.setVisibility(View.VISIBLE);
        }

        dailyProfilerIv = (ImageView) findViewById(R.id.dailyProfilerIv);
        dailyProfilerIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startImageLoad(v);
            }
        });
        profilePhotoIvs = new ImageView[]{
                (ImageView) findViewById(R.id.profile_photo_1_iv),
                (ImageView) findViewById(R.id.profile_photo_2_iv),
                (ImageView) findViewById(R.id.profile_photo_3_iv)};

        profilePhotoIvs[0].setVisibility(View.VISIBLE);
        profilePhotoIvs[1].setVisibility(View.VISIBLE);
        profilePhotoIvs[2].setVisibility(View.VISIBLE);

        for(View txt : profilePhotoIvs){
            txt.setVisibility(View.VISIBLE);
            txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startImageLoad(v);
                }
            });
        }

        user = Backend.makeUser(Backend.getCurrentUser());
        photoUploading = SharedPrefsHelper.isPhotoUploading(context);
        updateDailyProfileTv();
        updateUploadingPB();
        showPhotos();

        initializeClickListeners();

        showBackButton();
        showLabel();
//        setLabel(user.getFirstName());
        setLabel(getString(R.string.my_profile_photos_name));

        registerReceiver(photosUploadingBroadcastReceiver, new IntentFilter(PHOTOS_UPLOADING_ACTION));
        registerReceiver(photoUploadingBroadcastReceiver, new IntentFilter(PHOTO_UPLOADING_ACTION));
    }

    private void startImageLoad(View v) {
        switch (v.getId()){
            case R.id.dailyProfilerIv:
                logger("Click set photo", "0");
                CameraSelectionDialog.show(this, 0);
                break;
            case R.id.profile_photo_1_iv:
                logger("Click set photo", "1");
                CameraSelectionDialog.show(this, 1);
                break;
            case R.id.profile_photo_2_iv:
                logger("Click set photo", "2");
                CameraSelectionDialog.show(this, 2);
                break;
            case R.id.profile_photo_3_iv:
                logger("Click set photo", "3");
                CameraSelectionDialog.show(this, 3);
                break;
            default:
        }
    }

    private void logger(String place, String data){
        if(App.logger){
            Log.d("glimmer", ".." + place + ".." + data + "..");
        }
    }

    private void updateUploadingPB() {
        if (photoUploading) progressBar.setVisibility(View.VISIBLE);
        else progressBar.setVisibility(View.GONE);
    }

    private void updateDailyProfileTv() {
        if (user.isUploadedDailyProfiler()) uploadDailyProfilerTv.setVisibility(View.GONE);
        else uploadDailyProfilerTv.setVisibility(View.VISIBLE);
    }

    private void showPhotos() {
        if (user.getPhotos() != null && user.getPhotos().size() > 0) {
            ArrayList<String> photoUrls = user.getPhotos();
            Picasso picasso = Picasso.with(getApplicationContext());
            if (user.isUploadedDailyProfiler()) {

                picasso.load(photoUrls.get(0)).fit().centerInside().into(dailyProfilerIv);
                photoUrls.remove(0);
            }
            for (int i = 0; i < photoUrls.size(); i++)
                if (i < profilePhotoIvs.length) {
                    picasso.load(photoUrls.get(i)).fit().centerCrop().into(profilePhotoIvs[i]);
                    profilePhotoButtons[i].setVisibility(View.GONE);
                }
        }
    }

    private void initializeClickListeners() {
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        callbackManager = CallbackManager.Factory.create();
//        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                startPhotosUploading();
//            }
//
//            @Override
//            public void onCancel() {}
//
//            @Override
//            public void onError(FacebookException e) {}
//        });

        editDailyProfilerBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CameraSelectionDialog.show(MyProfilePhotosActivity.this);
            }
        });

        uploadDailyProfilerTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CameraSelectionDialog.show(MyProfilePhotosActivity.this);
            }
        });
    }

    private void startPhotosUploading(){
//        if (!SharedPrefsHelper.isPhotosUploading(getApplicationContext())) {
//            SharedPrefsHelper.startPhotosUploading(getApplicationContext());
//            progressBar.setVisibility(View.VISIBLE);
//            GraphRequest request = GraphRequest.newMeRequest(
//                    AccessToken.getCurrentAccessToken(),
//                    new GraphRequest.GraphJSONObjectCallback() {
//                        @Override
//                        public void onCompleted(JSONObject jsonObject, GraphResponse response) {
//                            PhotosUploadService.start(getApplicationContext(), jsonObject.toString());
//                        }
//                    });
//            Bundle parameters = new Bundle();
//            parameters.putString("fields", Backend.FACEBOOK_FIELDS);
//            request.setParameters(parameters);
//            request.executeAsync();
//        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(photosUploadingBroadcastReceiver);
        unregisterReceiver(photoUploadingBroadcastReceiver);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case MediaSelector.IMAGE_LOAD_REQUEST_CODE:
                if (data != null) {
                    upLoadFile(FileHelper.getPath(this, data.getData()));
                }
                break;
            case MediaSelector.IMAGE_CAPTURE_REQUEST_CODE:
                if (directory != null) {
                    upLoadFile(FileHelper.getPathFromUri(this, directory));
                }
            break;
        }
        position = -1;

//        mediaSelector.onActivityResult(requestCode, resultCode, data);
//        if (mediaSelector.hasFilePath()) {
//            Context context = getApplicationContext();
//            if (InternetHelper.checkInternetConnection(context)) {
//                SelectedPhotoUploadService.start(context, mediaSelector.getMediaPath());
//                photoUploading = true;
//                SharedPrefsHelper.startPhotoUploading(getApplicationContext());
//                updateUploadingPB();
//                mediaSelector.removeSelection();
//            } else ToastHelper.showErrorToast(context, R.string.check_your_internet_connection);
//        }
    }


    private void upLoadFile(String uri){
        if(uri == null) return;
        if (position > -1) {
//            String pathURI = FileHelper.getPathFromUri(this, uri);
            ImageView resources = null;
            switch (position) {
                case 0:
                    resources = dailyProfilerIv;
                    photosURLs[position] = uri;
                    break;
                case 1:
                case 2:
                case 3:
                    for (int p = 0, l = profilePhotoButtons.length; p < l; p++) {
                        if (position - 1 == p) {
                            resources = profilePhotoIvs[p];
                            photosURLs[position] = uri;
                            profilePhotoButtons[position-1].setVisibility(View.GONE);
                        }
                    }
                    break;
            }
            if (resources != null)
                Picasso.with(this).load(new File(uri)).centerCrop().fit().into(resources);
            logger("URI address", uri);
        }
//        ios = new ProgressSpinnerIOS().show(this);
        Backend.addedImageProfiles386(photosURLs, new Backend.onCallbackIosProgressDismiss() {
            @Override
            public void result() {
//                ios.dismiss();
            }
        });
    }


    @Override
    public void onRefreshClicked() {
//        Context context = getApplicationContext();
//        if (!InternetHelper.checkInternetConnection(context))
//            ToastHelper.showErrorToast(context, R.string.check_your_internet_connection);
//        else {
//            AccessToken accessToken = AccessToken.getCurrentAccessToken();
//            if (accessToken == null || accessToken.isExpired())
//                LoginManager.getInstance().logInWithReadPermissions(MyProfilePhotosActivity.this,
//                        Backend.FACEBOOK_PERMISSION);
//            else startPhotosUploading();
//        }
    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        overridePendingTransition(R.anim.activity_finish_enter_right_side,
//                R.anim.activity_finish_exit_right_side);
//    }

    @Override
    public void setMediaUri(Uri uri) {
        directory = uri;
    }

    @Override
    public void setNumberUpdatedPhoto(int photoUpdate) {
        logger("listener", photoUpdate + "");
        position = photoUpdate;
    }
}