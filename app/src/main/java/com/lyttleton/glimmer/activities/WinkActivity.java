package com.lyttleton.glimmer.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lyttleton.glimmer.DataAccessObject.UserORM;
import com.lyttleton.glimmer.DataAccessObject.WinkORM;
import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.adapters.User;
import com.lyttleton.glimmer.adapters.Venue;
import com.lyttleton.glimmer.adapters.Wink;
import com.lyttleton.glimmer.adapters.WinkViewPagerAdapter;
import com.lyttleton.glimmer.fragments.FeedListFragment;
import com.lyttleton.glimmer.fragments.dialogs.AlreadyMatchedDialog;
import com.lyttleton.glimmer.fragments.dialogs.TipDialog;
import com.lyttleton.glimmer.helpers.CircleTransform;
import com.lyttleton.glimmer.helpers.InternetHelper;
import com.lyttleton.glimmer.helpers.SharedPrefsHelper;
import com.lyttleton.glimmer.helpers.ToastHelper;
import com.lyttleton.glimmer.loaders.Backend;
import com.parse.ParseObject;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class WinkActivity extends FragmentActivity {

    public static final String USER_INDEX_ARG = "USER_INDEX_ARG";
    public static final String VENUE_ARG = "VENUE_ARG";
    private ViewPager viewPager;
    private ArrayList<User> users;
    private static ArrayList<User> usersParam = (ArrayList<User>) UserORM.loadAllUsers();
    private View winkBt, winkLayout, cancelWinkTv, winkWithPhotoBt, winkWithClueBt;
    private View itsMatchLayout, chatBt, cancelMatchTv;
    private EditText clueEt;
    private ImageView winkPhotoIv, matchPhotoIv;
    private Venue venue;
    private CircleTransform transform = new CircleTransform();
    private View loadingImage;

    public static Intent createStartIntent(Context context, User user, Venue venue) {
        Intent intent = new Intent(context, WinkActivity.class);
        usersParam = new ArrayList<>();
        usersParam.add(user);
        intent.putExtra(USER_INDEX_ARG, 0);
        intent.putExtra(VENUE_ARG, venue);
        return intent;
    }

    public static void start(Activity activity, ArrayList<User> users, int currentUserIndex,
                             Venue venue){
        Intent intent = new Intent(activity, WinkActivity.class);
        usersParam = users;
        intent.putExtra(USER_INDEX_ARG, currentUserIndex);
        intent.putExtra(VENUE_ARG, venue);
        activity.startActivity(intent);
        BaseActivity.applyActivityRightAnimation(activity);
    }

    public static void start(Activity activity, User user, Venue venue){
        ArrayList<User> users = new ArrayList<>();
        users.add(user);
        start(activity, users, 0, venue);
    }

    public static void start(Activity activity, User user) {
        ArrayList<User> users = new ArrayList<>();
        users.add(user);
        start(activity, users, 0, null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wink_activity);

        if (usersParam == null || usersParam.size() == 0) {
            super.onBackPressed();
            return;
        }
        users = usersParam;

        venue = (Venue) getIntent().getSerializableExtra(VENUE_ARG);

        viewPager = (ViewPager) findViewById(R.id.wink_view_pager);
        winkBt = findViewById(R.id.wink_bt);
        loadingImage = findViewById(R.id.loading_image);
        ((TextView)findViewById(R.id.loading_text_tv)).setText(R.string.sent);

        // Check if activity started from chat
        if (venue == null){
            winkBt.setVisibility(View.GONE);
            View chatBt = findViewById(R.id.chat_bt);
            chatBt.setVisibility(View.VISIBLE);
            chatBt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        ViewGroup layout = ((ViewGroup)findViewById(R.id.layout));

        viewPager.setAdapter(new WinkViewPagerAdapter(getSupportFragmentManager(), users));
        viewPager.setCurrentItem(getIntent().getIntExtra(USER_INDEX_ARG, 0));

        //Wink transparent layout
        winkLayout = getLayoutInflater().inflate(R.layout.wink_to_user_layout, null);
        winkLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        winkWithPhotoBt = winkLayout.findViewById(R.id.wink_with_photo_bt);
        winkWithClueBt = winkLayout.findViewById(R.id.wink_with_clue_bt);
        clueEt = (EditText) winkLayout.findViewById(R.id.clue_et);
        cancelWinkTv = winkLayout.findViewById(R.id.cancel_wink_tv);
        layout.addView(winkLayout);
        winkPhotoIv = (ImageView) findViewById(R.id.wink_photo_iv);

        //It's match transparent layout
        itsMatchLayout = getLayoutInflater().inflate(R.layout.its_match_layout, null);
        itsMatchLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        String user = getSelectedUser().getFirstName();
        TextView matchMessageTv = (TextView) itsMatchLayout.findViewById(R.id.match_message_tv);
        matchMessageTv.setText(getString(R.string.you_and) + " " + getSelectedUser().getFirstName()
                + " " + getString(R.string.like_each_other));
        chatBt = itsMatchLayout.findViewById(R.id.chat_bt);
        cancelMatchTv = itsMatchLayout.findViewById(R.id.cancel_match_tv);
        layout.addView(itsMatchLayout);
        matchPhotoIv = (ImageView) findViewById(R.id.match_photo_iv);

        initializeClickListeners();
        loadWinkAndMatchIcons();
    }

    public void setSelectedUserMatched(User user){
        User selectedUser = getSelectedUser();
        if (selectedUser.getObjectId().equals(user.getObjectId())) {
            getSelectedUser().setMatched(true);
            updateWinkButton();
        }
    }

    private void loadWinkAndMatchIcons(){
        User user = getSelectedUser();
        winkPhotoIv.setVisibility(View.GONE);
        matchPhotoIv.setVisibility(View.GONE);
        if (user.hasPhotos()) {
            int iconSize = (int) getResources().getDimension(R.dimen.wink_to_user_icon_size);
            Picasso.with(this).load(user.getPhotoUrl()).resize(iconSize, 0).transform(transform).
                    into(winkPhotoIv, new Callback() {
                        @Override
                        public void onSuccess() {
                            winkPhotoIv.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onError() {}
                    });

            iconSize = (int) getResources().getDimension(R.dimen.its_match_icon_size);
            Picasso.with(this).load(user.getPhotoUrl()).resize(iconSize, 0).transform(transform).
                    into(matchPhotoIv, new Callback() {
                        @Override
                        public void onSuccess() {
                            matchPhotoIv.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onError() {}
                    });
        }
        updateWinkButton();
    }

    // Block orientation change
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void updateWinkButton(){
        if (getSelectedUser().isMatched()) winkBt.setBackgroundResource(R.drawable.chat);
        else winkBt.setBackgroundResource(R.drawable.wink);
    }

    private void initializeClickListeners() {
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                loadWinkAndMatchIcons();
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });

        View prevUserBt = findViewById(R.id.prev_user_bt);
        if (users.size() > 1) {
            prevUserBt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int prevItem = viewPager.getCurrentItem() - 1;
                    if (prevItem >= 0) viewPager.setCurrentItem(prevItem);
                }
            });
        } else prevUserBt.setVisibility(View.GONE);

        View nextUserBt = findViewById(R.id.next_user_bt);
        if (users.size() > 1) {
            nextUserBt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int nextItem = viewPager.getCurrentItem() + 1;
                    if (nextItem < viewPager.getAdapter().getCount())
                        viewPager.setCurrentItem(nextItem);
                }
            });
        } else nextUserBt.setVisibility(View.GONE);

        winkBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final User user = users.get(viewPager.getCurrentItem());
                if (!InternetHelper.checkInternetConnection(WinkActivity.this)) {
                    ToastHelper.showErrorToast(WinkActivity.this, R.string.check_your_internet_connection);
                    return;
                } else if (user.getObjectId().equals(Backend.getCurrentUser().getObjectId())){
                    ToastHelper.showErrorToast(WinkActivity.this, R.string.wink_yourself);
                    return;
                } else if (user.isMatched()) {
                    ChatActivity.start(WinkActivity.this, user);
                    return;
                }
                BaseActivity.blockOrientationChange(WinkActivity.this);
                Backend.didUserWinkedAtCurrentUser(user,
                        new Backend.OnSuccessResponseListener(WinkActivity.this) {

                            @Override
                            public void onError(String error, int errorCode) {
                                BaseActivity.restoreOrientationChange(WinkActivity.this);
                                if (errorCode == Backend.OBJECT_NOT_FOUND_ERROR_CODE){
                                    Animation animation = AnimationUtils.loadAnimation(WinkActivity.
                                            this, R.anim.fade_in);
                                    animation.setAnimationListener(new Animation.AnimationListener() {
                                        @Override
                                        public void onAnimationStart(Animation animation) {}

                                        @Override
                                        public void onAnimationEnd(Animation animation) {
                                            if (!SharedPrefsHelper.hasWinksTip(WinkActivity.this))
                                                TipDialog.show(WinkActivity.this, R.string.winks_capital,
                                                        R.string.winks_tip,
                                                        SharedPrefsHelper.WINKS_TIP);
                                        }

                                        @Override
                                        public void onAnimationRepeat(Animation animation) {}
                                    });
                                    winkLayout.setVisibility(View.VISIBLE);
                                    winkLayout.startAnimation(animation);
                                } else super.onError(error, errorCode);
                            }

                            @Override
                            public void onSuccess(ParseObject parseObject) {
                                Backend.didCurrentUserWinkedAtUser(user, new Backend.
                                        OnSuccessResponseListener(WinkActivity.this) {

                                    @Override
                                    public void onError(String error, int errorCode) {
                                        BaseActivity.restoreOrientationChange(WinkActivity.this);
                                        if (errorCode == Backend.OBJECT_NOT_FOUND_ERROR_CODE){
                                            Animation animation = AnimationUtils.loadAnimation(WinkActivity.
                                                    this, R.anim.fade_in);
                                            itsMatchLayout.setVisibility(View.VISIBLE);
                                            itsMatchLayout.startAnimation(animation);
                                            Backend.saveWink(Wink.CLUE_TYPE, null, user, venue,
                                                    WinkActivity.this,
                                                    new Backend.OnSuccessResponseListener(WinkActivity.this) {
                                                        @Override
                                                        public void onSuccess(ParseObject parseObject) {
                                                            WinkORM.deleteWinksFromDatabase(user.getObjectId());
                                                            getSelectedUser().setMatched(true);
                                                            winkBt.setBackgroundResource(R.drawable.chat);
                                                        }
                                                    });
                                        } else super.onError(error, errorCode);
                                    }

                                    @Override
                                    public void onSuccess(ParseObject parseObject) {
                                        BaseActivity.restoreOrientationChange(WinkActivity.this);
                                        AlreadyMatchedDialog.show(WinkActivity.this, user);
                                    }
                                });
                            }
                        });
            }
        });

        winkWithPhotoBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Backend.saveWink(Wink.PHOTO_TYPE, null, getSelectedUser(), venue, WinkActivity.this,
                        new Backend.OnSuccessResponseListener(WinkActivity.this) {
                            @Override
                            public void onSuccess(ParseObject parseObject) {
                                showLoadingImage();
                            }
                        });
                hideWinkLayout();
            }
        });

        winkWithClueBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = clueEt.getText().toString();
                if (TextUtils.isEmpty(text)) clueEt.requestFocus();
                else {
                    Backend.saveWink(Wink.CLUE_TYPE, text, getSelectedUser(), venue, WinkActivity.this,
                            new Backend.OnSuccessResponseListener(WinkActivity.this) {
                                @Override
                                public void onSuccess(ParseObject parseObject) {
                                    showLoadingImage();
                                }
                            });
                    hideWinkLayout();
                    clueEt.setText("");
                }
            }
        });

        chatBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatActivity.start(WinkActivity.this, getSelectedUser());
                hideItsMatchLayout();
            }
        });

        cancelWinkTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (winkLayout.getAnimation() == null || !winkLayout.getAnimation().hasStarted())
                    hideWinkLayout();
            }
        });
        cancelMatchTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itsMatchLayout.getAnimation() == null || !itsMatchLayout.getAnimation().hasStarted())
                    hideItsMatchLayout();
            }
        });
    }

    private void showLoadingImage(){
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.loading_circle_scale);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                loadingImage.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        loadingImage.setVisibility(View.VISIBLE);
        loadingImage.startAnimation(animation);
    }

    private void hideWinkLayout(){
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.fade_out_fast);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                winkLayout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        winkLayout.startAnimation(animation);
    }

    private void hideItsMatchLayout(){
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.fade_out_fast);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                itsMatchLayout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        itsMatchLayout.startAnimation(animation);
    }

    @Override
    public void onBackPressed() {
        if (winkLayout.getVisibility() == View.VISIBLE
                || itsMatchLayout.getVisibility() == View.VISIBLE){
            if (winkLayout.getAnimation() == null || !winkLayout.getAnimation().hasStarted())
                hideWinkLayout();
            if (itsMatchLayout.getAnimation() == null || !itsMatchLayout.getAnimation().hasStarted())
                hideItsMatchLayout();
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.activity_finish_enter_right_side,
                    R.anim.activity_finish_exit_right_side);
        }
    }

    private User getSelectedUser(){
        return users.get(viewPager.getCurrentItem());
    }

    @Override
    protected void onDestroy() {
        usersParam = null;
        super.onDestroy();
    }
}
