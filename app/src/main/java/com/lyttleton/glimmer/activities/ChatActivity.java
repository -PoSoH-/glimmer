package com.lyttleton.glimmer.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.adapters.User;
import com.lyttleton.glimmer.fragments.ChatFragment;
import com.lyttleton.glimmer.fragments.dialogs.CameraSelectionDialog;
import com.lyttleton.glimmer.helpers.CircleTransform;
import com.squareup.picasso.Picasso;

public class ChatActivity extends BaseActivity
        implements CameraSelectionDialog.MediaSelectorListener {

    private CircleTransform transform = new CircleTransform();

    public static Intent createStartIntent(Context context, User user){
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra(ChatFragment.USER_ARG, user);
        return intent;
    }

    public static void start(Activity activity, User user){
        activity.startActivity(createStartIntent(activity, user));
        applyActivityRightAnimation(activity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_layout);

        showBackButton();
        showLabel();

        final User user = (User) getIntent().getSerializableExtra(ChatFragment.USER_ARG);
        setLabel(user.getFirstName());

        if (user.hasPhotos()) {
            showImageIv();

            ImageView imageIv = getImageIv();
            int iconSize = (int) getResources().getDimension(R.dimen.toolbar_image_size);
            Picasso.with(getApplicationContext()).load(user.getPhotoUrl()).resize(iconSize, 0).
                    transform(transform).into(imageIv);

            imageIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    WinkActivity.start(ChatActivity.this, user);
                }
            });
        }

        getLabel().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WinkActivity.start(ChatActivity.this, user);
            }
        });

        if (getFragment(ChatFragment.class) == null)
            setFragment(ChatFragment.create(user), R.id.fragment_container);
    }

    @Override
    public void setMediaUri(Uri uri) {
        ChatFragment fragment = (ChatFragment) getFragment(ChatFragment.class);
        if (fragment != null) fragment.setMediaUri(uri);
    }

    @Override
    public void setNumberUpdatedPhoto(int photoUpdate) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ChatFragment fragment = (ChatFragment) getFragment(ChatFragment.class);
        if (fragment != null) fragment.onActivityResult(requestCode, resultCode, data);
    }
}
