package com.lyttleton.glimmer.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.fragments.FeedListFragment;

public class MyWinksActivity extends BaseActivity {

    public static final String TYPE_ARG = "TYPE_ARG";

    public static void start(Activity activity, int type){
        Intent intent = new Intent(activity, MyWinksActivity.class);
        intent.putExtra(TYPE_ARG, type);
        activity.startActivity(intent);
        applyActivityRightAnimation(activity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_layout);

        int type = getIntent().getIntExtra(TYPE_ARG, 0);

        showBackButton();
        setLabel(type == FeedListFragment.WINKS_TYPE ? R.string.winks_received : R.string.my_matches);
        showLabel();

        if (getFragment(FeedListFragment.class) == null)
            setFragment(FeedListFragment.create(type), R.id.fragment_container);
    }
}
