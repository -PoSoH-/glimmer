package com.lyttleton.glimmer.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.fragments.PinPopListFragment;

public class PinPopListActivity extends BaseActivity {

    public static void start(Activity activity, int fragmentType){
        Intent intent = new Intent(activity, PinPopListActivity.class);
        intent.putExtra(PinPopListFragment.FRAGMENT_TYPE_ARG, fragmentType);
        activity.startActivity(intent);
        applyActivityRightAnimation(activity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_layout);

        showBackButton();
        showLabel();
        int type = getIntent().getIntExtra(PinPopListFragment.FRAGMENT_TYPE_ARG, 0);
        setLabel(type == PinPopListFragment.PIN_FRAGMENT_TYPE ? R.string.my_pins : R.string.my_pops);

        if (getFragment(PinPopListFragment.class) == null)
            setFragment(PinPopListFragment.create(type), R.id.fragment_container);
    }
}
