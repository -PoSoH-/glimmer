package com.lyttleton.glimmer.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.loaders.Backend;
import com.parse.ParseObject;

public class TermsActivity extends BaseActivity {

    public static void start(Activity activity){
        Intent intent = new Intent(activity, TermsActivity.class);
        activity.startActivity(intent);
        applyActivityRightAnimation(activity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.terms_layout);

        showBackButton();
        setLabel(R.string.terms_of_service);
        showLabel();

        final View progressBar = findViewById(R.id.progress_bar);
        final WebView termsWV = (WebView) findViewById(R.id.terms_wv);
        final TextView termsTV = (TextView) findViewById(R.id.terms_tv);

        Backend.loadTermsOfService(this, new Backend.OnSuccessResponseListener(this) {
            @Override
            public void onSuccess(ParseObject parseObject) {
                progressBar.setVisibility(View.GONE);
//                termsWV.loadDataWithBaseURL(null, Backend.getTermsText(parseObject), "text/html",
//                        "UTF-8", null);
                termsTV.setText(Html.fromHtml(Backend.getTermsText(parseObject)));
            }

            @Override
            public void onError(String error, int errorCode) {
                super.onError(error, errorCode);
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
