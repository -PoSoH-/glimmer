package com.lyttleton.glimmer.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.AbsListView;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.adapters.Venue;
import com.lyttleton.glimmer.fragments.dialogs.GpsCheckDialog;
import com.lyttleton.glimmer.fragments.VenueFragment;
import com.lyttleton.glimmer.fragments.scroll_tab_holder.ScrollChangeListener;

public class VenueActivity extends BaseActivity implements ScrollChangeListener,
        GpsCheckDialog.GpsDialogListener {

    public static Intent createStartIntent(Context context, Venue venue) {
        Intent intent = new Intent(context, VenueActivity.class);
        intent.putExtra(VenueFragment.VENUE_ARG, venue);
        return intent;
    }

    public static void start(Activity activity, Venue venue){
        activity.startActivity(createStartIntent(activity, venue));
        applyActivityRightAnimation(activity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setBaseLayout();

        Venue venue = (Venue) getIntent().getSerializableExtra(VenueFragment.VENUE_ARG);
        showBackButton();
        showLabel();
        setLabel(venue.getName());

        if (getFragment(VenueFragment.class) == null)
            setFragment(VenueFragment.create(venue), R.id.content_layout);
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                         int totalItemCount, int pagePosition) {
        VenueFragment venueFragment = (VenueFragment) getFragment(VenueFragment.class);
        if (venueFragment != null) venueFragment.onScroll(view, firstVisibleItem, visibleItemCount,
                totalItemCount, pagePosition);
    }

    @Override
    public void onCancelGpsDialog() {}

    @Override
    public void onGoToGpsSettings() {}
}
