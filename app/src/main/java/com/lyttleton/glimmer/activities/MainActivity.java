package com.lyttleton.glimmer.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.adapters.MainViewPagerAdapter;
import com.lyttleton.glimmer.fragments.DiscoverFragment;
import com.lyttleton.glimmer.loaders.Backend;
import com.lyttleton.glimmer.views.TouchableWrapper;
import com.parse.ParseException;

public class MainActivity extends BaseActivity
        implements TouchableWrapper.UpdateMapAfterUserInteraction {

    public static final int DISCOVER_FRAGMENT_INDEX = 1;
    public static final String OPEN_VENUES_ACTION = "OPEN_VENUES_ACTION";
    private ViewPager viewPager;
    private MainViewPagerAdapter viewPagerAdapter;
    private LinearLayout tabs[];
    private int tabsResIds[], tabsSelectedResIds[];

    public static void start(Activity activity){
        Intent intent = new Intent(activity, MainActivity.class);
        activity.startActivity(intent);
        applyActivityRightAnimation(activity);
    }

    private BroadcastReceiver openVenuesBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            openVenues();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        tabs = new LinearLayout[]{(LinearLayout) findViewById(R.id.feed_tab),
                (LinearLayout) findViewById(R.id.discover_tab),
                (LinearLayout) findViewById(R.id.user_profile_tab)};

        tabsResIds = new int[]{R.drawable.feed, R.drawable.pin_bt, R.drawable.account_bt};
        tabsSelectedResIds = new int[]{R.drawable.feed_selected, R.drawable.pin_bt_selected,
                R.drawable.account_bt_selected};

        viewPagerAdapter = new MainViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);

        showLabel();
        initializeClickListeners();

        viewPager.setCurrentItem(DISCOVER_FRAGMENT_INDEX);

        registerReceiver(openVenuesBroadcastReceiver, new IntentFilter(OPEN_VENUES_ACTION));

//        try {
//            Backend.fetchCurrentUser();
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
    }

    private void updateTabs(){
        int backgroundResId;
        for (int i = 0; i < tabs.length; i++){
            if (i == viewPager.getCurrentItem()) backgroundResId = tabsSelectedResIds[i];
            else backgroundResId =  tabsResIds[i];
            tabs[i].getChildAt(0).setBackgroundResource(backgroundResId);
        }
    }

    private void initializeClickListeners() {
        for (int i = 0; i < tabs.length; i++) {
            final View tab = tabs[i];
            final int finalI = i;
            tab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewPager.setCurrentItem(finalI);
                }
            });
        }

        final String titles[] = getResources().getStringArray(R.array.main_activity_titles);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                setLabel(titles[position]);
                if (position == DISCOVER_FRAGMENT_INDEX) {
                    DiscoverFragment fragment = getDiscoverFragment();
                    if (fragment != null) fragment.onFragmentSelected();
                }
                updateTabs();
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });
    }

    @Override
    public void onUpdateMapAfterUserInteraction() {}

    public DiscoverFragment getDiscoverFragment(){
        return (DiscoverFragment)viewPagerAdapter.getRegisteredFragment(DISCOVER_FRAGMENT_INDEX);
    }

    public Location getLastUserLocation(){
        DiscoverFragment fragment = getDiscoverFragment();
        if (fragment != null) return getDiscoverFragment().getLastUserLocation();
        return null;
    }

    public void openVenues(){
        viewPager.setCurrentItem(DISCOVER_FRAGMENT_INDEX);
        getDiscoverFragment().openVenues();
    }

    public void openVenueSearch() {
        viewPager.setCurrentItem(DISCOVER_FRAGMENT_INDEX);
        getDiscoverFragment().openVenueSearch();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(openVenuesBroadcastReceiver);
    }

    public static void openVenuesWithBroadcast(Context context){
        context.sendBroadcast(new Intent(OPEN_VENUES_ACTION));
    }
}