package com.lyttleton.glimmer.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.lyttleton.glimmer.DataAccessObject.WinkORM;
import com.lyttleton.glimmer.adapters.Wink;
import com.lyttleton.glimmer.helpers.InternetHelper;
import com.parse.ParseException;

import java.util.ArrayList;
import java.util.List;

public class WinksLoader extends AsyncTaskLoader<List<Wink>> {

    private int type;
    private List<Wink> previousItems, lastLoadedItems;
    private boolean loaderCreating, hasNextPage;
    private int loadingStartOffset;
    private boolean noInternet;

    public WinksLoader(Context context, int type, List<Wink> previousItems,
                       int loadingStartOffset) {
        super(context);
        this.type = type;
        this.previousItems = previousItems;
        loaderCreating = true;
        if(previousItems != null) {
            this.loadingStartOffset = previousItems.size();
        }
    }

    @Override
    protected void onStartLoading() {
        if (loaderCreating || lastLoadedItems == null || lastLoadedItems.size() == 0) {
            forceLoad();
//            if (!loaderCreating) loadingStartOffset = 0;
        } else deliverResult(lastLoadedItems);
    }

    public boolean isNoInternet() {
        return noInternet;
    }

    @Override
    public List<Wink> loadInBackground() {
        loaderCreating = false;
        noInternet = false;
        List<Wink> loadedWinks = null;
        try {
            if (!InternetHelper.checkInternetConnection(getContext())){
                noInternet = true;
//                loadedWinks = HelpersORM.getAllWinks();
            } else {
                if (type == Wink.MATCH_TYPE) {
                    loadedWinks = Backend.loadMatches(loadingStartOffset, this);
                    WinkORM.replaceAllWinks(loadedWinks);
                }
                else{
                    loadedWinks = Backend.loadWinks(type, loadingStartOffset, this, previousItems);
                    WinkORM.replaceAllWinks(loadedWinks);
//                HelpersORM.replaceAllWinks(loadedWinks);
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
            if (e.getCode() == Backend.CONNECTION_FAILED_ERROR_CODE){
//                loadedWinks = HelpersORM.getAllWinks();
                noInternet = true;
            }
        }

        List<Wink> winks = new ArrayList<>();
        if (previousItems != null) winks.addAll(previousItems);
        if (loadedWinks != null) winks.addAll(loadedWinks);
//        for(Wink wink : winks){
//            if(wink.getType() == Wink.MATCH_TYPE){
//
//            }
//        }
        WinkORM.replaceAllWinks(winks);
        lastLoadedItems = winks;

        return lastLoadedItems;
    }

    public void setHasNextPage(boolean hasNextPage) {
        this.hasNextPage = hasNextPage;
    }

    public boolean hasNextPage() {
        return hasNextPage;
    }

    public int getLoadingStartOffset() {
        return loadingStartOffset;
    }

    public void setLoadingStartOffset(int loadingStartOffset) {
        this.loadingStartOffset = loadingStartOffset;
    }
}
