package com.lyttleton.glimmer.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.lyttleton.glimmer.App;
import com.lyttleton.glimmer.DataAccessObject.VenueORM;
import com.lyttleton.glimmer.adapters.Venue;
//import com.lyttleton.glimmer.helpers.DAO;
//import com.lyttleton.glimmer.helpers.HelpersORM;
import com.lyttleton.glimmer.helpers.ImageHelper;
import com.lyttleton.glimmer.helpers.InternetHelper;
import com.parse.ParseException;

import java.util.ArrayList;
import java.util.List;

public class VenueLoader extends AsyncTaskLoader<List<Venue>> {

    private String searchString;
    private boolean loaderCreating, search, noInternet, hasNextPage;
    private ArrayList<BitmapDescriptor> markerIcons = new ArrayList<>();
    private Context context;
    private List<Venue> previousItems, lastLoadedItems;

    public VenueLoader(Context context, String searchString, boolean search, List<Venue> previousItems) {
        super(context);
        this.searchString = searchString;
        loaderCreating = true;
        this.search = search;
        this.context = context;
        this.previousItems = previousItems;
    }

    @Override
    protected void onStartLoading() {
        if (loaderCreating || lastLoadedItems == null || lastLoadedItems.size() == 0) forceLoad();
        else {
            deliverResult(lastLoadedItems);
            test(lastLoadedItems, "onStartLoading");
        }
    }

    private void test(List<Venue> venues, String method){
        if(App.logger) {
            if (venues != null) {
                for (Venue v : venues) {
                    Log.d("glimmer", "Write venues : " + v.getName() + "..." + method);
                }
            }
        }
    }


    @Override
    public List<Venue> loadInBackground() {
        loaderCreating = false;
        List<Venue> loadedVenues = null;

        noInternet = false;
        int startOffset = previousItems != null ? previousItems.size() : 0;
        if (InternetHelper.checkInternetConnection(getContext())) {
            try {
                loadedVenues = Backend.loadPopularVenues(searchString, startOffset);
//                DAO.replaceVenues(loadedVenues);
            } catch (ParseException e) {
                e.printStackTrace();
                if (e.getCode() == Backend.CONNECTION_FAILED_ERROR_CODE){
//                    loadedVenues = HelpersORM.getAllVenues();
//                    loadedVenues = DAO.loadAllVenues();
                    test(loadedVenues, "loadInBackground - 1");
//                    noInternet = true;
                }
            }
        }
        else{
//            loadedVenues = DAO.loadAllVenues();
            test(loadedVenues, "loadInBackground - 2");
//            noInternet = true;
        }

            List<Venue> venues = new ArrayList<>();
            if (previousItems != null) venues.addAll(previousItems);
            if (loadedVenues != null) {
                venues.addAll(loadedVenues);
                if(InternetHelper.checkInternetConnection(getContext())) {
                    hasNextPage = true;
                    if (loadedVenues.size() < Backend.LOAD_VENUES_LIMIT) hasNextPage = false;
                }
            }

            if (!search)
                for (Venue venue : venues)
                    markerIcons.add(BitmapDescriptorFactory.fromBitmap(ImageHelper.
                            createMapIcon(context, String.valueOf(venue.getPinPopCount()))));

        if(venues.size() > 0){
            lastLoadedItems = venues;
            VenueORM.replaceVenuesFromDataBaseValues(venues);
            test(venues, "loadInBackground - 3");
            VenueORM.getCountVenuesFromDataBase();
        }else {
            lastLoadedItems = previousItems;
        }
        return lastLoadedItems;
    }

    public ArrayList<BitmapDescriptor> getMarkerIcons() {
        return markerIcons;
    }

    public boolean isNoInternet() {
        return noInternet;
    }

    public boolean hasNextPage() {
        return hasNextPage;
    }
}
