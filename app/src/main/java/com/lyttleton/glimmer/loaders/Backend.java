package com.lyttleton.glimmer.loaders;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.AsyncTask;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.lyttleton.glimmer.App;
import com.lyttleton.glimmer.DataAccessObject.PinPopORM;
import com.lyttleton.glimmer.DataAccessObject.UserORM;
import com.lyttleton.glimmer.DataAccessObject.VenueORM;
import com.lyttleton.glimmer.DataAccessObject.WinkORM;
import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.activities.BaseActivity;
import com.lyttleton.glimmer.activities.IntroActivity;
import com.lyttleton.glimmer.adapters.Chat;
import com.lyttleton.glimmer.adapters.PinPop;
import com.lyttleton.glimmer.adapters.User;
import com.lyttleton.glimmer.adapters.Venue;
import com.lyttleton.glimmer.adapters.Wink;
import com.lyttleton.glimmer.fragments.FeedListFragment;
import com.lyttleton.glimmer.fragments.GenderInterestFragment;
import com.lyttleton.glimmer.fragments.PinPopListFragment;
import com.lyttleton.glimmer.fragments.SignTelephoneFragment;
import com.lyttleton.glimmer.helpers.FileHelper;
import com.lyttleton.glimmer.helpers.ImageHelper;
import com.lyttleton.glimmer.helpers.InternetHelper;
import com.lyttleton.glimmer.helpers.TimeHelper;
import com.lyttleton.glimmer.helpers.ToastHelper;
import com.lyttleton.glimmer.helpers.TwilioHelper;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseCrashReporting;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import bolts.Task;

public class Backend {

    public static final String APPLICATION_ID = "k2OX3po7NB0ozcZphLbQfMWKPNmkQodzZbBPxU1S";
    public static final String CLIENT_KEY = "1wnvilE33hY82nSRAUXG6cXqjsEA5He78JJ0lCpu";

    public static final String VENUES_TABLE = "Venues";
    public static final String PIN_POP_TABLE = "PinPop";
    public static final String USER_TABLE = "_User";
    public static final String REQUESTS_TABLE = "Requests";
    public static final String WINKS_TABLE = "Winks";
    public static final String CHAT_TABLE = "Chat";
    public static final String TERMS_OF_SERVICE_TABLE = "Content";

    public static final String TU_NAME_COLUMN = "name";
    public static final String TU_GENDER_COLUMN = "gender";
    public static final String TU_INTEREST_GENDER_COLUMN = "Interest";
    public static final String USER_COLUMN = "user";
    public static final String CREATED_AT_COLUMN = "createdAt";
    public static final String VENUE_ID_COLUMN = "venueId";
    public static final String DISPLAY_ADDRESS_COLUMN = "displayAddress";
    public static final String LOCATION_COLUMN = "location";
    public static final String TYPE_COLUMN = "type";
    public static final String USER_ID_COLUMN = "userId";
    public static final String OBJECT_ID_COLUMN = "objectId";
    public static final String VENUE_REQUESTED_COLUMN = "venueRequested";
    public static final String RECEIVER_ID_COLUMN = "receiverId";
    public static final String CLUE_COLUMN = "clue";
    public static final String PROFILE_IMAGE_COLUMN = "profileImage0";
    public static final String MESSAGE_COLUMN = "message";
    public static final String USER_PHOTOS_COLUMN = "userPhotos";
    public static final String PIN_POP_COUNT_COLUMN = "pinPopCount";
    public static final String TIME_STAMP_COLUMN = "timestamp";
    public static final String TU_PREFIX = "prefix";
    public static final String TU_NUMBER_PHONE = "telephone";
    public static final String TU_STATUS = "status";

    public static final String FEMALES = "Females";
    public static final String MALES = "Males";
    public static final String FEMALE = "female";
    public static final String MALE = "male";

    public static final int GENERAL_ERROR_CODE = 0;
    public static final int CONNECTION_FAILED_ERROR_CODE = 100;
    public static final int OBJECT_NOT_FOUND_ERROR_CODE = 101;

    public static final int MAKE_PIN_HOURS_INTERVAL = 1;
    public static final int MADE_POP_MINUTES_INTERVAL = 5;

    private static final int MAKE_WINK_HOURS_INTERVAL = 1;
    public static final int MALE_WINKS_PER_HOUR_LIMIT = 3;

    public static final int USER_PHOTOS_COUNT = 4;
    public static final int FACEBOOK_PHOTOS_COUNT = 4;
    public static final int LOAD_ITEMS_LIMIT = 32;
    public static final int LOAD_VENUES_LIMIT = 100;

    public static final String MEDIA_FILE_NAME = "media";
    public static final String MEDIA_COLUMN = "media";
    public static final String USER_PHOTO_NAME = "photo";
    public static final String SEARCH_STRING_PARAM = "searchString";
    public static final String USER_GENDER_PARAM = "userGender";
    public static final String USER_INTEREST_GENDER_PARAM = "userInterestGender";
    public static final String SOUND_KEY = "sound";
    public static final String DEFAULT = "default";
    public static final String SAVE_CHAT_MESSAGE_FUNC = "saveChatMessage";
    public static final String RECEIVER_NAME_PARAM = "receiverName";
    public static final String START_OFFSET_PARAM = "startOffset";
    public static final String MUTUAL_FRIENDS_COUNT = "mutualFriendsCount";
    public static final String UPLOADED_DAILY_PROFILER = "uploadedDailyProfiler";
    public static final String CONTENT_AVAILABLE = "content-available";

    public static int PIN_TYPE = 0;
    public static int POP_TYPE = 1;

    public static final String PARSE_PUSH_DATA = "com.parse.Data";
    public static final String ALERT_KEY = "alert";

    public static final List<String> FACEBOOK_PERMISSION = Arrays.asList("email", /*"user_birthday",*/
            "user_photos", "user_friends");
    public static final String FACEBOOK_FIELDS = "first_name, gender, email, picture";

    public static final String PARSE_PUSH_ACTION = "com.parse.push.intent.RECEIVE";
    public static final String PUSH_BROADCAST_ACTION = "com.lyttleton.glimmer.PUSH_BROADCAST";

    public static final String GET_VENUES_CLOUD_FUNC = "getVenues";
    public static final String SAVE_PIN_POP_FUNC = "makePinPop";
    public static final String SAVE_WINK_FUNC = "makeWink";

    public static final int PARSE_OR_CLOSURES_LIMIT = 10;

    public static void initialize(Context context) {
        ParseCrashReporting.enable(context);
        Parse.initialize(context, APPLICATION_ID, CLIENT_KEY);
//        ParseFacebookUtils.initialize(context);
        ParseInstallation.getCurrentInstallation().saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                App.logOut("INTALATION", (String) ParseInstallation.getCurrentInstallation().get("deviceToken"));
            }
        });
    }

//    public static void onActivityResult(int requestCode, int resultCode, Intent data) {
////        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
//    }

    public final static String CODE = "CODE";

    public static void signUpUser(final Activity activity, HashMap<String, String> userInfo, final ResponseListener listener){

        BaseActivity.blockOrientationChange(activity);

        final ParseUser currentUser = new ParseUser();
        currentUser.setUsername((userInfo.get(CODE)));
        currentUser.setEmail(userInfo.get(CODE) + "@droid.com");
        currentUser.setPassword(userInfo.get(CODE));

        currentUser.put(TU_NAME_COLUMN, userInfo.get(TU_NAME_COLUMN));
        currentUser.put(TU_INTEREST_GENDER_COLUMN, userInfo.get(TU_INTEREST_GENDER_COLUMN));
        currentUser.put(TU_GENDER_COLUMN, userInfo.get(TU_GENDER_COLUMN));
//        currentUser.put(TU_PREFIX, userInfo.get(TU_PREFIX));
//        currentUser.put(TU_NUMBER_PHONE, userInfo.get(TU_NUMBER_PHONE));
        currentUser.put(TU_STATUS, IntroActivity.CREATED);
        currentUser.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    BaseActivity.restoreOrientationChange(activity);
                    listener.onSuccess(currentUser);
                } else {
                    BaseActivity.restoreOrientationChange(activity);
                    listener.onError(e.getMessage(), e.getCode());
                }
            }
        });
    }

    public static void addedNumberPhone(final String prefix, final String numberPhone
            , final Response responseListener){
        final ParseUser currentUser = getCurrentUser();
        final ParseQuery query = ParseQuery.getQuery(USER_TABLE);
        query.getInBackground(currentUser.getObjectId(), new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if(e==null){
                    if(parseObject!=null){
                        parseObject.put(TU_PREFIX, "+" + prefix);
                        parseObject.put(TU_NUMBER_PHONE, prefix + numberPhone);
                        parseObject.put(TU_STATUS, IntroActivity.PENDING);
                        currentUser.put(TU_STATUS, IntroActivity.PENDING);
                        parseObject.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null)
                                    responseListener.onSuccess();
                                else
                                    responseListener.onError("Error", -2);
                            }
                        });
                    }else
                        responseListener.onError("Error! User not found..", -2);
                }
            }
        });
    }

    public static void verifyNumberPhone(){
        final ParseUser currentUser = getCurrentUser();
        final ParseQuery query = ParseQuery.getQuery(USER_TABLE);
        query.getInBackground(currentUser.getObjectId(), new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if(e==null){
                    if(parseObject != null){
                        parseObject.put(TU_STATUS, IntroActivity.VERIFIED);
                        currentUser.put(TU_STATUS, IntroActivity.VERIFIED);
                        try {
                            parseObject.save();
                        } catch (ParseException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    public static void addedImageProfile(final String uri, final Response listener){
        final ParseUser currentUser = getCurrentUser();
        final ParseQuery query = ParseQuery.getQuery(USER_TABLE);
        query.getInBackground(currentUser.getObjectId(), new GetCallback<ParseObject>() {
            @Override
            public void done(final ParseObject parseObject, ParseException e) {
                if(e==null){
                    if(parseObject != null){
                        ParseFile parseFile = null;
                        try {
                            parseFile = new ParseFile(
                                    USER_PHOTO_NAME + "." + FileHelper.getFileExtension(uri)
                                    , getByteArrayImageFile(uri));
                        }catch (Exception exception){
                            Log.d("glimmer", "..Exception.." + exception.getMessage());
                        }
                        if(parseFile == null) return;
                        parseObject.put(PROFILE_IMAGE_COLUMN + "0", parseFile);
                        Log.d("glimmer", "..full fields is created");
                        parseObject.put(UPLOADED_DAILY_PROFILER, true);
                        parseObject.put(TU_STATUS, IntroActivity.LIVE);
                        currentUser.put(TU_STATUS, IntroActivity.LIVE);
                        parseObject.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if(e==null){
                                    ParseInstallation parseInstallation = ParseInstallation.getCurrentInstallation();
                                    parseInstallation.put("user", parseObject);
                                    parseInstallation.put("badge", 0);
                                    parseInstallation.saveInBackground();
                                    listener.onSuccess();
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    public static void addedImageProfiles(final String[] uriArray){
        final ParseUser currentUser = getCurrentUser();
        final ParseQuery query = ParseQuery.getQuery(USER_TABLE);
        query.getInBackground(currentUser.getObjectId(), new GetCallback<ParseObject>() {
            @Override
            public void done(final ParseObject parseObject, ParseException e) {
                if(e==null){
                    if(parseObject != null){
                        for (int i=0, o=uriArray.length; i<o;i++) {
                            if(uriArray[i] != null) {
                                int count = countEmptyPosition();
                                if(count>=0) {
                                    uploadParseUserFile(parseObject, uriArray[i], count);
                                }else{
                                    uploadParseUserFile(parseObject, uriArray[i], i);
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    public static void addedImageProfiles386(final String[] uriArray, final onCallbackIosProgressDismiss result){
        final ParseUser currentUser = getCurrentUser();
        final ParseQuery query = ParseQuery.getQuery(USER_TABLE);
        query.getInBackground(currentUser.getObjectId(), new GetCallback<ParseObject>() {
            @Override
            public void done(final ParseObject parseObject, ParseException e) {
                if(e==null){
                    if(parseObject != null){
                        for (int i=0, o=uriArray.length; i<o;i++) {
                            if(uriArray[i] != null) {
                                uploadParseUserFile(parseObject, uriArray[i], i);
                                result.result();
                            }else{
                                result.result();
                            }
                        }
                    }
                }
            }
        });
    }

    public interface onCallbackIosProgressDismiss {
        void result();
    }

    private static int countEmptyPosition(){
        for(int i=0, l=3; i<l; i++){
            if(getCurrentUser().getParseFile(PROFILE_IMAGE_COLUMN + i) == null) return i;
        }
        return -2;
    }

    private static byte[] getByteArrayImageFile(String url){
        Bitmap bitmap = ImageHelper.decodeSampledBitmapFromPath(url, 799);  // 860 Size big
        ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
        assert bitmap != null;
        bitmap.compress(Bitmap.CompressFormat.PNG, 75, stream2);
        byte[] byteArray = stream2.toByteArray();
        return byteArray;
    }

    private static void uploadParseUserFile(ParseObject parseObject, String url, int position) {
        ParseFile parseFile = null;
        try {
            parseFile = new ParseFile(USER_PHOTO_NAME + "." +
                    FileHelper.getFileExtension(url), getByteArrayImageFile(url));
        } catch (Exception exception){
            if (App.logger) Log.d("glimmer", "..Exception.." + exception.getMessage());
        }
        if (parseFile == null) return;

        parseObject.put(PROFILE_IMAGE_COLUMN + position, parseFile);
        if (App.logger) Log.d("glimmer", "..full fields is created");
        final int finalI = position;
        parseObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    try {
                        fetchCurrentUser();
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }
                    if (App.logger)
                        Log.d("glimmer", "url " + finalI + " is uploaded...");
                }
            }
        });
    }

    public static void sendVerificationTelephoneNumber(Context context
            , final String telephoneNumber
            , final String verificationCode
            , final SignTelephoneFragment.ResponseListener listener){

        if(!InternetHelper.checkInternetConnection(context)){
            listener.error(context.getString(R.string.check_your_internet_connection));
            return;
        }

        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        SmsManager smsManager = SmsManager.getDefault();

        PendingIntent sentIntent = PendingIntent.getBroadcast(context, 0, new Intent(SENT), 0);
        PendingIntent deliveryIntent = PendingIntent.getBroadcast(context, 0, new Intent(DELIVERED), 0);

//        try{
//            smsManager.sendTextMessage("+" + telephoneNumber, null, "Glimmer verification code: " +
//                    verificationCode, sentIntent, deliveryIntent);
//            Toast.makeText(context, "Message Sent", Toast.LENGTH_LONG).show();
//        } catch (Exception ex) {
//            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_LONG).show();
//            ex.printStackTrace();
//        }
        new AsyncTask<Void, Void, JSONObject>() {
            @Override
            protected JSONObject doInBackground(Void... params) {
                JSONObject response = null;
                try {

                    response = new JSONObject(TwilioHelper.sendSms("+" + telephoneNumber
                            , "Glimmer verification code: " +
                            verificationCode));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                try {
                    if (jsonObject.has(TwilioHelper.ERROR_MESSAGE))
                        listener.error("ERROR! send sms : "
                                + jsonObject.getString(TwilioHelper.ERROR_MESSAGE));
                    else {

                        listener.success();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.error("ERROR! send sms : " + e.getMessage());
                }
            }
        }.execute();
    }

    public static void logout() {
        ParseUser.logOut();
        ParseInstallation parseInstallation = ParseInstallation.getCurrentInstallation();
        parseInstallation.remove(USER_COLUMN);
        parseInstallation.saveEventually();
    }

    public static boolean hasCurrentUser() {
        return getCurrentUser() != null;
    }

//    public static boolean isUserSelectedInterestGender() {
//        return !TextUtils.isEmpty(getCurrentUser().getString(TU_INTEREST_GENDER_COLUMN));
//    }

    public static boolean isUserStatusCreated() {
        return TextUtils.equals(getCurrentUser().getString(TU_STATUS), IntroActivity.CREATED);
    }

    public static boolean isUserStatusPending() {
        return TextUtils.equals(getCurrentUser().getString(TU_STATUS), IntroActivity.PENDING);
    }

    public static boolean isUserStatusVerified() {
        return TextUtils.equals(getCurrentUser().getString(TU_STATUS), IntroActivity.VERIFIED);
    }

    public static boolean isUserStatusLive() {
        return TextUtils.equals(getCurrentUser().getString(TU_STATUS), IntroActivity.LIVE);
    }

    public static void saveUserLocation(Location location) {
        if (location != null) {
            ParseGeoPoint parseGeoPoint = new ParseGeoPoint(location.getLatitude(),
                    location.getLongitude());
            ParseUser parseUser = getCurrentUser();
            parseUser.put(LOCATION_COLUMN, parseGeoPoint);
            parseUser.saveEventually();
        }
    }

    public static void fetchCurrentUserInBackground(final Activity activity, final ResponseListener listener) {
        ParseUser user = getCurrentUser();
        BaseActivity.blockOrientationChange(activity);
        user.fetchInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if (e == null) listener.onSuccess(parseObject);
                else {
                    String errorMessage = e.getMessage();
                    if (e.getCode() == OBJECT_NOT_FOUND_ERROR_CODE) {
                        errorMessage = activity.getString(R.string.deleted_account);
                        logout();
                    }
                    listener.onError(errorMessage, e.getCode());
                }
                BaseActivity.restoreOrientationChange(activity);
            }
        });
    }

    public static void fetchCurrentUser() throws ParseException {
        ParseUser user = getCurrentUser();
        user.fetch();
    }

    public static List<Venue> loadPopularVenues(String searchString, int startOffset) throws ParseException {
        List<Venue> venues = new ArrayList<>();

        HashMap<String, Object> params = new HashMap<>();
        params.put(SEARCH_STRING_PARAM, searchString);
        params.put(USER_GENDER_PARAM, getCurrentUser().getString(TU_GENDER_COLUMN));
        params.put(USER_INTEREST_GENDER_PARAM, getCurrentUser().getString(TU_INTEREST_GENDER_COLUMN));
        params.put(SEARCH_STRING_PARAM, searchString);
        params.put(START_OFFSET_PARAM, startOffset);

        List<ParseObject> parseObjects = ParseCloud.callFunction(GET_VENUES_CLOUD_FUNC, params);
        Venue venue;
        for (ParseObject parseObject : parseObjects) {
            venue = makeVenue(parseObject);
            if (venue != null) {
                if (parseObject.has(PIN_POP_COUNT_COLUMN))
                    venue.setPinPopCount(parseObject.getInt(PIN_POP_COUNT_COLUMN));
                venues.add(venue);
            }
        }
        VenueORM.replaceVenuesFromDataBaseValues(venues);
        return venues;
    }

    public static void searchVenue(String venueName, final Activity activity,
                                   final ResponseListener listener) {
        BaseActivity.blockOrientationChange(activity);
        ParseQuery<ParseObject> query = new ParseQuery<>(VENUES_TABLE);
        query.whereMatches(TU_NAME_COLUMN, venueName, "i");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                BaseActivity.restoreOrientationChange(activity);
                if (e == null) {
                    if (parseObjects.size() > 0) listener.onSuccess(parseObjects.get(0));
                    else listener.onError("Venue not found", OBJECT_NOT_FOUND_ERROR_CODE);
                } else listener.onError(e.getMessage(), e.getCode());
            }
        });
    }

    public static Venue makeVenue(ParseObject parseObject) {
        ParseGeoPoint parseGeoPoint = parseObject.getParseGeoPoint(LOCATION_COLUMN);
        Venue venue = null;
        List<String> userPhotos = parseObject.getList(USER_PHOTOS_COLUMN);

        ArrayList<String> userPhotosShortList = null;
        if (userPhotos != null) {
            if (userPhotos.size() > 5) userPhotosShortList = new ArrayList<>(userPhotos.subList(0, 5));
            else userPhotosShortList = new ArrayList<>(userPhotos);
        }

//        Log.d("makeVenue", parseObject.getString(TU_NAME_COLUMN) + ", Photos List size: " + userPhotosShortList.size());

        if (parseGeoPoint != null) {
            venue = new Venue(parseObject.getObjectId(),
                    parseObject.getString(TU_NAME_COLUMN),
                    parseObject.getString(DISPLAY_ADDRESS_COLUMN),
                    parseGeoPoint.getLatitude(),
                    parseGeoPoint.getLongitude(),
//                    userPhotos != null ? new ArrayList<>(userPhotos) : null);
                     userPhotosShortList);
        }
        return venue;
    }

    public static List<PinPop> loadPinPopList(int loadType, Venue venue, PinPopLoader loader,
                                              int startOffset, List<PinPop> previousItems) throws ParseException {
        ParseQuery<ParseObject> pinPopQuery = new ParseQuery<>(PIN_POP_TABLE);

        //Gender filter
        String interestGender = getCurrentUser().getString(TU_INTEREST_GENDER_COLUMN);
        if (interestGender == null) return null;

        String interestGenderConverted = getUserGenderInterestConverted();

        String currentUserGender = getCurrentUser().getString(TU_GENDER_COLUMN);
        boolean isUserNormal = true;
        if (currentUserGender != null)
                isUserNormal = !currentUserGender.equals(interestGenderConverted);

        //Time filter
        if (loadType == PinPopListFragment.TODAY_TYPE) {
            Date dateLessThan = TimeHelper.getCalendarDay(0).getTime(),
                    dateGreaterThan = TimeHelper.getCalendarDay(-1).getTime();
            pinPopQuery.whereLessThan(CREATED_AT_COLUMN, dateLessThan);
            pinPopQuery.whereGreaterThan(CREATED_AT_COLUMN, dateGreaterThan);
        } /*else if (loadType == PinPopListFragment.EVER_TYPE)
            pinPopQuery.whereEqualTo(TYPE_COLUMN, POP_TYPE);*/

        //Venue filter
        pinPopQuery.whereEqualTo(VENUE_ID_COLUMN, venue.getObjectId());

        //Sort by createdAt date
        pinPopQuery.orderByDescending(CREATED_AT_COLUMN);

        //Paging
        pinPopQuery.setSkip(startOffset);
        pinPopQuery.setLimit(LOAD_ITEMS_LIMIT);

        List<PinPop> pinPopList = new ArrayList<>();

        List<ParseObject> pinPopParseObjects = pinPopQuery.find();

        if (pinPopParseObjects.size() < LOAD_ITEMS_LIMIT) loader.setHasNextPage(false);
        else loader.setHasNextPage(true);
        loader.setLoadingStartOffset(startOffset + pinPopParseObjects.size());

        // Load users
        HashSet<String> userIds = new HashSet<>();
        for (ParseObject pinPopParseObject : pinPopParseObjects) {
            userIds.add(pinPopParseObject.getString(USER_ID_COLUMN));
        }

        ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
        userQuery.whereEqualTo(TU_STATUS, IntroActivity.LIVE);
        userQuery.whereContainedIn(OBJECT_ID_COLUMN, userIds);
        List<ParseUser> userParseObjects = userQuery.find();

//        List<ParseQuery<ParseObject>> matchQueries = new ArrayList<>();

        String userId;
        int pinPopType;
        for (ParseObject pinPopParseObject : pinPopParseObjects) {
            userId = pinPopParseObject.getString(USER_ID_COLUMN);
            User user = null;
            for (ParseUser parseUser : userParseObjects) {

//                if (parseUser.getObjectId() == null) Log.d("loadPinPopList", "parseUser.getObjectId() == null");
//                if (parseUser.getString(TU_GENDER_COLUMN) == null) Log.d("loadPinPopList", "parseUser.getString(TU_GENDER_COLUMN) == null");
//                if (userId == null) Log.d("loadPinPopList", "userId == null");

                if (parseUser.getString(TU_GENDER_COLUMN) != null
                        && parseUser.getString(TU_INTEREST_GENDER_COLUMN) != null) {
                    if (parseUser.getObjectId().equals(userId) && parseUser.getString(TU_GENDER_COLUMN).equals(interestGenderConverted)) {
                        if (isUserNormal || parseUser.getString(TU_INTEREST_GENDER_COLUMN).equals(interestGender)) {
                            user = makeUser(parseUser);
                        }
                        break;
                    }
                }
            }

            pinPopType = pinPopParseObject.getInt(TYPE_COLUMN);

            if (user != null
                    && !hasPinPopWithUser(previousItems, pinPopType, user.getObjectId())
                    && !hasPinPopWithUser(pinPopList, pinPopType, user.getObjectId())) {

                pinPopList.add(new PinPop(pinPopType,
                        pinPopParseObject.getCreatedAt(), user, venue, pinPopParseObject.getObjectId()));
                UserORM.addUser(user);

                // Check if user matched
//                ParseQuery<ParseObject> matchQuery1 = new ParseQuery<>(WINKS_TABLE);
//                matchQuery1.whereEqualTo(USER_ID_COLUMN, getCurrentUser().getObjectId());
//                matchQuery1.whereEqualTo(RECEIVER_ID_COLUMN, user.getObjectId());
//                matchQuery1.whereEqualTo(TYPE_COLUMN, Wink.CLUE_TYPE);
//                matchQuery1.whereEqualTo(CLUE_COLUMN, null);
//
//                ParseQuery<ParseObject> matchQuery2 = new ParseQuery<>(WINKS_TABLE);
//                matchQuery2.whereEqualTo(USER_ID_COLUMN, user.getObjectId());
//                matchQuery2.whereEqualTo(RECEIVER_ID_COLUMN, getCurrentUser().getObjectId());
//                matchQuery2.whereEqualTo(TYPE_COLUMN, Wink.CLUE_TYPE);
//                matchQuery2.whereEqualTo(CLUE_COLUMN, null);
//
//                matchQueries.add(ParseQuery.or(Arrays.asList(matchQuery1, matchQuery2)));
            }
        }

        // Check if users matched, Parse has limit to 10 or closures
//        if (matchQueries.size() > 0) {
//            ParseQuery<ParseObject> matchQuery = ParseQuery.or(matchQueries);
//            List<ParseObject> matchWinks = matchQuery.find();
//            for (PinPop pinPop : pinPopList)
//                if (winksHasUser(matchWinks, pinPop.getUser())) pinPop.getUser().setMatched(true);
//        }

        PinPopORM.replaceAllPinPops(pinPopList);

        return pinPopList;
    }

    private static boolean hasPinPopWithUser(List<PinPop> pinPopList, int pinPopType, String userId){
        if (pinPopList != null)
            for (PinPop pinPop : pinPopList)
                if (pinPop.getType() == pinPopType && pinPop./*getUser().getObjectId()*/getUserId().equals(userId))
                    return true;
        return false;
    }

    public static List<PinPop> loadPinPopListOfUser(int fragmentType, PinPopLoader loader,
                                                    int startOffset) throws ParseException {
        ParseQuery<ParseObject> pinPopQuery = new ParseQuery<>(PIN_POP_TABLE);

        pinPopQuery.whereEqualTo(USER_ID_COLUMN, getCurrentUser().getObjectId());
        pinPopQuery.whereEqualTo(TYPE_COLUMN, fragmentType);

        //Sort by createdAt date
        pinPopQuery.orderByDescending(CREATED_AT_COLUMN);

        pinPopQuery.setSkip(startOffset);
        pinPopQuery.setLimit(LOAD_ITEMS_LIMIT);

        List<PinPop> pinPopList = new ArrayList<>();

        List<ParseObject> pinPopParseObjects = pinPopQuery.find();

        if (pinPopParseObjects.size() < LOAD_ITEMS_LIMIT) loader.setHasNextPage(false);
        else loader.setHasNextPage(true);
        loader.setLoadingStartOffset(startOffset + pinPopParseObjects.size());

        HashSet<String> venueIds = new HashSet<>();
        for (ParseObject pinPopParseObject : pinPopParseObjects)
            venueIds.add(pinPopParseObject.getString(VENUE_ID_COLUMN));

        ParseQuery<ParseObject> venueQuery = new ParseQuery<>(VENUES_TABLE);
        venueQuery.whereContainedIn(OBJECT_ID_COLUMN, venueIds);
        List<ParseObject> venueParseObjects = venueQuery.find();

        String venueId;
        User user = makeUser(getCurrentUser());
        for (ParseObject pinPopParseObject : pinPopParseObjects) {
            venueId = pinPopParseObject.getString(VENUE_ID_COLUMN);
            Venue venue = null;
            for (ParseObject venueParseObject : venueParseObjects)
                if (venueParseObject.getObjectId().equals(venueId)) {
                    venue = makeVenue(venueParseObject);
                    break;
                }
            if (venue != null) pinPopList.add(new PinPop(pinPopParseObject.getInt(TYPE_COLUMN),
                    pinPopParseObject.getCreatedAt(), user, venue, pinPopParseObject.getObjectId()));
        }

        return pinPopList;
    }

    private static boolean winksHasUser(List<ParseObject> winks, User user){
        for (ParseObject parseObject : winks)
            if (parseObject.getString(USER_ID_COLUMN).equals(user.getObjectId())
                    || parseObject.getString(RECEIVER_ID_COLUMN).equals(user.getObjectId())) {
                return true;
            }
        return false;
    }

    public static User makeUser(ParseUser parseUser) {
        ArrayList<String> photoUrls = new ArrayList<>();
        ParseFile parseFile;
        for (int i = 0; i < USER_PHOTOS_COUNT; i++) {
            parseFile = parseUser.getParseFile(PROFILE_IMAGE_COLUMN + i);
            if (parseFile != null && parseFile.getUrl() != null) photoUrls.add(parseFile.getUrl());
        }
        return new User(parseUser.getObjectId(), parseUser.getString(TU_NAME_COLUMN),
                parseUser.getString(TU_GENDER_COLUMN), photoUrls,
                parseUser.getBoolean(UPLOADED_DAILY_PROFILER));
    }

    public static String getUserGenderInterestConverted() {
        String interestGender;
        if (getCurrentUser().getString(TU_INTEREST_GENDER_COLUMN).equals(FEMALES))
            interestGender = FEMALE;
        else interestGender = MALE;
        return interestGender;
    }

    private static boolean hasUser(String objectId, List<PinPop> pinPopList) {
        for (PinPop pinPop : pinPopList)
            if (pinPop.getUser().getObjectId().equals(objectId)) return true;
        return false;
    }

    public static void saveWink(int type, final String clue, final User selectedUser,
                                final Venue venue, final Activity activity,
                                final ResponseListener listener){

        HashMap<String, Object> params = new HashMap<>();
        params.put(USER_ID_COLUMN, getCurrentUser().getObjectId());
        params.put(RECEIVER_ID_COLUMN, selectedUser.getObjectId());
        params.put(TYPE_COLUMN, type);
        params.put(CLUE_COLUMN, !TextUtils.isEmpty(clue) ? clue : null);
        params.put(VENUE_ID_COLUMN, venue.getObjectId());
        params.put(TU_GENDER_COLUMN, getCurrentUser().getString(TU_GENDER_COLUMN));
        params.put(TU_NAME_COLUMN, selectedUser.getFirstName());

        final int finalType;
        if (type == Wink.CLUE_TYPE && TextUtils.isEmpty(clue)) finalType = Wink.MATCH_TYPE;
        else finalType = type;

        ParseCloud.callFunctionInBackground(SAVE_WINK_FUNC, params,
                new FunctionCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                        BaseActivity.restoreOrientationChange(activity);
                        if (listener != null) {
                            if (e == null) {
                                sendWinkPush(selectedUser, venue, activity, clue, parseObject, finalType);

                                if (finalType == Wink.MATCH_TYPE)
                                    FeedListFragment.sendMatchBroadcast(parseObject.getObjectId(), venue,
                                            selectedUser, finalType, activity);
                                listener.onSuccess(parseObject);
                            } else listener.onError(e.getMessage(), e.getCode());
                        }
                    }
                });
    }

    public static ArrayList<Wink> loadMatches(int startOffset, WinksLoader loader) throws ParseException {
        final String currentUserId = getCurrentUser().getObjectId();

        // Load matches Parse objects
        ParseQuery<ParseObject> matchQuery1 = new ParseQuery<>(WINKS_TABLE);
        matchQuery1.whereEqualTo(USER_ID_COLUMN, currentUserId);
        matchQuery1.whereEqualTo(TYPE_COLUMN, Wink.CLUE_TYPE);
        matchQuery1.whereEqualTo(CLUE_COLUMN, null);

        ParseQuery<ParseObject> matchQuery2 = new ParseQuery<>(WINKS_TABLE);
        matchQuery2.whereEqualTo(RECEIVER_ID_COLUMN, currentUserId);
        matchQuery2.whereEqualTo(TYPE_COLUMN, Wink.CLUE_TYPE);
        matchQuery2.whereEqualTo(CLUE_COLUMN, null);

        ParseQuery<ParseObject> matchQuery = ParseQuery.or(Arrays.asList(matchQuery1, matchQuery2));
        matchQuery.setSkip(startOffset);
        matchQuery.setLimit(PARSE_OR_CLOSURES_LIMIT);
        List<ParseObject> matchesParseObjects = matchQuery.find();

        ArrayList<Wink> matches = new ArrayList<>();

        if (matchesParseObjects.size() < PARSE_OR_CLOSURES_LIMIT) loader.setHasNextPage(false);
        else loader.setHasNextPage(true);

        if (matchesParseObjects != null && matchesParseObjects.size() > 0) {
            loader.setLoadingStartOffset(loader.getLoadingStartOffset() + matchesParseObjects.size());

            ArrayList<String> userIds = new ArrayList<>(), venueIds = new ArrayList<>();
            String matchedUserId = null;
            for (ParseObject parseObject : matchesParseObjects) {
                if (!parseObject.getString(USER_ID_COLUMN).equals(currentUserId))
                    matchedUserId = parseObject.getString(USER_ID_COLUMN);
                else if (!parseObject.getString(RECEIVER_ID_COLUMN).equals(currentUserId))
                    matchedUserId = parseObject.getString(RECEIVER_ID_COLUMN);
                if (!TextUtils.isEmpty(matchedUserId)) {
                    userIds.add(matchedUserId);
                    venueIds.add(parseObject.getString(VENUE_ID_COLUMN));
                }
            }

            // Load users
            ParseQuery<ParseUser> usersQuery = ParseUser.getQuery();
            usersQuery.whereContainedIn(OBJECT_ID_COLUMN, userIds);
            List<ParseUser> parseUsers = usersQuery.find();
            List<User> users = new ArrayList<>();
            for (ParseUser parseUser : parseUsers) users.add(makeUser(parseUser));
            // Added user in to dataBase with validation
            UserORM.replaceUsersWithValidation(users);

            // Load venues
            ParseQuery<ParseObject> venuesQuery = new ParseQuery<>(VENUES_TABLE);
            venuesQuery.whereContainedIn(OBJECT_ID_COLUMN, venueIds);
            List<ParseObject> parseVenues = venuesQuery.find();
            List<Venue> venues = new ArrayList<>();
            for (ParseObject parseObject : parseVenues) venues.add(makeVenue(parseObject));
            // Added venue in to database with validation

//            VenueORM.replaceVenuesFromDataBaseValues(venues);

            // Make matches
            Venue venue;
            User user;
            Wink match;
            int i = 0;
            for (ParseObject parseObject : matchesParseObjects) {
                venue = findVenue(venueIds.get(i), venues);
                user = findUser(userIds.get(i), users);
                if (venue != null && user != null) {
                    match = makeWink(parseObject, venue, user);
                    match.setType(Wink.MATCH_TYPE);
                    matches.add(match);
                }
                i ++;
            }
        }

        // Added user in to dataBase with validation
        WinkORM.replaceAllWinks(matches);

        return matches;
    }

    public static List<Wink> loadWinks(int type, int startOffset, WinksLoader loader,
                                       List<Wink> previousWinks) throws ParseException {
        List<Wink> winks = new ArrayList<>();

        ParseQuery<ParseObject> allWinksQuery = new ParseQuery<>(WINKS_TABLE);
        allWinksQuery.whereEqualTo(RECEIVER_ID_COLUMN, getCurrentUser().getObjectId());
        allWinksQuery.orderByDescending(CREATED_AT_COLUMN);

        // Duplicate check
        if (previousWinks != null) {
            HashSet<String> matchUserIds = new HashSet<>();
            for (Wink wink : previousWinks)
                if (wink.getType() == Wink.MATCH_TYPE)
                    matchUserIds.add(wink.getUser().getObjectId());
            allWinksQuery.whereNotContainedIn(USER_ID_COLUMN, matchUserIds);
        }

        // Paging
        allWinksQuery.setSkip(startOffset);
        allWinksQuery.setLimit(PARSE_OR_CLOSURES_LIMIT);

        List<ParseObject> allWinksParseObjects = allWinksQuery.find();

        if (allWinksParseObjects.size() < PARSE_OR_CLOSURES_LIMIT) loader.setHasNextPage(false);
        else loader.setHasNextPage(true);
        loader.setLoadingStartOffset(startOffset + allWinksParseObjects.size());

        // Remove received matches
        Iterator<ParseObject> allWinksIterator = allWinksParseObjects.iterator();
        ParseObject winkFromIterator;
        while (allWinksIterator.hasNext()) {
            winkFromIterator = allWinksIterator.next();
            if (winkFromIterator.getInt(TYPE_COLUMN) == Wink.CLUE_TYPE
                    && !winkFromIterator.has(CLUE_COLUMN))
                allWinksIterator.remove();
        }

        // Get user ids
//        HashSet<String> allUserIds = new HashSet<>();
//        for (ParseObject parseObject : allWinksParseObjects)
//            allUserIds.add(parseObject.getString(USER_ID_COLUMN));

//        // Find matches
//        List<ParseQuery<ParseObject>> parseQueries = new ArrayList<>();
//        for (String userId : allUserIds) {
//            ParseQuery<ParseObject> matchesQuery = new ParseQuery<>(WINKS_TABLE);
//            matchesQuery.whereEqualTo(USER_ID_COLUMN, getCurrentUser().getObjectId());
//            matchesQuery.whereEqualTo(RECEIVER_ID_COLUMN, userId);
//            parseQueries.add(matchesQuery);
//        }
//
//        if (parseQueries.size() > 0) {
//            ParseQuery<ParseObject> matchQuery = ParseQuery.or(parseQueries);
//            List<ParseObject> matchParseObjects = matchQuery.find();
//
//            for (String userId : allUserIds) {
//                if (winksHasReceiverId(matchParseObjects, userId)) {
//                    if (type == FeedListFragment.ALL_TYPE || type == FeedListFragment.MATCHES_TYPE) {
//                        Iterator<ParseObject> iterator = allWinksParseObjects.iterator();
//                        // Change type of first wink and delete all other winks
//                        boolean canDelete = false;
//                        int i = 0;
//                        while (iterator.hasNext()) {
//                            if (iterator.next().getString(USER_ID_COLUMN).equals(userId)) {
//                                if (canDelete) iterator.remove();
//                                else {
//                                    canDelete = true;
//                                    allWinksParseObjects.get(i).put(TYPE_COLUMN, Wink.MATCH_TYPE);
//                                }
//                            }
//                            i++;
//                        }
//                    } else if (type == FeedListFragment.WINKS_TYPE) // Change wink type to WINK_MATCH
//                        for (ParseObject winkParseObject : allWinksParseObjects)
//                            if (winkParseObject.getString(USER_ID_COLUMN).equals(userId))
//                                winkParseObject.put(TYPE_COLUMN, Wink.WINK_MATCH_TYPE);
//                } else if (type == FeedListFragment.MATCHES_TYPE)
//                    removeWinksByUserId(userId, allWinksParseObjects); // Remove winks to show only matches
//            }
//        }

        // Load users
        HashSet<String> userIds = new HashSet<>();
        for (ParseObject parseObject : allWinksParseObjects)
            userIds.add(parseObject.getString(USER_ID_COLUMN));
        ParseQuery<ParseUser> usersQuery = ParseUser.getQuery();
        usersQuery.whereContainedIn(OBJECT_ID_COLUMN, userIds);
        List<ParseUser> parseUsers = usersQuery.find();
        List<User> users = new ArrayList<>();
        for (ParseUser parseUser : parseUsers) users.add(makeUser(parseUser));
        //Added users in to database with validation

        UserORM.replaceUsersWithValidation(users);

        // Load venues
        HashSet<String> venueIds = new HashSet<>();
        for (ParseObject parseObject : allWinksParseObjects)
            venueIds.add(parseObject.getString(VENUE_ID_COLUMN));
        ParseQuery<ParseObject> venuesQuery = new ParseQuery<>(VENUES_TABLE);
        venuesQuery.whereContainedIn(OBJECT_ID_COLUMN, venueIds);
        List<ParseObject> parseVenues = venuesQuery.find();
        List<Venue> venues = new ArrayList<>();
        for (ParseObject parseObject : parseVenues) venues.add(makeVenue(parseObject));
        //Added venues in to database with validation

//        VenueORM.replaceVenuesFromDataBaseValues(venues);

        // Make winks
        Venue venue;
        User user;
        for (ParseObject parseObject : allWinksParseObjects) {
            venue = findVenue(parseObject.getString(VENUE_ID_COLUMN), venues);
            user = findUser(parseObject.getString(USER_ID_COLUMN), users);
            if (venue != null && user != null && parseObject.getInt(TYPE_COLUMN) != Wink.WINK_MATCH_TYPE)
                winks.add(makeWink(parseObject, venue, user));
        }
        //Added winks in to database with validation

        WinkORM.replaceAllWinks(winks);

        return winks;
    }

    private static boolean winksHasReceiverId(List<ParseObject> parseObjects, String receiverId) {
        for (ParseObject parseObject : parseObjects)
            if (parseObject.getString(RECEIVER_ID_COLUMN).equals(receiverId)) return true;
        return false;
    }

    private static void removeWinksByUserId(String userId, List<ParseObject> winks) {
        Iterator<ParseObject> iterator = winks.iterator();
        while (iterator.hasNext())
            if (iterator.next().getString(USER_ID_COLUMN).equals(userId))
                iterator.remove();
    }

    private static User findUser(String objectId, List<User> users) {
        for (User user : users)
            if (user.getObjectId().equals(objectId))
                return user;
        return null;
    }

    private static Venue findVenue(String objectId, List<Venue> venues) {
        for (Venue venue : venues)
            if (venue.getObjectId().equals(objectId))
                return venue;
        return null;
    }

    public static void saveChatMessage(final Chat chat, final Activity activity,
                                       final ResponseListener listener){
        BaseActivity.blockOrientationChange(activity);

        HashMap<String, Object> params = new HashMap<>();
        params.put(USER_ID_COLUMN, getCurrentUser().getObjectId());
        params.put(RECEIVER_ID_COLUMN, chat.getReceiver().getObjectId());
        params.put(MESSAGE_COLUMN, chat.getMessage());
        params.put(RECEIVER_NAME_PARAM, chat.getReceiver().getFirstName());

        ParseCloud.callFunctionInBackground(SAVE_CHAT_MESSAGE_FUNC, params,
                new FunctionCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                        BaseActivity.restoreOrientationChange(activity);
                        if (listener != null) {
                            if (e == null) {
                                listener.onSuccess(parseObject);
                                sendChatPush(chat, activity.getApplicationContext());
                            } else listener.onError(e.getMessage(), e.getCode());
                        }
                    }
                });
    }

    private static void sendPopMatchPush(final Venue venue, final Context context){
        final String currentUserId = getCurrentUser().getObjectId();

        // Check if user matched
        ParseQuery<ParseObject> matchQuery1 = new ParseQuery<>(WINKS_TABLE);
        matchQuery1.whereEqualTo(USER_ID_COLUMN, currentUserId);
        matchQuery1.whereEqualTo(TYPE_COLUMN, Wink.CLUE_TYPE);
        matchQuery1.whereEqualTo(CLUE_COLUMN, null);
        matchQuery1.whereEqualTo(VENUE_ID_COLUMN, venue.getObjectId());

        ParseQuery<ParseObject> matchQuery2 = new ParseQuery<>(WINKS_TABLE);
        matchQuery2.whereEqualTo(RECEIVER_ID_COLUMN, currentUserId);
        matchQuery2.whereEqualTo(TYPE_COLUMN, Wink.CLUE_TYPE);
        matchQuery2.whereEqualTo(CLUE_COLUMN, null);
        matchQuery2.whereEqualTo(VENUE_ID_COLUMN, venue.getObjectId());

        ParseQuery<ParseObject> matchQuery = ParseQuery.or(Arrays.asList(matchQuery1, matchQuery2));
        matchQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null && list.size() > 0) {
                    List<ParseQuery<ParseUser>> userQueries = new ArrayList<>();
                    ParseQuery<ParseUser> userQuery;
                    String matchedUserId = null;
                    for (ParseObject parseObject : list) {
                        if (!parseObject.getString(USER_ID_COLUMN).equals(currentUserId))
                            matchedUserId = parseObject.getString(USER_ID_COLUMN);
                        else if (!parseObject.getString(RECEIVER_ID_COLUMN).equals(currentUserId))
                            matchedUserId = parseObject.getString(RECEIVER_ID_COLUMN);
                        if (!TextUtils.isEmpty(matchedUserId)) {
                            userQuery = ParseUser.getQuery();
                            userQuery.whereEqualTo(OBJECT_ID_COLUMN, matchedUserId);
                            userQueries.add(userQuery);
                        }
                    }

                    if (userQueries.size() > 0) {
                        // Parse has limit to 10 or closures
                        int pushRequestsCount = userQueries.size() / PARSE_OR_CLOSURES_LIMIT;
                        if (userQueries.size() % PARSE_OR_CLOSURES_LIMIT != 0) pushRequestsCount ++;

                        int startIndex, endIndex;
                        for (int i = 0; i < pushRequestsCount; i++){
                            startIndex = i * PARSE_OR_CLOSURES_LIMIT;
                            endIndex = i == pushRequestsCount - 1 ? userQueries.size()
                                    : startIndex + PARSE_OR_CLOSURES_LIMIT;

                            userQuery = ParseQuery.or(userQueries.subList(startIndex, endIndex));
                            ParseQuery pushQuery = ParseInstallation.getQuery();
                            pushQuery.whereMatchesQuery(USER_COLUMN, userQuery);

                            ParsePush push = new ParsePush();
                            push.setQuery(pushQuery);
                            JSONObject data = new JSONObject();
                            try {
                                data.put(TYPE_COLUMN, Wink.POP_TYPE);
                                data.put(USER_ID_COLUMN, getCurrentUser().getObjectId());
                                data.put(SOUND_KEY, DEFAULT);

                                data.put(ALERT_KEY, getCurrentUserFirstName() + " " + context.
                                        getString(R.string.just_arrived_at) + " " + venue.getName()
                                        + " (;");
                                push.setData(data);
                                push.sendInBackground();
                            } catch (JSONException exc) {
                                exc.printStackTrace();
                            }
                        }
                    }
                }
            }
        });
    }

    private static void sendChatPush(Chat chat, Context context) {
        ParseQuery pushQuery = ParseInstallation.getQuery();
        User receiver = chat.getReceiver();
        pushQuery.whereEqualTo(USER_COLUMN, ParseObject.createWithoutData(USER_TABLE,
                receiver.getObjectId()));

        ParsePush push = new ParsePush();
        push.setQuery(pushQuery);
        JSONObject data = new JSONObject();
        try {
            data.put(TYPE_COLUMN, Wink.CHAT_TYPE);
            data.put(OBJECT_ID_COLUMN, chat.getObjectId());
            data.put(USER_ID_COLUMN, getCurrentUser().getObjectId());
            data.put(SOUND_KEY, DEFAULT);
            data.put(CONTENT_AVAILABLE, 1);

            data.put(ALERT_KEY, getCurrentUserFirstName() + " " + context.getString(R.string.
                    has_sent_you_a_message));
            push.setData(data);
            push.sendInBackground();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public interface ResponseListener {
        void onSuccess(ParseObject parseObject);
        void onError(String error, int errorCode);
    }

    public interface Response {
        void onSuccess();
        void onError(String error, int errorCode);
    }

    public static abstract class OnSuccessResponseListener implements ResponseListener {

        private Context context;

        protected OnSuccessResponseListener(Context context) {
            this.context = context;
        }

        @Override
        public void onError(String error, int errorCode) {
            String errorMessage = error;
            if (errorCode == CONNECTION_FAILED_ERROR_CODE)
                errorMessage = context.getString(R.string.check_your_internet_connection);
            ToastHelper.showErrorToast(context, errorMessage);
        }
    }

    public static void saveUserPhoto(byte[] bytes, int index, String fileExtension) {
        ParseUser parseUser = getCurrentUser();
        ParseFile parseFile = new ParseFile(USER_PHOTO_NAME + "." + fileExtension, bytes);
        parseUser.put(PROFILE_IMAGE_COLUMN + index, parseFile);
        parseUser.saveInBackground();
    }

    public static void deleteUserPhotos() throws ParseException {
        ParseUser parseUser = getCurrentUser();
        for (int i = 0; i < USER_PHOTOS_COUNT; i++) parseUser.remove(PROFILE_IMAGE_COLUMN + i);
        parseUser.save();
    }

    private static boolean hasFirstPhoto(ParseObject parseObject){
        return parseObject.getParseFile(PROFILE_IMAGE_COLUMN + "0") != null;
    }

    private static boolean hasPhoto(ParseObject parseObject) {
        for (int i = 0; i < USER_PHOTOS_COUNT; i++)
            if (parseObject.getParseFile(PROFILE_IMAGE_COLUMN + i) != null) return true;
        return false;
    }

//    public static void login(final Activity activity, final ResponseListener listener) {
//        beginAsyncOperation(activity, activity.getString(R.string.logging_in));
//        Task<ParseUser> parseUserTask = ParseFacebookUtils.logInWithReadPermissionsInBackground(activity,
//                FACEBOOK_PERMISSION, new LogInCallback() {
//                    @Override
//                    public void done(final ParseUser user, ParseException err) {
//                        if (user == null) {
//                            int errorCode = GENERAL_ERROR_CODE;
//                            String errorMessage = activity.getString(R.string.need_login_to_facebook);
//                            if (err != null) {
//                                errorCode = err.getCode();
//                                errorMessage = err.toString();
//                            }
//                            listener.onError(errorMessage, errorCode);
//                            finishAsyncOperation(activity, null);
//                        } else {
//                            GraphRequest request = GraphRequest.newMeRequest(
//                                    AccessToken.getCurrentAccessToken(),
//                                    new GraphRequest.GraphJSONObjectCallback() {
//                                        @Override
//                                        public void onCompleted(JSONObject jsonObject, GraphResponse response) {
//                                            try {
//                                                String firstName = jsonObject.getString("first_name"),
//                                                        gender = jsonObject.getString("gender"),
//                                                        email = jsonObject.getString("email");
//                                                user.put(TU_NAME_COLUMN, firstName);
//                                                user.put(TU_GENDER_COLUMN, gender);
//                                                user.setEmail(email);
//                                                user.saveEventually();
//                                                saveCurrentUserToInstallation();
//
//                                                if (!hasPhoto(user))
//                                                    PhotosUploadService.start(activity,
//                                                            jsonObject.toString());
//
//                                                finishAsyncOperation(activity, new AnimationListener() {
//                                                    @Override
//                                                    public void done() {
//                                                        listener.onSuccess(user);
//                                                    }
//                                                });
//                                            } catch (JSONException e) {
//                                                e.printStackTrace();
//                                            }
//                                        }
//                                    });
//                            Bundle parameters = new Bundle();
//                            parameters.putString("fields", FACEBOOK_FIELDS);
//                            request.setParameters(parameters);
//                            request.executeAsync();
//                        }
//                    }
//                });
//    }

    private static void saveCurrentUserToInstallation() {
        ParseInstallation parseInstallation = ParseInstallation.getCurrentInstallation();
        parseInstallation.put(USER_COLUMN, getCurrentUser());
        parseInstallation.saveEventually();
    }

    public static ParseUser getCurrentUser() {
        return ParseUser.getCurrentUser();
    }

    public static void saveUserInterest(int interest) {
        ParseUser parseUser = getCurrentUser();
        parseUser.put(TU_INTEREST_GENDER_COLUMN, interest == GenderInterestFragment.FEMALES ?
                FEMALES : MALES);
        parseUser.saveEventually();
        saveCurrentUserToInstallation();
    }

    public static void beginAsyncOperation(Activity activity, String loadingMessage) {
        BaseActivity.blockOrientationChange(activity);
        View loadingLayout = activity.findViewById(R.id.loading_layout);
        loadingLayout.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(activity, R.anim.fade_in);
        loadingLayout.startAnimation(animation);
        ((TextView) activity.findViewById(R.id.loading_text_tv)).setText(loadingMessage);
    }

    public static void finishAsyncOperation(Activity activity, final AnimationListener listener) {
        BaseActivity.restoreOrientationChange(activity);
        final View loadingLayout = activity.findViewById(R.id.loading_layout);
        Animation animation = AnimationUtils.loadAnimation(activity, R.anim.fade_out);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                loadingLayout.setVisibility(View.GONE);
                if (listener != null) listener.done();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        if (loadingLayout != null) loadingLayout.startAnimation(animation);
    }

    public interface AnimationListener {
        void done();
    }

    public static void savePinPop(final Venue venue, final Activity activity, final int type,
                                  final ResponseListener listener) {

        BaseActivity.blockOrientationChange(activity);

        HashMap<String, Object> params = new HashMap<>();
        params.put(USER_ID_COLUMN, getCurrentUser().getObjectId());
        params.put(VENUE_ID_COLUMN, venue.getObjectId());
        params.put(TYPE_COLUMN, type);

        final Date startRequestDate = new Date();
        ParseCloud.callFunctionInBackground(SAVE_PIN_POP_FUNC, params,
                new FunctionCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                        BaseActivity.restoreOrientationChange(activity);
                        if (listener != null) {
                            if (e == null) {
                                listener.onSuccess(parseObject);
                                if (type == POP_TYPE) {
                                    Date pinPopDate = parseObject.getDate(TIME_STAMP_COLUMN);
                                    // Check if new Pop created
                                    if (pinPopDate.getTime() > startRequestDate.getTime())
                                        sendPopMatchPush(venue, activity.getApplicationContext());
                                }
                                PinPopListFragment.sendPinPopBroadcast(makePinPop(parseObject,
                                        getCurrentUser(), venue), activity);
                            } else listener.onError(e.getMessage(), e.getCode());
                        }
                    }
                });
    }

    public static void makePin(final Venue venue, final Activity activity,
                               final ResponseListener listener) {
//        beginAsyncOperation(activity, activity.getString(R.string.pin).toLowerCase());
        BaseActivity.blockOrientationChange(activity);

        //Check if user made pin hour ago
        ParseQuery<ParseObject> pinPopQuery = new ParseQuery<>(PIN_POP_TABLE);
        pinPopQuery.whereEqualTo(TYPE_COLUMN, PIN_TYPE);
        pinPopQuery.whereEqualTo(VENUE_ID_COLUMN, venue.getObjectId());
        pinPopQuery.whereEqualTo(USER_ID_COLUMN, getCurrentUser().getObjectId());
        Date greaterThenDate = new Date(new Date().getTime() - MAKE_PIN_HOURS_INTERVAL
                * TimeHelper.HOUR_MILLIS);
        pinPopQuery.whereGreaterThan(CREATED_AT_COLUMN, greaterThenDate);
        pinPopQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, final ParseException e) {
                if (e == null) {
                    if (parseObjects.size() > 0) {
                        BaseActivity.restoreOrientationChange(activity);
//                        finishAsyncOperation(activity, null);
                        listener.onError(activity.getString(R.string.already_pinned),
                                GENERAL_ERROR_CODE);
                    } else {
                        final ParseObject parseObject = new ParseObject(PIN_POP_TABLE);
                        parseObject.put(USER_ID_COLUMN, getCurrentUser().getObjectId());
                        parseObject.put(VENUE_ID_COLUMN, venue.getObjectId());
                        parseObject.put(TYPE_COLUMN, PIN_TYPE);
                        parseObject.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {
                                    BaseActivity.restoreOrientationChange(activity);
//                                    finishAsyncOperation(activity, new AnimationListener() {
//                                        @Override
//                                        public void done() {
                                    listener.onSuccess(parseObject);
                                    PinPopListFragment.sendPinPopBroadcast(makePinPop(parseObject,
                                            getCurrentUser(), venue), activity);
//                                        }
//                                    });
                                } else {
                                    BaseActivity.restoreOrientationChange(activity);
//                                    finishAsyncOperation(activity, null);
                                    listener.onError(e.getMessage(), e.getCode());
                                }
                            }
                        });
                    }
                } else {
                    BaseActivity.restoreOrientationChange(activity);
//                    finishAsyncOperation(activity, null);
                    listener.onError(e.getMessage(), e.getCode());
                }
            }
        });
    }

    public static void makePop(final Venue venue, final Activity activity,
                               final ResponseListener listener) {

        //Check if user made pop 5 min ago
        ParseQuery<ParseObject> pinPopQuery = new ParseQuery<>(PIN_POP_TABLE);
        pinPopQuery.whereEqualTo(TYPE_COLUMN, POP_TYPE);
        pinPopQuery.whereEqualTo(VENUE_ID_COLUMN, venue.getObjectId());
        pinPopQuery.whereEqualTo(USER_ID_COLUMN, getCurrentUser().getObjectId());
        Date greaterThenDate = new Date(new Date().getTime() - MADE_POP_MINUTES_INTERVAL
                * TimeHelper.MINUTE_MILLIS);
        pinPopQuery.whereGreaterThan(CREATED_AT_COLUMN, greaterThenDate);
        pinPopQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, final ParseException e) {
                if (e == null) {
                    if (parseObjects.size() > 0) {
                        BaseActivity.restoreOrientationChange(activity);
//                        finishAsyncOperation(activity, null);
                        listener.onError(activity.getString(R.string.already_popped),
                                GENERAL_ERROR_CODE);
                    } else {
                        final ParseObject parseObject = new ParseObject(PIN_POP_TABLE);
                        parseObject.put(USER_ID_COLUMN, getCurrentUser().getObjectId());
                        parseObject.put(VENUE_ID_COLUMN, venue.getObjectId());
                        parseObject.put(TYPE_COLUMN, POP_TYPE);
                        parseObject.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {
                                    BaseActivity.restoreOrientationChange(activity);
//                                    finishAsyncOperation(activity, new AnimationListener() {
//                                        @Override
//                                        public void done() {
                                    listener.onSuccess(parseObject);
                                    PinPopListFragment.sendPinPopBroadcast(makePinPop(parseObject,
                                            getCurrentUser(), venue), activity);
//                                        }
//                                    });
                                } else {
                                    BaseActivity.restoreOrientationChange(activity);
//                                    finishAsyncOperation(activity, null);
                                    listener.onError(e.getMessage(), e.getCode());
                                }
                            }
                        });
                    }
                } else {
                    BaseActivity.restoreOrientationChange(activity);
//                    finishAsyncOperation(activity, null);
                    listener.onError(e.getMessage(), e.getCode());
                }
            }
        });
    }

    public static PinPop makePinPop(ParseObject parseObject, ParseUser parseUser, Venue venue) {
        return new PinPop(parseObject.getInt(TYPE_COLUMN), parseObject.getCreatedAt(),
                makeUser(parseUser), venue, parseObject.getObjectId());
    }

    public static void searchRequest(String venueName, final Activity activity,
                                     final ResponseListener listener) {
        BaseActivity.blockOrientationChange(activity);
        ParseQuery<ParseObject> query = new ParseQuery<>(REQUESTS_TABLE);
        query.whereMatches(VENUE_REQUESTED_COLUMN, venueName, "i");
        query.whereEqualTo(USER_ID_COLUMN, getCurrentUser().getObjectId());
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                BaseActivity.restoreOrientationChange(activity);
                if (e == null) {
                    if (parseObjects.size() > 0) listener.onSuccess(parseObjects.get(0));
                    else listener.onError("Venue not found", OBJECT_NOT_FOUND_ERROR_CODE);
                } else listener.onError(e.getMessage(), e.getCode());
            }
        });
    }

    public static void saveRequest(String venueName) {
        ParseObject parseObject = new ParseObject(REQUESTS_TABLE);
        parseObject.put(USER_ID_COLUMN, getCurrentUser().getObjectId());
        parseObject.put(VENUE_REQUESTED_COLUMN, venueName);
        parseObject.saveEventually();
    }

    public static void didUserWinkedAtCurrentUser(User user, final ResponseListener listener) {
        ParseQuery<ParseObject> query = new ParseQuery<>(WINKS_TABLE);
        query.whereEqualTo(USER_ID_COLUMN, user.getObjectId());
        query.whereEqualTo(RECEIVER_ID_COLUMN, getCurrentUser().getObjectId());
        query.setLimit(1);

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                if (e == null) {
                    if (parseObjects.size() > 0) listener.onSuccess(parseObjects.get(0));
                    else listener.onError("Did not wink", OBJECT_NOT_FOUND_ERROR_CODE);
                } else listener.onError(e.getMessage(), e.getCode());
            }
        });
    }

    public static void didCurrentUserWinkedAtUser(User user, final ResponseListener listener) {
        ParseQuery<ParseObject> query = new ParseQuery<>(WINKS_TABLE);
        query.whereEqualTo(USER_ID_COLUMN, getCurrentUser().getObjectId());
        query.whereEqualTo(RECEIVER_ID_COLUMN, user.getObjectId());
        query.setLimit(1);

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                if (e == null) {
                    if (parseObjects.size() > 0) listener.onSuccess(parseObjects.get(0));
                    else listener.onError("Did not wink", OBJECT_NOT_FOUND_ERROR_CODE);
                } else listener.onError(e.getMessage(), e.getCode());
            }
        });
    }

    public static void didCurrentUserWinkedLastHour(User user, final ResponseListener listener) {
        ParseQuery<ParseObject> query = new ParseQuery<>(WINKS_TABLE);
        query.whereEqualTo(USER_ID_COLUMN, getCurrentUser().getObjectId());
        query.whereEqualTo(RECEIVER_ID_COLUMN, user.getObjectId());

        Date greaterThenDate = new Date(new Date().getTime() - MAKE_WINK_HOURS_INTERVAL
                * TimeHelper.HOUR_MILLIS);
        query.whereGreaterThan(CREATED_AT_COLUMN, greaterThenDate);

        query.setLimit(MALE_WINKS_PER_HOUR_LIMIT);

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                if (e == null) {
                    if (parseObjects.size() == MALE_WINKS_PER_HOUR_LIMIT)
                        listener.onSuccess(parseObjects.get(0));
                    else listener.onError("Did not wink", OBJECT_NOT_FOUND_ERROR_CODE);
                } else listener.onError(e.getMessage(), e.getCode());
            }
        });
    }

    public static String getCurrentUserFirstName() {
        return getCurrentUser().getString(TU_NAME_COLUMN);
    }

    private static String makeWinkPushAlert(String username, String clue, Context context, Venue venue) {
        return username + " " + context.getString(R.string.has_winked_at_you) + " " + venue.getName();
    }

    public static void sendWinkPush(User receiver, Venue venue, Context context, String clue,
                                    ParseObject winkParseObject, int type) {
        ParseQuery pushQuery = ParseInstallation.getQuery();
        pushQuery.whereEqualTo(USER_COLUMN, ParseObject.createWithoutData(USER_TABLE,
                receiver.getObjectId()));

        ParsePush push = new ParsePush();
        push.setQuery(pushQuery);
        JSONObject data = new JSONObject();
        try {
            String username = getCurrentUserFirstName(), message = null;
            switch (type) {
                case Wink.CLUE_TYPE:
                    username = context.getString(R.string.someone);
                    message = makeWinkPushAlert(username, clue, context, venue);
                    break;
                case Wink.PHOTO_TYPE:
                    message = makeWinkPushAlert(username, clue, context, venue);
                    break;
                case Wink.MATCH_TYPE:
                    message = context.getString(R.string.new_match);
                    break;
            }

            String venueId = venue.getObjectId();
            data.put(USER_ID_COLUMN, getCurrentUser().getObjectId());
            data.put(VENUE_ID_COLUMN, venueId);
            data.put(OBJECT_ID_COLUMN, winkParseObject.getObjectId());
            data.put(TYPE_COLUMN, type);
            data.put(ALERT_KEY, message);
            data.put(SOUND_KEY, DEFAULT);

            push.setData(data);
            push.sendInBackground();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static Venue loadVenue(String venueId) {
        Venue venue = null;
        ParseQuery<ParseObject> query = new ParseQuery<>(VENUES_TABLE);
        try {
            ParseObject parseObject = query.get(venueId);
            venue = makeVenue(parseObject);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        VenueORM.deleteVenueFromDataBase(venue.getObjectId());
        VenueORM.addVenue(venue);
        App.logOut("backend load venue", venue.getName());
        return venue;
    }

    public static User loadUser(String userId) {
        User user = null;
        ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
        try {
            ParseUser parseUser = userQuery.get(userId);
            user = makeUser(parseUser);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        UserORM.deleteUserFromDataBase(user.getObjectId());
        UserORM.addUser(user);
        App.logOut("backend load user", user.getFirstName());
        return user;
    }

    public static List<Chat> loadChat(User user, int startOffset) throws ParseException {
        List<Chat> chatList = new ArrayList<>();

        ParseQuery<ParseObject> querySender = new ParseQuery<>(CHAT_TABLE);
        querySender.whereEqualTo(USER_ID_COLUMN, getCurrentUser().getObjectId());
        querySender.whereEqualTo(RECEIVER_ID_COLUMN, user.getObjectId());

        ParseQuery<ParseObject> queryReceiver = new ParseQuery<>(CHAT_TABLE);
        queryReceiver.whereEqualTo(USER_ID_COLUMN, user.getObjectId());
        queryReceiver.whereEqualTo(RECEIVER_ID_COLUMN, getCurrentUser().getObjectId());

        List<ParseQuery<ParseObject>> queries = new ArrayList<>();
        queries.add(querySender);
        queries.add(queryReceiver);

        ParseQuery<ParseObject> mainQuery = ParseQuery.or(queries);
        mainQuery.orderByAscending(CREATED_AT_COLUMN);

        mainQuery.setSkip(startOffset);
        mainQuery.setLimit(LOAD_ITEMS_LIMIT);

        List<ParseObject> parseObjects = mainQuery.find();
        for (ParseObject parseObject : parseObjects) chatList.add(makeChat(parseObject, user));

        return chatList;
    }

    public static void loadChatMessage(String objectId, final ResponseListener listener) {
        ParseQuery<ParseObject> query = new ParseQuery<>(CHAT_TABLE);
        query.getInBackground(objectId, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if (e == null) listener.onSuccess(parseObject);
                else listener.onError(e.getMessage(), e.getCode());
            }
        });
    }

    public static Chat makeChat(ParseObject parseObject, User user) {
        User sender, receiver, currentUser = makeUser(getCurrentUser());
        String senderId = parseObject.getString(USER_ID_COLUMN);

        if (senderId.equals(currentUser.getObjectId())) {
            sender = currentUser;
            receiver = user;
        } else {
            sender = user;
            receiver = currentUser;
        }

        String mediaUrl = null;
        ParseFile parseFile = parseObject.getParseFile(MEDIA_COLUMN);
        if (parseFile != null) mediaUrl = parseFile.getUrl();
        return new Chat(parseObject.getObjectId(), parseObject.getString(MESSAGE_COLUMN),
                sender, receiver, parseObject.getCreatedAt(), mediaUrl);
    }

    public static void loadWink(String objectId, final ResponseListener listener) {
        ParseQuery<ParseObject> query = new ParseQuery<>(WINKS_TABLE);
        query.getInBackground(objectId, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if (e == null) {
                    listener.onSuccess(parseObject);
                } else listener.onError(e.getMessage(), e.getCode());
            }
        });
    }

    public static Wink makeWink(ParseObject parseObject, Venue venue, User user) {
        return new Wink(parseObject.getString(CLUE_COLUMN), user, venue,
                parseObject.getInt(TYPE_COLUMN), parseObject.getObjectId());
    }

    public static void loadTermsOfService(final Activity activity, final ResponseListener listener) {
        BaseActivity.blockOrientationChange(activity);

        ParseQuery<ParseObject> query = new ParseQuery<>(TERMS_OF_SERVICE_TABLE);
        query.whereEqualTo(TYPE_COLUMN, "tos");

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                BaseActivity.restoreOrientationChange(activity);
                if (e == null) {
                    if (parseObjects.size() > 0) listener.onSuccess(parseObjects.get(0));
                    else listener.onError("No terms :(", GENERAL_ERROR_CODE);
                } else listener.onError(e.getMessage(), e.getCode());
            }
        });
    }

    public static String getTermsText(ParseObject parseObject) {
        return parseObject.getString(MESSAGE_COLUMN);
    }

    public static void savePhotosOrder(List<String> photoUrls, final Activity activity,
                                       final ResponseListener listener) {
        if (!InternetHelper.checkInternetConnection(activity)){
            listener.onError(activity.getString(R.string.check_your_internet_connection),
                    GENERAL_ERROR_CODE);
            return;
        }

        beginAsyncOperation(activity, activity.getString(R.string.saving));
        final ParseUser parseUser = getCurrentUser();
        List<ParseFile> parseFiles = new ArrayList<>();
        String photoName;
        for (int i = 0; i < USER_PHOTOS_COUNT; i ++){
            photoName = PROFILE_IMAGE_COLUMN + i;
            if (parseUser.has(photoName)) {
                parseFiles.add(parseUser.getParseFile(photoName));
                parseUser.remove(photoName);
            }
        }

        ParseFile photoParseFile;
        for (int i = 0; i < photoUrls.size(); i ++){
            photoParseFile = findParseFileByUrl(parseFiles, photoUrls.get(i));
            if (photoParseFile != null) parseUser.put(PROFILE_IMAGE_COLUMN + i, photoParseFile);
        }

        parseUser.saveInBackground(new SaveCallback() {
            @Override
            public void done(final ParseException e) {
                finishAsyncOperation(activity, null);
                if (e == null) listener.onSuccess(parseUser);
                else listener.onError(e.getMessage(), e.getCode());
            }
        });
    }

    private static ParseFile findParseFileByUrl(List<ParseFile> parseFiles, String url){
        for (ParseFile parseFileEl : parseFiles)
            if (parseFileEl.getUrl().equals(url))
                return parseFileEl;
        return null;
    }

//    public static void getMutualFriendsCount(final User user, final Activity activity,
//                                             final ResponseListener listener){
//        BaseActivity.blockOrientationChange(activity);
//
//        HashMap<String, Object> params = new HashMap<>();
//        params.put(USER_ID_COLUMN, user.getObjectId());

//        ParseCloud.callFunctionInBackground("getFacebookId", params,
//                new FunctionCallback<String>() {
//                    @Override
//                    public void done(String result, ParseException e) {
//                        if (listener != null) {
//                            if (e == null) {
//                                Bundle params = new Bundle();
//                                params.putString("fields", "context.fields(mutual_friends)");
//                                new GraphRequest(
//                                        AccessToken.getCurrentAccessToken(),
//                                        "/" + result,
//                                        params,
//                                        HttpMethod.GET,
//                                        new GraphRequest.Callback() {
//                                            public void onCompleted(GraphResponse response) {
//                                                BaseActivity.restoreOrientationChange(activity);
//                                                try {
//                                                    JSONObject jsonObject = response.getJSONObject().
//                                                            getJSONObject("context");
//                                                    int mutualFriends = jsonObject.getJSONObject("mutual_friends").
//                                                            getJSONObject("summary").getInt("total_count");
//                                                    ParseObject parseObject = ParseObject.create(USER_TABLE);
//                                                    parseObject.put(MUTUAL_FRIENDS_COUNT, mutualFriends);
//                                                    listener.onSuccess(parseObject);
//                                                } catch (JSONException e1) {
//                                                    e1.printStackTrace();
//                                                    listener.onError(e1.getMessage(), GENERAL_ERROR_CODE);
//                                                }
//                                            }
//                                        }
//                                ).executeAsync();
//                            } else {
//                                listener.onError(e.getMessage(), GENERAL_ERROR_CODE);
//                                BaseActivity.restoreOrientationChange(activity);
//                            }
//                        } else BaseActivity.restoreOrientationChange(activity);
//                    }
//                });
//    }

    public static void isUserMatched(final User user, final Activity activity,
                                     final ResponseListener listener){
        BaseActivity.blockOrientationChange(activity);

        ParseQuery<ParseObject> matchQuery1 = new ParseQuery<>(WINKS_TABLE);
        matchQuery1.whereEqualTo(USER_ID_COLUMN, getCurrentUser().getObjectId());
        matchQuery1.whereEqualTo(RECEIVER_ID_COLUMN, user.getObjectId());
        matchQuery1.whereEqualTo(TYPE_COLUMN, Wink.CLUE_TYPE);
        matchQuery1.whereEqualTo(CLUE_COLUMN, null);

        ParseQuery<ParseObject> matchQuery2 = new ParseQuery<>(WINKS_TABLE);
        matchQuery2.whereEqualTo(USER_ID_COLUMN, user.getObjectId());
        matchQuery2.whereEqualTo(RECEIVER_ID_COLUMN, getCurrentUser().getObjectId());
        matchQuery2.whereEqualTo(TYPE_COLUMN, Wink.CLUE_TYPE);
        matchQuery2.whereEqualTo(CLUE_COLUMN, null);

        ParseQuery<ParseObject> matchQuery = ParseQuery.or(Arrays.asList(matchQuery1, matchQuery2));
        matchQuery.setLimit(1);
        matchQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                BaseActivity.restoreOrientationChange(activity);
                if (e == null){
                    listener.onSuccess(list.size() == 0 ? null : list.get(0));
                } else listener.onError(e.getMessage(), GENERAL_ERROR_CODE);
            }
        });
    }
}
