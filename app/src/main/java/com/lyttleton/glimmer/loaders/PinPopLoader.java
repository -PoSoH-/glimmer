package com.lyttleton.glimmer.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.lyttleton.glimmer.DataAccessObject.PinPopORM;
import com.lyttleton.glimmer.DataAccessObject.UserORM;
import com.lyttleton.glimmer.adapters.PinPop;
import com.lyttleton.glimmer.adapters.Venue;
import com.lyttleton.glimmer.fragments.PinPopListFragment;
import com.lyttleton.glimmer.helpers.InternetHelper;
import com.parse.ParseException;

import java.util.ArrayList;
import java.util.List;

public class PinPopLoader extends AsyncTaskLoader<List<PinPop>> {

    private int loadType, fragmentType;
    private Venue venue;
    private boolean hasNextPage;
    private List<PinPop> previousItems, lastLoadedItems;
    private boolean loaderCreating, noInternet;
    private int loadingStartOffset;

    public PinPopLoader(Context context, int loadType, int fragmentType, Venue venue,
                        List<PinPop> previousItems, int loadingStartOffset) {
        super(context);
        this.loadType = loadType;
        this.fragmentType = fragmentType;
        this.venue = venue;
        this.previousItems = previousItems;
        this.loadingStartOffset = loadingStartOffset;
        loaderCreating = true;
        loadedUsersToPinPop();
    }

    private void loadedUsersToPinPop(){
        if(previousItems != null) {
            for (int pos_pop = 0, len = previousItems.size(); pos_pop < len; pos_pop++) {
                previousItems.get(pos_pop).addUser(UserORM.loadUsersFromDataBase(previousItems.get(pos_pop).getUserId()));
                previousItems.get(pos_pop).addVenue(venue);
            }
        }
    }

    @Override
    protected void onStartLoading() {
        if (loaderCreating || lastLoadedItems == null || lastLoadedItems.size() == 0) forceLoad();
        else deliverResult(lastLoadedItems);
    }

    @Override
    public List<PinPop> loadInBackground() {
        loaderCreating = false;
        List<PinPop> pinPopList = new ArrayList<>();
        List<PinPop> loadedPinPopList = null;
        noInternet = false;
        if (InternetHelper.checkInternetConnection(getContext())) {
            try {
                if (fragmentType == PinPopListFragment.VENUE_FRAGMENT_TYPE) {
                    loadedPinPopList = Backend.loadPinPopList(loadType, venue, this, loadingStartOffset,
                            previousItems);
//                    HelpersORM.replaceAllPinPop(loadedPinPopList);
                }
                else {
                    loadedPinPopList = Backend.loadPinPopListOfUser(fragmentType, this, loadingStartOffset);
//                    HelpersORM.replaceAllPinPop(loadedPinPopList);
                }
            } catch (ParseException e) {
                e.printStackTrace();
                if (e.getCode() == Backend.CONNECTION_FAILED_ERROR_CODE){
//                    noInternet = true;
//                    loadedPinPopList = HelpersORM.getAllPinPop();
                    loadedPinPopList = PinPopORM.loadAllPinPops();
                }
            }

            if (previousItems != null) pinPopList.addAll(previousItems);
            if (loadedPinPopList != null) pinPopList.addAll(loadedPinPopList);
        }

        //view pinpop ..
        if(pinPopList.size() == 0){
            lastLoadedItems = previousItems;
        }else{
            lastLoadedItems = pinPopList;
            PinPopORM.replaceAllPinPops(lastLoadedItems);
        }
        return lastLoadedItems;
    }

    public boolean hasNextPage() {
        return hasNextPage;
    }

    public void setHasNextPage(boolean hasNextPage) {
        this.hasNextPage = hasNextPage;
    }

    public int getLoadingStartOffset() {
        return loadingStartOffset;
    }

    public void setLoadingStartOffset(int loadingStartOffset) {
        this.loadingStartOffset = loadingStartOffset;
    }

    public boolean isNoInternet() {
        return noInternet;
    }
}
