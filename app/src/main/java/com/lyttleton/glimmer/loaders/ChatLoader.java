package com.lyttleton.glimmer.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.lyttleton.glimmer.DataAccessObject.ChatORM;
import com.lyttleton.glimmer.adapters.Chat;
import com.lyttleton.glimmer.adapters.User;
import com.lyttleton.glimmer.helpers.InternetHelper;
import com.parse.ParseException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ChatLoader extends AsyncTaskLoader<List<Chat>> {

    private User user;
    private List<Chat> previousItems, lastLoadedItems;
    private boolean hasNextPage, loaderCreating, noInternet;

    public ChatLoader(Context context, User user, List<Chat> previousItems) {
        super(context);
        this.user = user;
        this.previousItems = previousItems;
        loaderCreating = true;
    }

    @Override
    protected void onStartLoading() {
        if (loaderCreating || lastLoadedItems == null || lastLoadedItems.size() == 0) forceLoad();
        else deliverResult(lastLoadedItems);
    }

    @Override
    public List<Chat> loadInBackground() {
        loaderCreating = false;

        List<Chat> items = new ArrayList<>();

        noInternet = false;
        if (InternetHelper.checkInternetConnection(getContext())) {
            try {
                int startOffset = 0;
//                if(hasNextPage) {
                    startOffset = previousItems != null ? previousItems.size() : 0;
//                }
                List<Chat> loadedItems;
                loadedItems = Backend.loadChat(user, startOffset);
                if (previousItems != null)
                    for (Chat chat : previousItems)
                        if (findChat(chat, loadedItems) == null) items.add(chat);
                if (loadedItems != null) {
                    items.addAll(loadedItems);
//                    items.addAll(previousItems);
                    if (loadedItems.size() == Backend.LOAD_ITEMS_LIMIT) hasNextPage = true;
                }
            }catch(ParseException e){
                e.printStackTrace();
            }
        }

        Collections.sort(items, new Comparator<Chat>() {
            @Override
            public int compare(Chat lhs, Chat rhs) {
                return lhs.getDate().compareTo(rhs.getDate());
            }
        });

        if (items.size() > 0) {
            lastLoadedItems = items;
            ChatORM.replaceAllChats(items);
        } else lastLoadedItems = previousItems;
        return lastLoadedItems;
    }

    private Chat findChat(Chat chat, List<Chat> chats) {
        for (Chat chatEl : chats)
            if (chatEl.getObjectId().equals(chat.getObjectId())) return chatEl;
        return null;
    }

    public boolean hasNextPage() {
        return hasNextPage;
    }

    public boolean isNoInternet() {
        return noInternet;
    }
}
