package com.lyttleton.glimmer.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.activities.ChatActivity;
import com.lyttleton.glimmer.activities.SplashScreenActivity;
import com.lyttleton.glimmer.activities.VenueActivity;
import com.lyttleton.glimmer.activities.WinkActivity;
import com.lyttleton.glimmer.adapters.User;
import com.lyttleton.glimmer.adapters.Venue;
import com.lyttleton.glimmer.adapters.Wink;
import com.lyttleton.glimmer.helpers.SharedPrefsHelper;
import com.lyttleton.glimmer.loaders.Backend;

import org.json.JSONException;
import org.json.JSONObject;

public class PushReceiverService extends IntentService {

    public static final String JSON_DATA_ARG = "JSON_DATA_ARG";
    public static final String VENUE_ARG = "VENUE_ARG";
    public static final String USER_ARG = "USER_ARG";

    public PushReceiverService() {
        super(PushReceiverService.class.getSimpleName());
    }

    public static void start(Context context, String jsonData){
        Intent intent = new Intent(context, PushReceiverService.class);
        intent.putExtra(JSON_DATA_ARG, jsonData);
        context.startService(intent);
    }

    private void sendPushBroadcast(String objectId, Venue venue, User user, int type){
        Intent intent = new Intent(Backend.PUSH_BROADCAST_ACTION);
        intent.putExtra(Backend.OBJECT_ID_COLUMN, objectId);
        intent.putExtra(VENUE_ARG, venue);
        intent.putExtra(USER_ARG, user);
        intent.putExtra(Backend.TYPE_COLUMN, type);
        sendBroadcast(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Context context = getApplicationContext();
        int type = 0;
        try {
            JSONObject data = new JSONObject(intent.getStringExtra(JSON_DATA_ARG));

            Intent resultIntent = null;
            if (data.has(Backend.TYPE_COLUMN)){
                type = data.getInt(Backend.TYPE_COLUMN);

                Venue venue = null;
                if (data.has(Backend.VENUE_ID_COLUMN))
                    venue = Backend.loadVenue(data.getString(Backend.VENUE_ID_COLUMN));

                User user = null;
                String userId = null;
                if (data.has(Backend.USER_ID_COLUMN)) {
                    userId = data.getString(Backend.USER_ID_COLUMN);
                    user = Backend.loadUser(userId);
                }

                String objectId = null;
                if (data.has(Backend.OBJECT_ID_COLUMN))
                    objectId = data.getString(Backend.OBJECT_ID_COLUMN);

                switch (type){
                    case Wink.CLUE_TYPE:
                        if (venue != null) {
                            resultIntent = VenueActivity.createStartIntent(context, venue);
                            sendPushBroadcast(objectId, venue, user, type);
                        }
                        break;
                    case Wink.PHOTO_TYPE:
                        if (user != null && venue != null) {
                            resultIntent = WinkActivity.createStartIntent(context, user, venue);
                            sendPushBroadcast(objectId, venue, user, type);
                        }
                        break;
                    case Wink.MATCH_TYPE:
                        if (user != null) {
                            resultIntent = ChatActivity.createStartIntent(context, user);
                            sendPushBroadcast(objectId, venue, user, type);
                        }
                        break;
                    case Wink.CHAT_TYPE:
                            String userIdOfOpenedChat = SharedPrefsHelper.getUserIdOfOpenedChat(context);
                            // Check if chat with sender user is not already opened
                            if (userIdOfOpenedChat == null || !user.getObjectId().equals(userIdOfOpenedChat)) {
                                SharedPrefsHelper.addNewChatMessageUserId(userId, context);
                                sendPushBroadcast(objectId, venue, user, type);
                                if (userId != null && user != null) {
                                    resultIntent = ChatActivity.createStartIntent(context, user);
                            }
                        }
                        break;
                    case Wink.POP_TYPE:
                        if (userId != null && user != null)
                            resultIntent = ChatActivity.createStartIntent(context, user);
                        break;
                }
            } else resultIntent = new Intent(context, SplashScreenActivity.class);

            if (resultIntent == null) return;

            if (type == Wink.CHAT_TYPE)
                resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

            PendingIntent resultPendingIntent = PendingIntent.getActivity(
                        context,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(context)
                            .setContentTitle(context.getString(R.string.app_name))
                            .setContentText(data.getString(Backend.ALERT_KEY));
            mBuilder.setSmallIcon(R.drawable.icon);
            mBuilder.setContentIntent(resultPendingIntent);
            mBuilder.setAutoCancel(true);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                mBuilder.setColor(getApplicationContext().getResources().getColor(R.color.purple));

            NotificationManager mNotificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            Notification notification = mBuilder.build();
            notification.defaults = Notification.DEFAULT_ALL;
            mNotificationManager.notify(0, notification);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

//    private int getNotificationIcon(NotificationCompat.Builder mBuilder) {
//        boolean lollipop = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
//        if (lollipop){
//            mBuilder.setColor(getApplicationContext().getResources().getColor(R.color.purple));
//        }
//        return R.drawable.icon;
//    }
}
