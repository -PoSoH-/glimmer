package com.lyttleton.glimmer.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.activities.MyProfilePhotosActivity;
import com.lyttleton.glimmer.helpers.FileHelper;
import com.lyttleton.glimmer.helpers.InternetHelper;
import com.lyttleton.glimmer.helpers.SharedPrefsHelper;
import com.lyttleton.glimmer.helpers.ToastHelper;
import com.lyttleton.glimmer.loaders.Backend;
import com.parse.ParseException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;

public class PhotosUploadService extends IntentService {

    public static final String JSON_OBJECT_ARG = "JSON_OBJECT_ARG";

    public PhotosUploadService() {
        super(PhotosUploadService.class.getSimpleName());
    }

    public static void start(Context context, String jsonObject){
        Intent intent = new Intent(context, PhotosUploadService.class);
        intent.putExtra(JSON_OBJECT_ARG, jsonObject);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        JSONObject jsonObject;
        try {
            Backend.deleteUserPhotos();

            jsonObject = new JSONObject(intent.getStringExtra(JSON_OBJECT_ARG));

            int uploadedPhotosCount = 0;
            String avatarName = null;

            // If user has avatar use it as first photo
//            if (jsonObject.has("picture")){
//                JSONObject pictureJsonObject = jsonObject.getJSONObject("picture").
//                        getJSONObject("data");
//                if (!pictureJsonObject.getBoolean("is_silhouette")){
//                    avatarName = FileHelper.getFileName(pictureJsonObject.getString("url").
//                            replace("\\", "").replaceAll("\\?.+", ""));
//                    uploadedPhotosCount = uploadPhoto("https://graph.facebook.com/"
//                            + jsonObject.getString("id") + "/picture?width=900",
//                            uploadedPhotosCount, avatarName, FileHelper.getFileExtension(avatarName));
//                }
//            }

            // Get user photos
//            GraphRequest photosGraphRequest = GraphRequest.
//                    newGraphPathRequest(AccessToken.
//                            getCurrentAccessToken(), "/" + jsonObject.getString("id") + "/photos/uploaded", null);
//            GraphResponse photosResponse = photosGraphRequest.executeAndWait();
//
//            JSONArray photosJsonArray = photosResponse.getJSONObject().getJSONArray("data");
//            String photoUrl;
//            for (int i = 0; i < photosJsonArray.length(); i++){
//                try {
//                    photoUrl = photosJsonArray.getJSONObject(i).getString("source");
//                    uploadedPhotosCount = uploadPhoto(photoUrl, uploadedPhotosCount, avatarName);
//                    if (uploadedPhotosCount == Backend.USER_PHOTOS_COUNT) break;
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }

//            if (jsonObject.has("albums")) {
//                JSONArray albumsJsonArray = jsonObject.getJSONObject("albums").getJSONArray("data");
//                JSONObject album;
//
//                for (int i = 0; i < albumsJsonArray.length(); i++) {
//                    album = albumsJsonArray.getJSONObject(i);
//
//                    GraphRequest albumPhotos = GraphRequest.
//                            newGraphPathRequest(AccessToken.
//                                    getCurrentAccessToken(), "/" + album.getString("id") + "/photos", null);
//                    GraphResponse graphResponse = albumPhotos.executeAndWait();
//                    JSONArray photosJsonArray = graphResponse.getJSONObject().
//                            getJSONArray("data");
//
//                    String photoUrl;
//                    for (int j = 0; j < photosJsonArray.length(); j++){
//                        try {
//                            photoUrl = photosJsonArray.getJSONObject(j).getString("source");
//                            uploadedPhotosCount = uploadPhoto(photoUrl, uploadedPhotosCount,
//                                    avatarName);
//                            if (uploadedPhotosCount == Backend.USER_PHOTOS_COUNT) break;
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                    if (uploadedPhotosCount == Backend.USER_PHOTOS_COUNT) break;
//                }
//                SharedPrefsHelper.finishPhotosUploading(getApplicationContext());
//                MyProfilePhotosActivity.finishPhotosUploadingByBroadcast(getApplicationContext());
//            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
            handleParseException(e);
        } finally {
            try {
                Backend.fetchCurrentUser();
            } catch (ParseException e) {
                e.printStackTrace();
                handleParseException(e);
            } finally {
                SharedPrefsHelper.finishPhotosUploading(getApplicationContext());
                MyProfilePhotosActivity.finishPhotosUploadingByBroadcast(getApplicationContext());
            }
        }
    }

    private void handleParseException(ParseException e){
        String errorMessage = e.getMessage();
        if (e.getCode() == Backend.OBJECT_NOT_FOUND_ERROR_CODE) {
            errorMessage = getString(R.string.deleted_account);
            Backend.logout();
        }
        ToastHelper.showErrorToast(getApplicationContext(), errorMessage);
    }

    private static int uploadPhoto(String photoUrl, int uploadedPhotosCount, String avatarName) {
        String fileName = FileHelper.getFileName(photoUrl).replaceAll("\\?.+", "");
        return uploadPhoto(photoUrl, uploadedPhotosCount, avatarName,
                FileHelper.getFileExtension(fileName));
    }

    private static int uploadPhoto(String photoUrl, int uploadedPhotosCount, String avatarName,
                                   String fileExtension){
        try {
            String fileName = FileHelper.getFileName(photoUrl).replaceAll("\\?.+", "");
            // If this photo is not avatar
            if (TextUtils.isEmpty(avatarName) || !avatarName.equals(fileName)) {
                InputStream inputStream = new java.net.URL(photoUrl).openStream();
                Backend.saveUserPhoto(InternetHelper.readBytes(inputStream),
                        uploadedPhotosCount, fileExtension);
                uploadedPhotosCount++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return uploadedPhotosCount;
    }
}
