package com.lyttleton.glimmer.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.lyttleton.glimmer.activities.MyProfilePhotosActivity;
import com.lyttleton.glimmer.helpers.FileHelper;
import com.lyttleton.glimmer.helpers.SharedPrefsHelper;
import com.lyttleton.glimmer.loaders.Backend;
import com.parse.ParseFile;
import com.parse.ParseUser;

public class SelectedPhotoUploadService extends IntentService {

    public static final String PHOTO_INDEX = "PHOTO_INDEX";
    public static final String FILE_PATH = "FILE_PATH";

    public SelectedPhotoUploadService() {
        super(SelectedPhotoUploadService.class.getSimpleName());
    }

    public static void start(Context context, String filePath){
        Intent intent = new Intent(context, SelectedPhotoUploadService.class);
        intent.putExtra(FILE_PATH, filePath);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Context context = getApplicationContext();
        String filePath = intent.getStringExtra(FILE_PATH);
        try {
            Backend.fetchCurrentUser();
            ParseUser user = Backend.getCurrentUser();

            ParseFile photoParseFile = new ParseFile("user_photo."
                    + FileHelper.getFileExtension(filePath), FileHelper.readBytes(filePath));

            if (!user.getBoolean(Backend.UPLOADED_DAILY_PROFILER)){
                // Move all photos right
                ParseFile photoParseFileEl;
                for (int i = Backend.FACEBOOK_PHOTOS_COUNT - 1; i >= 0 ; i--) {
                    photoParseFileEl = user.getParseFile(Backend.PROFILE_IMAGE_COLUMN + (i - 1));
                    if (photoParseFileEl != null)
                        user.put(Backend.PROFILE_IMAGE_COLUMN + i, photoParseFileEl);
                }
            }
            user.put(Backend.UPLOADED_DAILY_PROFILER, true);
            user.put(Backend.PROFILE_IMAGE_COLUMN + 0, photoParseFile);
            user.save();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            SharedPrefsHelper.finishPhotoUploading(context);
            Intent broadcastIntent = new Intent(MyProfilePhotosActivity.PHOTO_UPLOADING_ACTION);
            context.sendBroadcast(broadcastIntent);
        }
    }
}
