package com.lyttleton.glimmer.receivers;

import android.content.Context;
import android.content.Intent;

import com.lyttleton.glimmer.loaders.Backend;
import com.lyttleton.glimmer.services.PushReceiverService;
import com.parse.ParsePushBroadcastReceiver;

public class ParsePushReceiver extends ParsePushBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        PushReceiverService.start(context, intent.getExtras().getString(Backend.PARSE_PUSH_DATA));
    }
}
