package com.lyttleton.glimmer.helpers;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.lyttleton.glimmer.R;

public class ToastHelper {

    public static void showErrorToast(Context context, String message){
        makeToast(context, message, R.layout.error_toast).show();
    }

    public static void showErrorToast(Context context, int messageResId){
        makeToast(context, context.getString(messageResId), R.layout.error_toast).show();
    }

    public static void showSuccessToast(Context context, int messageResId){
        showSuccessToast(context, context.getString(messageResId));
    }

    public static void showSuccessToast(Context context, String message){
        makeToast(context, message, R.layout.success_toast).show();
    }

    private static Toast makeToast(Context context, String message, int layoutResId){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.
                LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(layoutResId, null);

        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setText(message);

        Toast toast = new Toast(context);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);

        return toast;
    }
}
