package com.lyttleton.glimmer.helpers;

import android.text.TextUtils;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;

public class TwilioHelper {
    private static final String URL = "https://api.twilio.com/2010-04-01/Accounts/",
            URL_END = "/SMS/Messages.json";
    private static final String ACCOUNT_SID = "ACc47810a1137694cd066b7369ec6dabe6"; //"ACe6af081024edf22c5a8d4c882c4d3f39";
    public static final String AUTH_TOKEN = "a85c72198883662b8085778575c074bf"; //"30e84ffa35c928b6b4ba253764c12ac2";
    public static final String NUMBER_FROM = "+353861800259";
    public static final String ERROR_MESSAGE = "error_message";
    public static final String MESSAGE_STATUS = "status";

    public static String sendSms(String numberTo, String text){
//        ArrayList<NameValuePair> params = new ArrayList<>();
//        params.add(new BasicNameValuePair("To", numberTo));
//        params.add(new BasicNameValuePair("From", NUMBER_FROM));
//        params.add(new BasicNameValuePair("Body", text));
//        String url = URL + ACCOUNT_SID + URL_END, userName = ACCOUNT_SID, password = AUTH_TOKEN;
//
//        HttpClient httpClient = new DefaultHttpClient();
//        HttpPost httpPost = new HttpPost(url);
//        String result = null;
//        try {
//            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
//            if (!TextUtils.isEmpty(userName) && !TextUtils.isEmpty(password))
//                httpPost.addHeader(BasicScheme.authenticate(
//                        new UsernamePasswordCredentials(userName, password), "UTF-8", false));
//            HttpResponse response = httpClient.execute(httpPost);
//            result = EntityUtils.toString(response.getEntity());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return null; //result;

        ArrayList<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("To", numberTo));
        params.add(new BasicNameValuePair("From", NUMBER_FROM));
        params.add(new BasicNameValuePair("Body", text));
        return InternetHelper.getPOSTResponse(URL + ACCOUNT_SID + URL_END, params, ACCOUNT_SID,
                AUTH_TOKEN);
    }
}
