package com.lyttleton.glimmer.helpers;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

public class LocationHelper {

    public static LatLngBounds getMapBounds(LatLng[] coordinatesOfBounds){

        LatLngBounds.Builder bounds = new LatLngBounds.Builder();
        bounds.include(coordinatesOfBounds[0]);
        bounds.include(coordinatesOfBounds[1]);

        return bounds.build();
    }

    public static LatLng[] getCoordinatesOfBounds(Location location, int radiusOfAreaInMeters){

        double northeastLatitude;
        double northeastLongitude;
        double southwestLatitude;
        double southwestLongitude;

        double halfDeltaOfLongitudeInDegree = (radiusOfAreaInMeters
                / getLengthOfDegreeOfLongitude(location)) / 2;
        double halfDeltaOfLatitudeInDegree = (radiusOfAreaInMeters
                / getLengthOfDegreeOfLatitude(location)) / 2;

        double locationLatitude = location.getLatitude();
        double locationLongitude = location.getLongitude();

        northeastLatitude = locationLatitude + halfDeltaOfLatitudeInDegree;
        southwestLatitude = locationLatitude - halfDeltaOfLatitudeInDegree;

        northeastLongitude = locationLongitude + halfDeltaOfLongitudeInDegree;
        southwestLongitude = locationLongitude - halfDeltaOfLongitudeInDegree;

        LatLng northeast = new LatLng(northeastLatitude, northeastLongitude);
        LatLng southwest = new LatLng(southwestLatitude, southwestLongitude);

        return  new LatLng[]{northeast, southwest};
    }

    public static Double getLengthOfDegreeOfLongitude(Location location){

        Location locationA = new Location(location);
        locationA.setLatitude(location.getLatitude());
        locationA.setLongitude(location.getLongitude());

        Location locationB = new Location(location);
        locationB.setLatitude(location.getLatitude());
        locationB.setLongitude(location.getLongitude() + 1);

        double distance;
        distance = locationA.distanceTo(locationB);

        //Returns the approximate distance in meters
        return distance;
    }

    public static Double getLengthOfDegreeOfLatitude(Location location){

        Location locationA = new Location(location);
        locationA.setLatitude(location.getLatitude());
        locationA.setLongitude(location.getLongitude());

        Location locationB = new Location(location);
        locationB.setLatitude(location.getLatitude() + 1);
        locationB.setLongitude(location.getLongitude());

        double distance;
        distance = locationA.distanceTo(locationB);

        //Returns the approximate distance in meters
        return distance;
    }



}
