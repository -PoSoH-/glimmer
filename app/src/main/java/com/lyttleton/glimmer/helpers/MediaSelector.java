package com.lyttleton.glimmer.helpers;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

import com.lyttleton.glimmer.R;

import java.io.File;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;

public class MediaSelector implements Serializable {

    public static final int MIN_IMAGE_WIDTH = 0;
    public static final int MAX_VIDEO_DURATION = 12;
    public static final int MAX_FILE_SIZE_MB = 10;
    public static final String MEDIA_SELECTOR = "MEDIA_SELECTOR";
    public static final String MEDIA_URI = "MEDIA_URI";
    private transient Uri mediaUri = null;
    private String mediaPath = null;
    private boolean mediaSelected = false;

    public static final String FOLDER = "Glimmer";
    public static final int MEDIA_TYPE_IMAGE = 10;
    public static final int MEDIA_TYPE_VIDEO = 20;
    public static final int IMAGE_LOAD_REQUEST_CODE = 100;
    public static final int IMAGE_CAPTURE_REQUEST_CODE = 101;
    public static final int VIDEO_LOAD_REQUEST_CODE = 200;
    public static final int VIDEO_CAPTURE_REQUEST_CODE = 201;

//    private View iconLayout;
//    private ImageView iconImageView;

    private transient Context context;

    public MediaSelector(final Context context){              // , View iconLayout) {
        this.context = context;
//        this.iconLayout = iconLayout;
//        iconImageView = (ImageView) iconLayout.findViewById(R.id.media_icon);
//
//        iconImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                CameraSelectionDialog.show(activity);
//            }
//        });
//        iconLayout.findViewById(R.id.remove_file).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                removeSelectedFile();
//            }
//        });
    }

    public void setMediaUri(Uri mediaUri) {
        this.mediaUri = mediaUri;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK){
            switch (requestCode) {
                case IMAGE_LOAD_REQUEST_CODE:
                    if (data == null) return;
                    mediaPath = FileHelper.getPath(context, data.getData());
                    int imageWidth = FileHelper.getWidthOfImageFile(mediaPath);
                    if (FileHelper.isFileSizeLessThen10Mb(mediaPath, context)) {
                        if (imageWidth >= MIN_IMAGE_WIDTH) {
                            mediaSelected = true;
//                        setFileIcon(mediaPath, MEDIA_TYPE_IMAGE);
                        } else
                            ToastHelper.showErrorToast(context, "Selected photo width: " + imageWidth +
                                    "px . Minimum photo width is " + MIN_IMAGE_WIDTH + "px.");
                    } else show10MBFileSizeError();
                    break;
                case IMAGE_CAPTURE_REQUEST_CODE:
                    mediaPath = FileHelper.getPath(context, mediaUri);
                    mediaSelected = true;
                    imageWidth = FileHelper.getWidthOfImageFile(mediaPath);
                    if (FileHelper.isFileSizeLessThen10Mb(mediaPath, context)){
                        if (imageWidth >= MIN_IMAGE_WIDTH){
                            mediaSelected = true;
//                        setFileIcon(mediaPath, MEDIA_TYPE_IMAGE);
                        } else
                            ToastHelper.showErrorToast(context, "Making photo width: " + imageWidth + "px . " +
                                    "Minimum photo width is " + MIN_IMAGE_WIDTH + "px.");
                    } else show10MBFileSizeError();
                    break;
                case VIDEO_LOAD_REQUEST_CODE:
                    if (data == null) return;
                    mediaPath = FileHelper.getPath(context, data.getData());
                    long duration = FileHelper.getLengthOfVideoFile(mediaPath);
                    if (FileHelper.isFileSizeLessThen10Mb(mediaPath, context)){
                        if (duration <= MAX_VIDEO_DURATION){
                            mediaSelected = true;
//                        setFileIcon(mediaPath, MEDIA_TYPE_VIDEO);
                        }else
                            ToastHelper.showErrorToast(context, "Selected video duration: " + duration +
                                    "sec. Maximum video duration is 12sec.");
                    } show10MBFileSizeError();
                    break;
//                case VIDEO_CAPTURE_REQUEST_CODE:
//                    if (data == null) return;
//                    mediaPath = data.getStringExtra(CameraActivity.FILE_PATH);
//                    if (!TextUtils.isEmpty(mediaPath))
//                        if (FileHelper.isFileSizeLessThen10Mb(mediaPath, context))
//                            mediaSelected = true;
//                    break;
            }

            if (!TextUtils.isEmpty(mediaPath)
                    && TextUtils.isEmpty(FileHelper.getFileExtension(mediaPath))){
                ToastHelper.showErrorToast(context, R.string.no_extension);
                removeSelection();
            }

        } else if (resultCode == Activity.RESULT_CANCELED) {
            // User cancelled the selecting
//            Toast.makeText(this, "User cancelled the selecting new video", Toast.LENGTH_LONG).show();
        } else {
            // Video selection failed
            ToastHelper.showErrorToast(context, "Failed of file selection");
        }
    }

    private void show10MBFileSizeError() {
        ToastHelper.showErrorToast(context, "File size can't be more than 10MB");
    }

//    private void setFileIcon(String filePath, int mediaType) {
//        iconLayout.setVisibility(View.VISIBLE);
//        setImageToImageView(iconImageView, filePath, mediaType);
//    }

    public void removeSelection() {
        mediaPath = null;
        mediaSelected = false;
//        iconLayout.setVisibility(View.GONE);
    }

    /** For getting the real path of file from the uri */
    public String getRealPathFromUri(Uri contentUri) {
        String result = null;
        try {
            Cursor cursor = context.getContentResolver().query(contentUri, null, null, null, null);
            if (cursor == null) {
                // Source is Drop Box or other similar local file path
                result = contentUri.getPath();
            } else {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                result = cursor.getString(idx);
                cursor.close();
            }
        }catch (Exception e) {
            Log.e("MediaSelector", "Error getting file name from Uri: " + e.getMessage());
        }
        return result;
    }

    public boolean isMediaSelected() {
        return mediaSelected;
    }

    public String getMediaPath() {
        return mediaPath;
    }

    public Uri getMediaUri() {
        return mediaUri;
    }

    /** For getting the Uri of latest making photo or video */
    public static Uri getLastPhotoOrVideo(Context context) {
        String[] columns = { MediaStore.MediaColumns.DATA, MediaStore.MediaColumns.DATE_ADDED };
        ContentResolver cr = context.getContentResolver();
        Cursor cursor = cr.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, columns, null, null,
                MediaStore.MediaColumns.DATE_ADDED + " DESC");
        int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String path = cursor.getString(columnIndex);
        cursor.close();

        return Uri.fromFile(new File(path));
    }

    public boolean hasFilePath() {
        return !TextUtils.isEmpty(mediaPath);
    }

    public boolean isVideoSelected() {
        return isMediaSelected() && !FileHelper.isImage(mediaPath);
    }

    public void setMediaSelected(boolean mediaSelected) {
        this.mediaSelected = mediaSelected;
    }

    public void setMediaPath(String mediaPath) {
        this.mediaPath = mediaPath;
    }

    class ImageLoaderTask extends AsyncTask<String, Void, Bitmap> {

        private final WeakReference<ImageView> imageViewReference;
        private String imagePath;
        private int reqWidth = 0;
        private int reqHeight = 0;

        public ImageLoaderTask(ImageView imageView) {
            // Use a WeakReference to ensure the ImageView can be garbage collected
            imageViewReference = new WeakReference<ImageView>(imageView);
        }

        // Decode image in background.
        @Override
        protected Bitmap doInBackground(String[] params) {
            imagePath = params[0];
            reqWidth = Integer.parseInt(params[1]);
            reqHeight = Integer.parseInt(params[2]);
            return  decodeSampledBitmapFromPath(imagePath, reqWidth / 2, reqHeight / 2);
        }

        // Once complete, see if ImageView is still around and set bitmap.
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (isCancelled()) {
                bitmap = null;
            }
            if (imageViewReference != null && bitmap != null) {
                final ImageView imageView = imageViewReference.get();
                if (imageView != null) {
                    imageView.setImageBitmap(bitmap);
                }
            }
        }
    }

    //This is a method for load a scaled down version into memory
    public static Bitmap decodeSampledBitmapFromPath(String imagePath, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        try {
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);
            return bitmap;
        } catch (OutOfMemoryError outOfMemoryError){
            return null;
        }
        //return bitmap;
    }
    //This is a method to calculate a sample size value that is a power of two based on a target width and height
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    /** Create a file Uri for saving an image or video */
    public static File getOutputMediaFile(Context context, int type){
        // To be safe, you should check that the SDCard is mounted
        // Check that the SDCard is mounted
        if (Environment.getExternalStorageState() == null) {
            return null;
        }
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.
                DIRECTORY_DCIM), FOLDER);
        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                ToastHelper.showErrorToast(context, "Failed to create directory");
                Log.d(FOLDER, "Failed to create directory: " + FOLDER);
                return null;
            }
        }
        // Create a File for saving an image or video
        File mediaFile;
        switch (type){
            case MEDIA_TYPE_IMAGE:
                // For unique image file name appending current timeStamp with file name
                mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_"+
                        createUniqueName().toString() + ".jpg");
                break;
            case MEDIA_TYPE_VIDEO:
                // For unique video file name appending current timeStamp with file name
                mediaFile = new File(mediaStorageDir.getPath() + File.separator + "VID_"+
                        createUniqueName().toString() + ".3gp");
                break;
            default:
                mediaFile = null;
                break;
        }
        return mediaFile;
    }
    /** For unique file name appending current timeStamp with file name */
    public static String createUniqueName(){
        java.util.Date date= new java.util.Date();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date.getTime());
        return timeStamp;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void onSaveInstanceState(Bundle bundle){
        bundle.putSerializable(MEDIA_SELECTOR, this);
        bundle.putParcelable(MEDIA_URI, mediaUri);
    }

    public static MediaSelector restoreFromInstanceState(Bundle bundle, Context context){
        if (bundle != null){
            MediaSelector mediaSelector = (MediaSelector) bundle.getSerializable(MEDIA_SELECTOR);
            mediaSelector.setContext(context);
            mediaSelector.setMediaUri((Uri) bundle.getParcelable(MEDIA_URI));
            return mediaSelector;
        }
        return new MediaSelector(context);
    }
}
