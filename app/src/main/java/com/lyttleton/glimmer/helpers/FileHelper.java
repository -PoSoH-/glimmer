package com.lyttleton.glimmer.helpers;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;

import com.lyttleton.glimmer.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

public class FileHelper {

    public static byte[] readBytes(String filePath){
        byte [] bytes = null;
        File file = new File(filePath);
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(file);
            bytes = readBytes(fileInputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) try {
                fileInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return bytes;
    }

    public static byte[] readBytes(InputStream inputStream) throws IOException {
        // this dynamically extends to take the bytes you read
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

        // this is storage overwritten on each iteration with bytes
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        // we need to know how may bytes were read to write them to the byteBuffer
        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }

        // and then we can return your byte array.
        return byteBuffer.toByteArray();
    }

    public static String getFileExtension(String path){
        String extension = "";

        int i = path.lastIndexOf('.');
        if (i > 0) extension = path.substring(i + 1);

        return extension;
    }


    public static boolean isImageSizeLessThen5Mb(long imageSize){
        if (imageSize < 5242880){
            return true;
        } else {
            return false;
        }
    }

    public static long getFileSizeBytes(String filePath){
        File file = new File(filePath);
        return file.length();
    }

    public static boolean isFileSizeLessThen5Mb(long imageSize){
        if (imageSize < 5242880){
            return true;
        } else {
            return false;
        }
    }

    public static boolean isFileSizeLessThen10Mb(String filePath, Context context){
        long imageSize = getFileSizeBytes(filePath);
        if (imageSize < 10485760){
            return true;
        } else {
            ToastHelper.showErrorToast(context, context.getString(R.string.size_of_selected_photo) +
                    " " + FileHelper.getSizeFormatted(imageSize) + ". Maximum photo size is 10 Mb.");
            return false;
        }
    }

    public static String getSizeFormatted(float size){
        String sizeString;
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        if (size < 1024 * 1024)
            sizeString = decimalFormat.format(size / 1024) + " Kb";
        else if (size < 1024 * 1024 * 1024)
            sizeString = decimalFormat.format(size / 1024 / 1024) + " Mb";
        else
            sizeString = decimalFormat.format(size / 1024 / 1024 / 1024) + " Gb";
        return sizeString;
    }

    public static long getLengthOfVideoFile(String videoFilePath){
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(videoFilePath);
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long timeInMilliSec = Long.parseLong( time );
        long durationInSec = timeInMilliSec / 1000;

        return durationInSec;
    }

    public static int getWidthOfImageFile(String imageFilePath){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        //Returns null, sizes are in the options variable
        BitmapFactory.decodeFile(imageFilePath, options);
        int width = options.outWidth;
        int height = options.outHeight;
        //If you want, the MIME type will also be decoded (if possible)
        String type = options.outMimeType;

        return width;
    }

    /** For getting the real path of file from the uri */
    public static String getPathFromUri(Context context, Uri contentUri) {
        String result = null;
        try {
            Cursor cursor = context.getContentResolver().query(contentUri, null, null, null, null);
            if (cursor == null) {
                // Source is Drop Box or other similar local file path
                result = contentUri.getPath();
            } else {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                result = cursor.getString(idx);
                cursor.close();
            }
        }catch (Exception e) {
            Log.e("FileHelper", "Error getting file name from Uri: " + e.getMessage());
        }
        return result;
    }

    /** For getting the Uri of latest making photo or video */
    public static Uri getLastPhotoOrVideo(Context context) {
        String[] columns = { MediaStore.MediaColumns.DATA, MediaStore.MediaColumns.DATE_ADDED };
        ContentResolver cr = context.getContentResolver();
        Cursor cursor = cr.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, columns, null, null, MediaStore.MediaColumns.DATE_ADDED + " DESC");
        int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String path = cursor.getString(columnIndex);
        cursor.close();

        return Uri.fromFile(new File(path));
    }

    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @author paulburke
     */
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId( Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[] { split[1] };
                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = { column };
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null) cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static boolean isImage(String filePath){
        if (TextUtils.isEmpty(filePath)) return false;
        else {
            String fileExtension = FileHelper.getFileExtension(filePath).toLowerCase();
            return fileExtension.equals("jpg") || fileExtension.equals("png")
                    || fileExtension.equals("jpeg") || fileExtension.equals("bmp");
        }
    }

//    /**
//     * Create a file Uri for saving an image or video
//     */
//    public static File getOutputMediaFile(Context context, int type){
//        // To be safe, you should check that the SDCard is mounted
//        // Check that the SDCard is mounted
//        if (Environment.getExternalStorageState() == null) {
//            return null;
//        }
//        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), MainMapActivity.FOLDER);
//        // Create the storage directory if it does not exist
//        if (! mediaStorageDir.exists()){
//            if (! mediaStorageDir.mkdirs()){
//                Toast.makeText(context, "Failed to create directory " + MainMapActivity.FOLDER, Toast.LENGTH_LONG).show();
//                Log.d(MainMapActivity.FOLDER, "Failed to create directory: " + MainMapActivity.FOLDER);
//                return null;
//            }
//        }
//        // Create a File for saving an image or video
//        File mediaFile;
//        switch (type){
//            case MainMapActivity.MEDIA_TYPE_IMAGE:
//                // For unique image file name appending current timeStamp with file name
//                mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_"+ createUniqueName().toString() + ".jpg");
//                break;
//            case MainMapActivity.MEDIA_TYPE_VIDEO:
//                // For unique video file name appending current timeStamp with file name
//                mediaFile = new File(mediaStorageDir.getPath() + File.separator + "VID_"+ createUniqueName().toString() + ".3gp");
//                break;
//            default:
//                mediaFile = null;
//                break;
//        }
//        return mediaFile;
//    }

    /**
     * For unique file name appending current timeStamp with file name
     */
    public static String createUniqueName(){
        java.util.Date date= new java.util.Date();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date.getTime());
        return timeStamp;
    }

    public static String getFileName(String url){
        return url.substring(url.lastIndexOf('/') + 1, url.length());
    }
}
