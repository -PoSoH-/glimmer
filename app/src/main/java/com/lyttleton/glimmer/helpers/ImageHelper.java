package com.lyttleton.glimmer.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lyttleton.glimmer.R;

public class ImageHelper {

    public static Bitmap createMapIcon(Context context, String text) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.
                LAYOUT_INFLATER_SERVICE);
        //Inflate the layout into a view and configure it
        RelativeLayout view = new RelativeLayout(context);
        inflater.inflate(R.layout.marker, view, true);
        TextView marker_text = (TextView) view.findViewById(R.id.marker_text);
        marker_text.setText(text);

        ImageView marker_icon = (ImageView) view.findViewById(R.id.marker_icon);
        marker_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.pin_map));
        //Provide it with a layout params. It should necessarily be wrapping the content as we not
        // really going to have a parent for it.
        view.setLayoutParams(new ViewGroup.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT));
        //Pre-measure the view so that height and width don't remain null.
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        //Assign a size and position to the view and all of its descendants
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        //Create the bitmap
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        //Create a canvas with the specified bitmap to draw into
        Canvas c = new Canvas(bitmap);
        //Render this view (and all of its children) to the given Canvas
        view.draw(c);
        return bitmap;
    }

    public static Bitmap decodeSampledBitmapFromPath(String imagePath, int maxSize) {

        // First decode with inJustDecodeBounds=true to check dimensions
        double coef = 1.0;
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath, options);

        if(options.outWidth > options.outHeight) {
            if (maxSize == -1) maxSize = options.outWidth;
        }
        else {
            if (maxSize == -1) maxSize = options.outHeight;
        }
        double dW = options.outWidth;
        double dH = options.outHeight;
        if(options.outWidth > options.outHeight){  //  Landscape mode
            coef = dW/dH;
            if(options.outWidth > maxSize){
                // Calculate inSampleSize
                options.inSampleSize = calculateInSampleSize(options, maxSize, (int)(maxSize/coef));
            }
        }else {  //  Portrait mode
            coef = dH/dW;
            if(options.outHeight > maxSize){
                // Calculate inSampleSize
                options.inSampleSize = calculateInSampleSize(options, (int)(maxSize/coef), maxSize);
            }
        }

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        try {
            return BitmapFactory.decodeFile(imagePath, options);
        } catch (OutOfMemoryError outOfMemoryError){
            return null;
        }
        //return bitmap;
    }

    //This is a method to calculate a sample size value that is a power of two based on a target width and height
    public static int   calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    || (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

}
