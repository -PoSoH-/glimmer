package com.lyttleton.glimmer.helpers;

import android.content.Context;

public class TextHelper {
    public static String makeFirstCapital(Context context, int textResId){
        String string = context.getString(textResId).toLowerCase();
        return string.substring(0, 1).toUpperCase() + string.substring(1);
    }
}
