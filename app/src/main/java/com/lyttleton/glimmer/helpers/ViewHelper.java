package com.lyttleton.glimmer.helpers;

import android.os.Build;
import android.view.View;
import android.view.ViewTreeObserver;

public class ViewHelper {

    public static void removeOnGlobalLayoutListener(View view, ViewTreeObserver.
            OnGlobalLayoutListener listener){
        if (Build.VERSION.SDK_INT < 16) {
            view.getViewTreeObserver().removeGlobalOnLayoutListener(listener);
        } else {
            view.getViewTreeObserver().removeOnGlobalLayoutListener(listener);
        }
    }
}
