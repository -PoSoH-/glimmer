package com.lyttleton.glimmer.helpers;

import android.content.Context;

import com.lyttleton.glimmer.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TimeHelper {

    public static final long SECOND_MILLIS = 1000;
    public static final long MINUTE_MILLIS = 60 * SECOND_MILLIS;
    public static final long HOUR_MILLIS = 60 * MINUTE_MILLIS;
    public static final long DAY_MILLIS = 24 * HOUR_MILLIS;
    public static final long WEEK_MILLIS = 7 * DAY_MILLIS;
    public static final String DATE_FORMAT = "EEE dd MMMM yyyy";

    public static String makeTimeframe(Date date, Context context){
        String timeFrame;
        long timeDifference = new Date().getTime() - date.getTime();
        int weeks = (int) (timeDifference / WEEK_MILLIS),
                days = (int) ((timeDifference - weeks * WEEK_MILLIS) / DAY_MILLIS),
                hours = (int) ((timeDifference - weeks * WEEK_MILLIS - days * DAY_MILLIS) / HOUR_MILLIS),
                minutes = (int) ((timeDifference - weeks * WEEK_MILLIS - days * DAY_MILLIS
                        - hours * HOUR_MILLIS) / MINUTE_MILLIS),
                seconds = (int) ((timeDifference - weeks * WEEK_MILLIS - days * DAY_MILLIS
                        - hours * HOUR_MILLIS - minutes * MINUTE_MILLIS) / SECOND_MILLIS);

        String ago = " " + context.getString(R.string.ago);

        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
        if (timeDifference > WEEK_MILLIS * 4) timeFrame = format.format(date);
        else if (weeks > 0) timeFrame = weeks + " " + (weeks > 1 ? context.getString(R.string.weeks)
                : context.getString(R.string.week)) + ago;
        else if (days > 0) timeFrame = days + " " + (days > 1 ? context.getString(R.string.days)
                : context.getString(R.string.day)) + ago;
        else if (hours > 0) timeFrame = hours + " " + (hours > 1 ? context.getString(R.string.hours)
                : context.getString(R.string.hour)) + ago;
        else if (minutes > 0) timeFrame = minutes + " " + (minutes > 1 ? context.getString(R.
                string.mins) : context.getString(R.string.min)) + ago;
        else if (seconds > 0) timeFrame = seconds + " " + (seconds > 1 ? context.getString(R.
                string.seconds) : context.getString(R.string.second)) + ago;
        else timeFrame = 0 + " " + context.getString(R.string.seconds) + ago;

        return timeFrame;
    }

    public static Calendar getCalendarDay(int dayIndex){
        Calendar newCalendar = (Calendar) Calendar.getInstance().clone();
        newCalendar.roll(Calendar.DAY_OF_YEAR, dayIndex);

        return newCalendar;
    }
}
