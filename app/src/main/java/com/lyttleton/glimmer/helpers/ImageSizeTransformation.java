package com.lyttleton.glimmer.helpers;

import android.graphics.Bitmap;

import com.squareup.picasso.Transformation;

public class ImageSizeTransformation implements Transformation {

    private int imageWidth;

    public ImageSizeTransformation(int imageWidth) {
        this.imageWidth = imageWidth;
    }

    @Override
    public Bitmap transform(Bitmap bitmap) {
        Bitmap result;
        result = Bitmap.createBitmap(bitmap, 0, 0, imageWidth, imageWidth);
        bitmap.recycle();
        result = bitmap;
        return result;
    }

    @Override
    public String key() {
        return "imageTransform";
    }
}