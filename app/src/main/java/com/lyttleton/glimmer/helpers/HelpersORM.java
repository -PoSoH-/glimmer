//package com.lyttleton.glimmer.helpers;
//
//import android.util.Log;
//
//import com.lyttleton.glimmer.App;
//import com.lyttleton.glimmer.adapters.PinPop;
//import com.lyttleton.glimmer.adapters.User;
//import com.lyttleton.glimmer.adapters.Venue;
//import com.lyttleton.glimmer.adapters.Wink;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by Sergiy Polishuk on 08.02.2016.
// */
//public class HelpersORM {
//
//    public static void logger(String info){
//        Log.d("glimmer", "..ORM data base working.." + info);
//    }
//
//    public static void saveAllUsers(List<User> users){
//        if(App.logger) {
//            for (User user : users) {
//                user.save();
//                logger("user saved");
//            }
//        }
//    }
//
//    public static void adUser(User user){
//        user.save();
//        logger("user saved");
//    }
//
//    public static void deleteUserFromDataBase(String ObjectId){
//        User.deleteAll(User.class, "object_id = ?", ObjectId);
//        logger("user delete");
//    }
//
//    public static void deleteAllUsers(){
//        User.deleteAll(User.class);
//        logger("all users deleted");
//    }
//
//    public static void replaceAllUsers(List<User> users){
//        deleteAllUsers();
//        saveAllUsers(users);
//    }
//
//   /*
//   Venues data base ORM
//    */
//    public static void saveAllVenues(List<Venue> venues){
//        for(Venue venue : venues){
//            venue.save();
//            logger("venue saved.." + venue.getName());
//        }
//        getAllVenues();
//    }
//
//    public static void addVenue(Venue venue){
//        venue.save();
//        logger("venue saved add.." + venue.getName());
//    }
//
//    public static void deleteVenueFromDataBase(String ObjectId){
//        Venue.deleteAll(Venue.class, "object_id = ?", ObjectId);
//        logger("venue delete item");
//    }
//
//    public static void deleteAllVenues(){
//        Venue.deleteAll(Venue.class);
//        logger("all venues deleted");
//    }
//
//    public static List<Venue> getAllVenues(){
//        List<Venue> venues = Venue.listAll(Venue.class);
//        logger("loaded is ORM all venues..." + venues.size());
//        return venues;
//    }
//
//    public static void  replaceVenuesFromDataBaseValues(List<Venue> venues){
//        deleteAllVenues();
//        saveAllVenues(venues);
//    }
//
//
//    /*
//    Winks data base ORM
//    */
//    public static void saveAllWinks(List<Wink> winks){
//        for(Wink wink : winks){
//            wink.save();
//            logger("venue saved item is all winks : " + wink.getObjectId());
//        }
//    }
//
//    public static void addWink(Wink wink){
//        wink.save();
//        logger("wink saved add" + wink.getObjectId());
//    }
//
//    public static void deleteWinksFromDataBase(String objectId){
//        Wink.deleteAll(Wink.class, "object_id = ?", objectId);
//        logger("venue delete item : " + objectId);
//    }
//
//    public static void deleteAllWinks(){
//        Wink.deleteAll(Wink.class);
//        logger("all venues deleted");
//    }
//
//    public static List<Wink> getAllWinks(){
//        logger("loaded is ORM all venues...");
//        return Wink.listAll(Wink.class);
//    }
//
//    public static void  replaceAllWinks(List<Wink> winks){
//        deleteAllWinks();
//        saveAllWinks(winks);
//    }
//
//    /*
//    PinPop data base ORM
//    */
//    public static void saveAllPinPop(List<PinPop> PinPops){
//        for(PinPop pinPop : PinPops){
//            pinPop.save();
//            logger("venue saved item is all winks : " + pinPop.getObjectId());
//        }
//    }
//
//    public static void addPinPop(PinPop pinPop){
//        pinPop.save();
//        logger("wink saved add : " + pinPop.getObjectId());
//    }
//
//    public static void deletePinPopFromDataBase(String objectId){
//        PinPop.deleteAll(Wink.class, "object_id = ?", objectId);
//        logger("venue delete item" + objectId);
//    }
//
//    public static void deleteAllPinPop(){
//        PinPop.deleteAll(PinPop.class);
//        logger("all venues deleted");
//    }
//
//    public static List<PinPop> getAllPinPop(){
//        logger("loaded is ORM all venues...");
//        return PinPop.listAll(PinPop.class);
//    }
//
//    public static void  replaceAllPinPop(List<PinPop> pinPops){
//        deleteAllPinPop();
//        saveAllPinPop(pinPops);
//    }
//
//}
