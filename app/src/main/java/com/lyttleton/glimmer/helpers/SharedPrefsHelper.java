package com.lyttleton.glimmer.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class SharedPrefsHelper {

    public static final String GLIMMER_PREFS = "GLIMMER_PREFS";
    public static final String DISCOVER_TIP = "DISCOVER_TIP";
    public static final String PIN_POP_TIP = "PIN_POP_TIP";
    public static final String POP_TIP = "POP_TIP";
    public static final String WINKS_TIP = "WINKS_TIP";
    public static final String CHAT_TIP = "CHAT_TIP_";

    public static final String PHOTOS_UPLOADING = "PHOTOS_UPLOADING";
    public static final String PHOTO_UPLOADING = "PHOTO_UPLOADING";

    public static final String USER_ID_OF_OPENED_CHAT = "USER_ID_OF_OPENED_CHAT";
    public static final String NEW_CHAT_MESSAGE_USER_IDS = "NEW_CHAT_MESSAGE_USER_IDS";
    public static final String EMPTY_JSON_ARRAY = "[]";

    public static final String HEADS_UP_READ = "HEADS_UP_READ";

    public static final String UPLOADING_PHOTOS_INDEXES = "UPLOADING_PHOTOS_INDEXES";

    public static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(GLIMMER_PREFS, Context.MODE_PRIVATE);
    }

    public static void saveTip(Context context, String tipId) {
        getSharedPreferences(context).edit().putBoolean(tipId, true).apply();
    }

    public static boolean hasDiscoverTip(Context context) {
        return getSharedPreferences(context).getBoolean(DISCOVER_TIP, false);
    }

    public static boolean hasPinPopTip(Context context) {
        return getSharedPreferences(context).getBoolean(PIN_POP_TIP, false);
    }

    public static boolean hasPopTip(Context context) {
        return getSharedPreferences(context).getBoolean(POP_TIP, false);
    }

    public static boolean hasWinksTip(Context context) {
        return getSharedPreferences(context).getBoolean(WINKS_TIP, false);
    }

    public static boolean hasChatTip(Context context, String userId) {
        return getSharedPreferences(context).getBoolean(CHAT_TIP + userId, false);
    }

    public static JSONArray getNewChatMessageUserIds(Context context) {
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(getSharedPreferences(context).getString(NEW_CHAT_MESSAGE_USER_IDS,
                    EMPTY_JSON_ARRAY));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    public static void addNewChatMessageUserId(String userId, Context context) {
        JSONArray userIds = getNewChatMessageUserIds(context);
        if (userIds == null) userIds = new JSONArray();
        if (!isUserIdExists(userId, userIds)) {
            userIds.put(userId);
            getSharedPreferences(context).edit().putString(NEW_CHAT_MESSAGE_USER_IDS,
                    userIds.toString()).apply();
        }
    }

    public static boolean isUserIdExists(String userId, JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                if (jsonArray.get(i).equals(userId)) return true;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public static void removeUserIdFromNewChatMessages(String userId, Context context) {
        JSONArray jsonArray = getNewChatMessageUserIds(context);
        if (jsonArray != null) {
            JSONArray userIds = new JSONArray();
            try {
                for (int i = 0; i < jsonArray.length(); i++)
                    if (!jsonArray.get(i).equals(userId)) userIds.put(jsonArray.get(i));
                getSharedPreferences(context).edit().putString(NEW_CHAT_MESSAGE_USER_IDS,
                        userIds.toString()).apply();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static void saveUserIdOfOpenedChat(String userId, Context context){
        getSharedPreferences(context).edit().putString(USER_ID_OF_OPENED_CHAT, userId).apply();
    }

    public static void removeUserIdOfOpenedChat(Context context){
        getSharedPreferences(context).edit().remove(USER_ID_OF_OPENED_CHAT).apply();
    }

    public static String getUserIdOfOpenedChat(Context context){
        return getSharedPreferences(context).getString(USER_ID_OF_OPENED_CHAT, null);
    }

    public static void saveHeadsUpRead(Context context){
        getSharedPreferences(context).edit().putBoolean(HEADS_UP_READ, true).apply();
    }

    public static boolean isHeadsUpRead(Context context){
        return getSharedPreferences(context).getBoolean(HEADS_UP_READ, false);
    }

    public static void startPhotosUploading(Context context) {
        getSharedPreferences(context).edit().putBoolean(PHOTOS_UPLOADING, true).apply();
    }

    public static void finishPhotosUploading(Context context) {
        getSharedPreferences(context).edit().remove(PHOTOS_UPLOADING).apply();
    }

    public static void startPhotoUploading(Context context) {
        getSharedPreferences(context).edit().putBoolean(PHOTO_UPLOADING, true).apply();
    }

    public static void finishPhotoUploading(Context context) {
        getSharedPreferences(context).edit().remove(PHOTO_UPLOADING).apply();
    }

    public static boolean isPhotoUploading(Context context) {
        return getSharedPreferences(context).getBoolean(PHOTO_UPLOADING, false);
    }

    public static boolean isPhotosUploading(Context context){
        return getSharedPreferences(context).getBoolean(PHOTOS_UPLOADING, false);
    }

    public static List<Integer> getUploadingPhotosIndexes(Context context) {
        List<Integer> uploadingPhotosIndexes = new ArrayList<>();
        String uploadingPhotosIndexesString = getSharedPreferences(context).
                getString(UPLOADING_PHOTOS_INDEXES, null);
        if (!TextUtils.isEmpty(uploadingPhotosIndexesString)) {
            try {
                JSONArray jsonArray = new JSONArray(uploadingPhotosIndexesString);
                for (int i = 0; i < jsonArray.length(); i++)
                    uploadingPhotosIndexes.add(jsonArray.getInt(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return uploadingPhotosIndexes;
    }

    public static void addUploadingPhotoIndex(Context context, int index) {
        List<Integer> uploadingPhotosIndexes = getUploadingPhotosIndexes(context);
        if (!uploadingPhotosIndexes.contains(index)) {
            uploadingPhotosIndexes.add(index);
            saveJSONArray(context, UPLOADING_PHOTOS_INDEXES, uploadingPhotosIndexes);
        }
    }

    public static void removeUploadingPhotoIndex(Context context, int index) {
        List<Integer> uploadingPhotosIndexes = getUploadingPhotosIndexes(context);
        uploadingPhotosIndexes.remove(Integer.valueOf(index));
        saveJSONArray(context, UPLOADING_PHOTOS_INDEXES, uploadingPhotosIndexes);
    }

    public static void saveJSONArray(Context context, String key, List<Integer> values) {
        if (key == null || values == null) return;
        JSONArray jsonArray = new JSONArray();
        for (Integer integer : values)
            jsonArray.put(integer);
        getSharedPreferences(context).edit().putString(key, jsonArray.toString()).apply();
    }
}
