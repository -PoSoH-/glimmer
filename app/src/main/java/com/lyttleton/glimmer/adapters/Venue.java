package com.lyttleton.glimmer.adapters;

import android.location.Location;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.Serializable;
import java.util.ArrayList;

@Table(name = "GLIMMER_VENUE_TABLE")
public class Venue extends Model implements Serializable{

    @Column(name = "glimmer_object_id"
            , unique = true
            , onUniqueConflict = Column.ConflictAction.REPLACE)
    private String mObjectId;
    @Column(name = "glimmer_name")
    private String name;
    @Column(name = "glimmer_address")
    private String address;
    @Column(name = "glimmer_latitude_str")
    private double latitude;
    @Column(name = "glimmer_longitude_str")
    private double longitude;
    @Column(name = "glimmer_user_photos")
    private String userPhotos;
    @Column(name = "glimmer_pin_count")
    private int pinPopCount;
    @Column(name = "glimmer_popular")
    private int popular;

    //Methods

    private void setPopular(int popular){this.popular = popular;}
    private int getPopular(){return this.popular;}

    public String getObjectId() {
        return mObjectId;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public int getPinPopCount() {
        return pinPopCount;
    }

    public void setPinPopCount(int pinPopCount) {
        this.pinPopCount = pinPopCount;
    }

    public Location getLocation(){
        Location location = new Location("");
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        return location;
    }

    //constructors
    public ArrayList<String> getUserPhotos() {
        JSONArray array = new JSONArray();
        ArrayList<String> data = new ArrayList<>();
        if(this.userPhotos != null) {
            try {
                array = new JSONArray(this.userPhotos);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                for (int pos = 0, len = array.length(); pos < len; pos++) {
                    data.add(array.getString(pos));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return data;
    }

    public Venue(){super();}

    public Venue(String objectId, String name, String address, double latitude, double longitude,
                 ArrayList<String> userPhotos) {
        super();
        this.mObjectId = objectId;
        this.name = name;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        if(userPhotos != null) {
            JSONArray data = new JSONArray(userPhotos);
            this.userPhotos = data.toString();
        }
//        String url = "http://files.parsetfss.com/231a48da-8e97-4e79-a510-f090d2ac5908/tfss-24c361b2-cf58-42a3-8e12-df22c90bb484-user_photo.jpg";
//        this.userPhotos = new ArrayList<>(Arrays.asList(url, url, url, url, url));
    }
}
