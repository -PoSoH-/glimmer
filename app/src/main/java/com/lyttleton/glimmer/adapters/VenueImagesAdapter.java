package com.lyttleton.glimmer.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.helpers.CircleTransform;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class VenueImagesAdapter extends BaseAdapter {
    private Context context;
    private List<String> imageUrls;
    private CircleTransform transform = new CircleTransform();

    public VenueImagesAdapter(Context c, List<String> imageUrls) {
        context = c;
        this.imageUrls = imageUrls != null ? imageUrls : new ArrayList<String>();
    }

    public int getCount() {
        return imageUrls.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public void setItems(List<String> imageUrls){
        this.imageUrls = imageUrls != null ? imageUrls : new ArrayList<String>();
        notifyDataSetChanged();
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        int imageSize = (int) context.getResources().getDimension(R.dimen.venue_user_icon_size);
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(context);
            imageView.setLayoutParams(new GridView.LayoutParams(imageSize, imageSize));
            imageView.setFocusable(false);
            imageView.setFocusableInTouchMode(false);
        } else imageView = (ImageView) convertView;

        Picasso picasso = Picasso.with(context);
        picasso.cancelRequest(imageView);
        picasso.load(imageUrls.get(position)).resize(imageSize, 0).transform(transform).into(imageView);
        return imageView;
    }

    // Disable GridView click
    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }
}
