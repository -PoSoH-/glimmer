package com.lyttleton.glimmer.adapters;

//import com.orm.SugarRecord;
//import com.orm.dsl.Column;
//import com.orm.dsl.Table;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.Serializable;
import java.util.ArrayList;

//@Table(name = "glimmer_users_list")
@Table(name = "GLIMMER_USER")
public class User extends Model implements Serializable {

    @Column(name = "glimmer_object_id"
            , unique = true
            , onNullConflict = Column.ConflictAction.REPLACE)
    private String objectId;
    @Column(name = "glimmer_first_name")
    private String firstName;
    @Column(name = "glimmer_gender")
    private String gender;
    @Column(name = "glimmer_photos")
    private String photos;
    @Column(name = "glimmer_matched")
    private boolean matched;
    @Column(name = "glimmer_uploaded")
    private boolean uploadedDailyProfiler;

    public User(){super();}
//    public User(){}

    public User(String objectId, String firstName, String gender, ArrayList<String> photos,
                boolean uploadedDailyProfiler) {
        this.objectId = objectId;
        this.firstName = firstName;
        this.gender = gender;
        JSONArray data = new JSONArray();
        for(String i : photos) {
            data.put(i);
        }
        this.photos = data.toString();
        this.uploadedDailyProfiler = uploadedDailyProfiler;
    }

    public String getObjectId() {
        return objectId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getGender() {
        return gender;
    }

    public ArrayList<String> getPhotos() {
        JSONArray array = new JSONArray();
        try {
            array = new JSONArray(this.photos);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ArrayList<String> data = new ArrayList<>();
        try {
            for(int pos=0, len= array.length(); pos<len; pos++){
                data.add(array.getString(pos));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

    public boolean hasPhotos() {
        return photos != null && getPhotos().size() > 0;
    }

    public String getPhotoUrl(){
        return getPhotos().get(0);
    }

    public boolean isMatched() {
        return matched;
    }

    public void setMatched(boolean matched) {
        this.matched = matched;
    }

    public void setPhotos(ArrayList<String> photos) {
        JSONArray data = new JSONArray();
        for(String i : photos) {
            data.put(i);
        }
        this.photos = data.toString();
    }

    public boolean isUploadedDailyProfiler() {
        return uploadedDailyProfiler;
    }
}
