package com.lyttleton.glimmer.adapters;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
//import com.orm.SugarRecord;

//import com.orm.SugarRecord;
//import com.orm.dsl.Column;
//import com.orm.dsl.Table;

import java.util.Date;
import java.util.List;

@Table(name = "GLIMMER_CHAT")
public class Chat extends Model{
    @Column(name = "glimmer_object_id", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String objectId;
    @Column(name = "glimmer_message")
    private String message;
    private User sender;
    @Column(name = "glimmer_sender_id")
    private String senderId;
    private User receiver;
    @Column(name = "glimmer_receiver_id")
    private String receiverId;
    @Column(name = "glimmer_date")
    private String date;
    @Column(name = "glimmer_media_url")
    private String mediaUrl;
    @Column(name = "glimmer_media_path")
    private String mediaPath;

    public Chat(){super();}
//    public Chat(){}

    public Chat(String objectId, String message, User sender, User receiver, Date date) {
        this.objectId = objectId;
        this.message = message;
        this.sender = sender;
        this.senderId = sender.getObjectId();
        this.receiver = receiver;
        this.receiverId = receiver.getObjectId();
        this.date = String.valueOf(date.getTime());
    }

    public Chat(String objectId, String message, User sender, User receiver, Date date,
                String mediaUrl) {
        this(objectId, message, sender, receiver, date);
        this.mediaUrl = mediaUrl;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getMessage() {
        return message;
    }

    public User getSender() {
        return sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public Date getDate() {
        Date date = new Date(); //.setTime(this.date);
        date.setTime(Long.valueOf(this.date));
        return date;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public String getMediaPath() {
        return mediaPath;
    }

    public void setMediaPath(String mediaPath) {
        this.mediaPath = mediaPath;
    }

    public String getSenderId(){return this.senderId;}
    public String getReceiverId(){return this.receiverId;}

    public void addUserSender(User user){this.sender = user;}
    public void addUserReceiver(User user){this.receiver = user;}
}
