package com.lyttleton.glimmer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.lyttleton.glimmer.R;

public class MyProfileListAdapter extends ArrayAdapter<String> {

    private final Context context;
    private final String[] values;

    public MyProfileListAdapter(Context context, String[] values) {
        super(context, R.layout.user_profile_item, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.user_profile_item, null, true);
            viewHolder = new ViewHolder((TextView)convertView.findViewById(R.id.user_profile_item_txt));

            convertView.setTag(viewHolder);
        } else viewHolder = (ViewHolder) convertView.getTag();

        viewHolder.listItemTitle.setText(values[position]);

        return convertView;
    }

    private class ViewHolder{
        public TextView listItemTitle;

        private ViewHolder(TextView listItemTitle) {
            this.listItemTitle = listItemTitle;
        }
    }

}
