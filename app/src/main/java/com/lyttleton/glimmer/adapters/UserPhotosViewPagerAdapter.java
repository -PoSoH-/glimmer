package com.lyttleton.glimmer.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.lyttleton.glimmer.fragments.PhotoFragment;
import com.lyttleton.glimmer.fragments.PinPopListFragment;

import java.util.ArrayList;

public class UserPhotosViewPagerAdapter extends FragmentStatePagerAdapter {

    private SparseArray<PhotoFragment> registeredFragments = new SparseArray<>();
    ArrayList<String> imageUrls;
    private boolean createdForProfiler;

    public UserPhotosViewPagerAdapter(FragmentManager fm, ArrayList<String> imageUrls) {
        this(fm, imageUrls, false);
    }

    public UserPhotosViewPagerAdapter(FragmentManager fm, ArrayList<String> imageUrls,
                                      boolean createdForProfiler) {
        super(fm);
        this.imageUrls = imageUrls;
        this.createdForProfiler = createdForProfiler;
    }

    @Override
    public Fragment getItem(int i) {
        return PhotoFragment.create(imageUrls.get(i), createdForProfiler);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        PhotoFragment fragment = (PhotoFragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public PhotoFragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }

    @Override
    public int getCount() {
        return imageUrls.size();
    }

    public void setImageUrls(ArrayList<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    // Need for recreating all items on notifyDataSetChanged()
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
