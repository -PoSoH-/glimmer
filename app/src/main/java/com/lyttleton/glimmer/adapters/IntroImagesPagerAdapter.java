package com.lyttleton.glimmer.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.lyttleton.glimmer.fragments.IntroImageFragment;

public class IntroImagesPagerAdapter extends FragmentStatePagerAdapter {

    private SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();
    private int countOfPages;

    public IntroImagesPagerAdapter(FragmentManager fm, int countOfPages) {
        super(fm);
        this.countOfPages = countOfPages;
    }

    @Override
    public Fragment getItem(int i) {
        return IntroImageFragment.create(i);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }

    @Override
    public int getCount() {
        return countOfPages;
    }
}
