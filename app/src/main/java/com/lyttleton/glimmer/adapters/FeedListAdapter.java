package com.lyttleton.glimmer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.fragments.FeedListFragment;
import com.lyttleton.glimmer.helpers.CircleTransform;
import com.lyttleton.glimmer.helpers.SharedPrefsHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.List;

public class FeedListAdapter extends ArrayAdapter<Wink> {

    private final Context context;
    private CircleTransform transform;
    private FeedListFragment fragment;
    private List<Wink> data;
    private JSONArray newMessageUserIds;

    public FeedListAdapter(Context context, FeedListFragment fragment) {
        super(context, R.layout.wink_item);
        this.context = context;
        transform = new CircleTransform();
        this.fragment = fragment;
        newMessageUserIds = SharedPrefsHelper.getNewChatMessageUserIds(context);
    }

    public void setData(List<Wink> data){
        clear();
        for (Wink wink : data) add(wink);
        this.data = data;
    }

    public List<Wink> getData() {
        return data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.
                    LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.wink_item, null, true);
            viewHolder = new ViewHolder((TextView)convertView.findViewById(R.id.title_tv),
                    (TextView) convertView.findViewById(R.id.text_tv),
                    (ImageView) convertView.findViewById(R.id.wink_icon_iv),
                    (ImageView) convertView.findViewById(R.id.new_message_indicator_iv));

            convertView.setTag(viewHolder);
        } else viewHolder = (ViewHolder) convertView.getTag();

        Wink wink = getItem(position);


        if(wink.getVenue() != null){
            viewHolder.titleTv.setText(wink.getVenue().getName());
        }

        String text = null;
        Context context = getContext();
        switch (wink.getType()){
            case Wink.CLUE_TYPE:
                text = context.getString(R.string.clue) + " (" + wink.getClue() + ")";
                break;
            case Wink.PHOTO_TYPE:
                text = context.getString(R.string.photo) + " ("
                        + context.getString(R.string.tap_to_see) + ")";
                break;
            case Wink.WINK_MATCH_TYPE:
            case Wink.MATCH_TYPE:
                text = wink.getUser().getFirstName();
                if (SharedPrefsHelper.isUserIdExists(wink.getUser().getObjectId(), newMessageUserIds))
                    viewHolder.newMessageIndicatorIv.setVisibility(View.VISIBLE);
                else viewHolder.newMessageIndicatorIv.setVisibility(View.GONE);
                break;
        }
        viewHolder.textTv.setText(text);

        viewHolder.iconIv.setVisibility(View.VISIBLE);
        User user = wink.getUser();
        if (wink.getType() == Wink.MATCH_TYPE) {
            if (user.hasPhotos()) {
                int iconSize = (int) getContext().getResources().getDimension(R.dimen.feed_icon_size);
                Picasso.with(context).load(user.getPhotoUrl()).resize(iconSize, 0).
                        transform(transform).into(viewHolder.iconIv);
            } else viewHolder.iconIv.setVisibility(View.GONE);
        } else viewHolder.iconIv.setBackgroundResource(R.drawable.wink);

        int loadItemIndex = getCount() - 3;
        if (position == loadItemIndex || loadItemIndex < 0) fragment.onGetLastElements();

        return convertView;
    }

    private class ViewHolder{
        public TextView titleTv, textTv;
        public ImageView iconIv, newMessageIndicatorIv;

        private ViewHolder(TextView titleTv, TextView textTv, ImageView iconIv,
                           ImageView newMessageIndicatorIv) {
            this.titleTv = titleTv;
            this.textTv = textTv;
            this.iconIv = iconIv;
            this.newMessageIndicatorIv = newMessageIndicatorIv;
        }
    }

    public void update(){
        newMessageUserIds = SharedPrefsHelper.getNewChatMessageUserIds(context);
        notifyDataSetChanged();
    }
}
