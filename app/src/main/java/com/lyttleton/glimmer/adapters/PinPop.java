package com.lyttleton.glimmer.adapters;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

//import com.orm.SugarRecord;
//import com.orm.dsl.Column;
//import com.orm.dsl.Table;

import java.io.Serializable;
import java.util.Date;

@Table(name = "GLIMMER_PIN_POP")
public class PinPop extends Model implements Serializable {

    @Column(name = "glimmer_type")
    private int type;
    @Column(name = "glimmer_create_date")
    private Date createDate;
    @Column(name = "glimmer_user_id")
    private String userId;
    @Column(name = "glimmer_venue_id")
    private String venueId;
    @Column(name = "glimmer_object_id")
    private String objectId;

    private User user;
    private Venue venue;

//    public PinPop() {super();}
public PinPop() {}

    public PinPop(int type, Date createDate, User user, Venue venue, String objectId) {
        super();
        this.type = type;
        this.createDate = createDate;
        this.user = user;
        this.venue = venue;
        this.objectId = objectId;
        this.userId = user.getObjectId();
        this.venueId = venue.getObjectId();
    }

    public int getType() {
        return type;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public boolean isPin() {
        return type == 0;
    }

    public User getUser() {
        return user;
    }

    public Venue getVenue() {
        return venue;
    }

    public String getObjectId() {
        return objectId;
    }

    public String getUserId(){return this.userId;}

    public String getVenueId(){return this.venueId;}

    public void addUser(User user){this.user = user;}

    public void addVenue(Venue venue){this.venue = venue;}
}
