package com.lyttleton.glimmer.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.lyttleton.glimmer.fragments.FeedListFragment;

public class FeedViewPagerAdapter extends FragmentStatePagerAdapter {

    public static final int FRAGMENTS_COUNT = 2;
    private SparseArray<Fragment> registeredFragments = new SparseArray<>();

    public FeedViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        return FeedListFragment.create(i + 1); // Removed all tab
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }

    @Override
    public int getCount() {
        return FRAGMENTS_COUNT;
    }
}
