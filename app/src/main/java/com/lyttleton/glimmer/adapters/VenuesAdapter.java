package com.lyttleton.glimmer.adapters;

import android.content.Context;
import android.location.Location;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.fragments.ListPaging;
import com.lyttleton.glimmer.fragments.PopularVenuesFragment;
import com.lyttleton.glimmer.helpers.CircleTransform;
import com.lyttleton.glimmer.views.NonScrollableGridView;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.List;

public class VenuesAdapter extends ArrayAdapter<Venue> {

    private final Context context;
    private Location userLocation;
    private ListPaging listPaging;

    public VenuesAdapter(Context context, ListPaging listPaging) {
        super(context, R.layout.venue_item);
        this.context = context;
        this.listPaging = listPaging;
    }

    public void setUserLocation(Location userLocation) {
        this.userLocation = userLocation;
    }

    public void setData(List<Venue> data, Location userLocation){
        clear();
        this.userLocation = userLocation;
        for (Venue venue : data) add(venue);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        Venue venue = getItem(position);
        List<String> userPhotos = venue.getUserPhotos();

        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.
                    LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.venue_item, null, true);
            viewHolder = new ViewHolder((TextView)convertView.findViewById(R.id.venue_name_tv),
                    (NonScrollableGridView) convertView.findViewById(R.id.photos_gv));

            viewHolder.photosGv.setAdapter(new VenueImagesAdapter(context, userPhotos));

            convertView.setTag(viewHolder);
        } else viewHolder = (ViewHolder) convertView.getTag();

        String name = venue.getName();
        if (userLocation != null) {
            int distance = (int) venue.getLocation().distanceTo(userLocation);
            name += " (";
            if (distance < 1000) name += distance + "m";
            else name += new DecimalFormat("#.#").format(distance / 1000F) + "km";
            name += ")";
        }
        viewHolder.listItemTitle.setText(name);

        GridView photosGv = viewHolder.photosGv;
        photosGv.setVisibility(View.GONE);
        if (userPhotos != null && userPhotos.size() > 0) {
            ((VenueImagesAdapter) photosGv.getAdapter()).setItems(userPhotos);
            photosGv.setVisibility(View.VISIBLE);
        }

        int loadItemIndex = getCount() - 3;
        if (position == loadItemIndex || loadItemIndex < 0) listPaging.onGetLastElements();

        return convertView;
    }

    private class ViewHolder{
        TextView listItemTitle;
        NonScrollableGridView photosGv;

        private ViewHolder(TextView listItemTitle, NonScrollableGridView photosGv) {
            this.listItemTitle = listItemTitle;
            this.photosGv = photosGv;
        }
    }
}
