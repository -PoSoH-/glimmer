package com.lyttleton.glimmer.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.lyttleton.glimmer.fragments.DiscoverFragment;
import com.lyttleton.glimmer.fragments.FeedFragment;
import com.lyttleton.glimmer.fragments.MyProfileFragment;

public class MainViewPagerAdapter extends FragmentStatePagerAdapter {

    public static final int FRAGMENTS_COUNT = 3;
    private SparseArray<Fragment> registeredFragments = new SparseArray<>();

    public MainViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = null;
        switch (i){
            case 0:
                fragment = new FeedFragment();
                break;
            case 1:
                fragment = new DiscoverFragment();
                break;
            case 2:
                fragment = new MyProfileFragment();
                break;
        }
        return fragment;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }

    @Override
    public int getCount() {
        return FRAGMENTS_COUNT;
    }
}
