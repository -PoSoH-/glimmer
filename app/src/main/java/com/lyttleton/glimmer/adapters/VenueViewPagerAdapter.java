package com.lyttleton.glimmer.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.lyttleton.glimmer.fragments.PinPopListFragment;
import com.lyttleton.glimmer.fragments.scroll_tab_holder.ScrollChangeListener;

public class VenueViewPagerAdapter extends FragmentStatePagerAdapter {

    public static final int FRAGMENTS_COUNT = 2;
    private SparseArray<PinPopListFragment> registeredFragments = new SparseArray<>();
    private Venue venue;

    public VenueViewPagerAdapter(FragmentManager fm, Venue venue) {
        super(fm);
        this.venue = venue;
    }

    @Override
    public Fragment getItem(int i) {
        PinPopListFragment fragment = null;
        switch (i){
            case 0:
                fragment = PinPopListFragment.create(PinPopListFragment.TODAY_TYPE, venue);
                break;
            case 1:
                fragment = PinPopListFragment.create(PinPopListFragment.EVER_TYPE, venue);
                break;
        }
        return fragment;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        PinPopListFragment fragment = (PinPopListFragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public PinPopListFragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }

    @Override
    public int getCount() {
        return FRAGMENTS_COUNT;
    }
}
