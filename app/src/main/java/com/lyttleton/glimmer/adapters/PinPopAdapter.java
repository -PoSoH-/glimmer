package com.lyttleton.glimmer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.fragments.PinPopListFragment;
import com.lyttleton.glimmer.helpers.CircleTransform;
import com.lyttleton.glimmer.helpers.TimeHelper;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PinPopAdapter extends ArrayAdapter<PinPop> {

    private final Context context;
    private int fragmentType;
    private CircleTransform transform;
    private PinPopListFragment fragment;

    public PinPopAdapter(Context context, int fragmentType, PinPopListFragment fragment) {
        super(context, R.layout.pin_pop_item);
        this.context = context;
        this.fragmentType = fragmentType;
        transform = new CircleTransform();
        this.fragment = fragment;
    }

    public void setData(List<PinPop> data){
        clear();
        for (PinPop pinPop : data) add(pinPop);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.
                    LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.pin_pop_item, null, true);
            viewHolder = new ViewHolder((TextView)convertView.findViewById(R.id.title_tv),
                    (TextView) convertView.findViewById(R.id.pin_pop_type),
                    (TextView) convertView.findViewById(R.id.pin_pop_date),
                    (ImageView) convertView.findViewById(R.id.icon_iv));

            convertView.setTag(viewHolder);
        } else viewHolder = (ViewHolder) convertView.getTag();

        PinPop pinPop = getItem(position);

        String title = null;
        if (fragmentType == PinPopListFragment.VENUE_FRAGMENT_TYPE) {
            if (pinPop.getUser() != null) title = pinPop.getUser().getFirstName();
        } else title = pinPop.getVenue().getName();
        viewHolder.titleTv.setText(title);

        String pinPopType;
        int typeTextColor;
        if (pinPop.isPin()) {
            pinPopType = getContext().getString(R.string.is_going);
            typeTextColor = context.getResources().getColor(R.color.green);
        } else {
            pinPopType = context.getString(R.string.is_here);
            if (fragmentType == PinPopListFragment.POP_FRAGMENT_TYPE)
                pinPopType = context.getString(R.string.been_there);
            typeTextColor = context.getResources().getColor(R.color.purple);
        }
        viewHolder.pinPopType.setText(pinPopType);
        viewHolder.pinPopType.setTextColor(typeTextColor);

        String pinPopDate = "(" + TimeHelper.makeTimeframe(pinPop.getCreateDate(), getContext()) + ")";
        viewHolder.pinPopDate.setText(pinPopDate);
        viewHolder.pinPopDate.setTextColor(getContext().getResources().getColor(R.color.dark_grey));

        User user = pinPop.getUser();
        if (user != null && user.hasPhotos()) {
            viewHolder.iconIv.setVisibility(View.VISIBLE);
            int iconSize = (int) getContext().getResources().getDimension(R.dimen.pin_pop_list_icon_size);
            Picasso.with(getContext()).load(user.getPhotoUrl()).resize(iconSize, 0).
                    transform(transform).into(viewHolder.iconIv);
        } else viewHolder.iconIv.setVisibility(View.GONE);

        int loadItemIndex = getCount() - 3;
        if (position == loadItemIndex || loadItemIndex < 0) fragment.onGetLastElements();

        return convertView;
    }

    private class ViewHolder{
        public TextView titleTv, pinPopType, pinPopDate;
        private ImageView iconIv;

        private ViewHolder(TextView titleTv, TextView pinPopType, TextView pinPopDate, ImageView iconIv) {
            this.titleTv = titleTv;
            this.pinPopType = pinPopType;
            this.pinPopDate = pinPopDate;
            this.iconIv = iconIv;
        }
    }
}
