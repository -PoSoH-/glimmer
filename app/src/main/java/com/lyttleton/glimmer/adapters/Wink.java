package com.lyttleton.glimmer.adapters;

//import com.orm.SugarRecord;
//import com.orm.dsl.Column;
//import com.orm.dsl.Ignore;
//import com.orm.dsl.Table;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "GLIMMER_WINK")
public class Wink extends Model {


    public static final int CLUE_TYPE = 0;

    public static final int PHOTO_TYPE = 1;

    public static final int MATCH_TYPE = 2;

    public static final int CHAT_TYPE = 3;

    public static final int POP_TYPE = 4;

    public static final int WINK_MATCH_TYPE = 5;

    @Column(name = "glimmer_clue")
    private String clue;
    @Column(name = "glimmer_object_id", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String objectId;
    @Column(name = "glimmer_user_id")
    private String userId;

    private User user;
    @Column(name = "glimmer_venue_id")
    private String venueId;

    private Venue venue;
    @Column(name = "glimmer_type")
    private int type;

    public Wink(){super();}
//    public Wink(){}

    public Wink(String clue, User senderUser, Venue venue, int type, String objectId) {
        this.clue = clue;
        this.user = senderUser;
        this.venue = venue;
        this.type = type;
        this.objectId = objectId;
        this.userId = user.getObjectId();
        this.venueId = venue.getObjectId();
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getClue() {
        return clue;
    }

    public User getUser() {
        return user;
    }

    public Venue getVenue() {
        return venue;
    }

    public int getType() {
        return type;
    }

    public String getObjectId() {
        return objectId;
    }

    public String getUserId() {
        return userId;
    }

    public String getVenueId() {
        return venueId;
    }

    public void addUser(User user){this.user = user;}

    public void addVenue(Venue venue){this.venue = venue;}
}
