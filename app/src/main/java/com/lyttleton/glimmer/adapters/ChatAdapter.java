package com.lyttleton.glimmer.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.fragments.ChatFragment;
import com.lyttleton.glimmer.helpers.CircleTransform;
import com.lyttleton.glimmer.helpers.TimeHelper;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

public class ChatAdapter extends ArrayAdapter<Chat> {

    public static final int IMAGE_MARGIN = 40;
    private final Context context;
    private final int imageWidth;
    private CircleTransform transform;
    private ChatFragment fragment;

    public ChatAdapter(Context context, ChatFragment fragment) {
        super(context, R.layout.chat_item);
        this.context = context;
        transform = new CircleTransform();
        this.fragment = fragment;

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        imageWidth = (int) (displayMetrics.widthPixels - IMAGE_MARGIN * displayMetrics.density);
    }

    public void setData(List<Chat> data){
        clear();
        for (Chat chat : data) add(chat);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.
                    LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.chat_item, null, true);
            viewHolder = new ViewHolder((TextView)convertView.findViewById(R.id.title_tv),
                    (TextView) convertView.findViewById(R.id.date_tv),
                    (ImageView) convertView.findViewById(R.id.icon_iv),
                    (TextView) convertView.findViewById(R.id.message_tv),
                    (ImageView) convertView.findViewById(R.id.image_iv),
                    convertView.findViewById(R.id.progress_bar));

            convertView.setTag(viewHolder);
        } else viewHolder = (ViewHolder) convertView.getTag();

        Chat chat = getItem(position);

        User user = chat.getSender();
        viewHolder.titleTv.setText(user.getFirstName());
        viewHolder.dateTv.setText(TimeHelper.makeTimeframe(chat.getDate(), getContext()));

        String message = chat.getMessage();
        viewHolder.messageTv.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(message)) viewHolder.messageTv.setText(message);
        else viewHolder.messageTv.setVisibility(View.GONE);

        int userIconSize = (int) getContext().getResources().getDimension(R.dimen.feed_icon_size);
        if (user.hasPhotos()){
            viewHolder.iconIv.setVisibility(View.VISIBLE);
            Picasso.with(getContext()).load(user.getPhotoUrl()).resize(userIconSize, 0).
                    transform(transform).into(viewHolder.iconIv);
        } else viewHolder.iconIv.setVisibility(View.GONE);

        viewHolder.imageIv.setVisibility(View.VISIBLE);
        viewHolder.progressBar.setVisibility(View.VISIBLE);
        Picasso.with(getContext()).cancelRequest(viewHolder.imageIv);
        if (!TextUtils.isEmpty(chat.getMediaPath()))
            Picasso.with(getContext()).load(new File(chat.getMediaPath())).resize(imageWidth, 0).
                    into(viewHolder.imageIv);
        else if (!TextUtils.isEmpty(chat.getMediaUrl()))
            Picasso.with(getContext()).load(chat.getMediaUrl()).resize(imageWidth, 0).
                    into(viewHolder.imageIv, new Callback() {
                        @Override
                        public void onSuccess() {
                            viewHolder.progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });
        else {
            viewHolder.imageIv.setVisibility(View.GONE);
            viewHolder.progressBar.setVisibility(View.GONE);
        }

        if (position <= 3) fragment.onGetFirstElements();

        return convertView;
    }

    private class ViewHolder{
        private TextView titleTv, dateTv, messageTv;
        private ImageView iconIv, imageIv;
        private View progressBar;

        private ViewHolder(TextView titleTv, TextView dateTv, ImageView iconIv, TextView messageTv,
                           ImageView imageIv, View progressBar) {
            this.titleTv = titleTv;
            this.dateTv = dateTv;
            this.iconIv = iconIv;
            this.messageTv = messageTv;
            this.imageIv = imageIv;
            this.progressBar = progressBar;
        }
    }
}
