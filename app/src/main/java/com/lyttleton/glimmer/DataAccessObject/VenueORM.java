package com.lyttleton.glimmer.DataAccessObject;

import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.lyttleton.glimmer.App;
import com.lyttleton.glimmer.adapters.Venue;

import java.util.List;
/**
 * Created by Sergiy Polishuk on 08.02.2016.
 */
public class VenueORM {

    public static void logger(String places, String info){
        if(App.logger) {
            StringBuilder builder = new StringBuilder();
            builder.append("..");
            builder.append(places);
            builder.append("..");
            builder.append(info);
            builder.append("..");
            Log.d("glimmer", builder.toString());
        }
    }

    public static void saveAllVenues(List<Venue> venues){
        ActiveAndroid.beginTransaction();
        try {
            for(Venue venue : venues){
                venue.save();
                logger("venue saved..", venue.getName() + " - " + venue.getObjectId());
            }
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
        if(App.logger) loadAllVenues();
    }

    public static void addVenue(Venue venue){
        ActiveAndroid.beginTransaction();
        try {
            venue.save();
            ActiveAndroid.setTransactionSuccessful();
            logger("venue saved add..", venue.getName());
        } finally {
            ActiveAndroid.endTransaction();
            logger("venue saved add..", "end transaction");
        }
    }

    public static void deleteVenueFromDataBase(String objectId){
        ActiveAndroid.beginTransaction();
        try {
            new Delete().from(Venue.class).where("glimmer_object_id = ?", objectId).execute();
            ActiveAndroid.setTransactionSuccessful();
            logger("venue delete item success", objectId);
        } finally {
            ActiveAndroid.endTransaction();
            logger("venue delete items..", "end transaction");
        }
    }

    public static Venue loadVenueFromDataBase(String venueId){
        return new Select()
                .from(Venue.class)
                .where("glimmer_object_id = ?", venueId)
                .executeSingle();
    }

//    public static void deleteAllVenues(){
////        new Delete().from(Venue.class).execute();
//        logger("all venues deleted", "completed");
//    }

    public static List<Venue> loadAllVenues(){
        List<Venue> venues;
        ActiveAndroid.beginTransaction();
        try {
            venues = new Select().from(Venue.class).execute();
            logger("loaded is ORM all venues...", "count - " + venues.size());
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
            logger("venue delete items..", "end transaction");
        }
        return venues;
    }

    public static void getCountVenuesFromDataBase(){
        List<Venue> venues = new Select().from(Venue.class).execute();
        logger("Venue ORM", String.valueOf(venues.size()));
    }

    public static void replaceVenuesFromDataBaseValues(List<Venue> venues){
        for(Venue venue : venues){
//            deleteVenueFromDataBase(venue.getObjectId());
            addVenue(venue);
//            logger("Venue ORM load test", loadVenueFromDataBase(venue.getObjectId()).getObjectId());
        }
    }

//    public static void replaceAllVenues(List<Venue> venues){
//            deleteAllVenues();
//            saveAllVenues(venues);
//    }

    //sugar logic ORM working

//    public static void saveAllVenues(List<Venue> venues){
//        for(Venue venue : venues){
//            venue.save();
//            logger("venue saved..", venue.getName());
//        }
//        if(App.logger) loadAllVenues();
//    }
//
//    public static void addVenue(Venue venue){
//        venue.save();
//        logger("venue saved add..", venue.getName());
//    }
//
//    public static void deleteVenueFromDataBase(String objectId){
//        Venue.deleteAll(Venue.class, "m_object_id = ?", objectId);
//        logger("venue delete item", objectId);
//    }
//
//    public static Venue loadVenueFromDataBase(String venueId){
//        List<Venue> venues =  Venue.findWithQuery(Venue.class, "m_object_id = ?", venueId);
//        if (venues != null && venues.size()>0){
//            return venues.get(0);
//        }else{
//            return null;
//        }
//    }
//
//    public static void deleteAllVenues(){
//        Venue.deleteAll(Venue.class);
//        logger("all venues deleted", "completed");
//    }
//
//    public static List<Venue> loadAllVenues(){
//        try {
//            List<Venue> venues = Venue.listAll(Venue.class);
//            if(venues != null && venues.size()>0) {
//                logger("loaded is ORM all venues...", "count - " + venues.size());
//                return venues;
//            }else{
//                return null;
//            }
//        }catch (Exception e){
//            logger("loaded is ORM all venues...", "error list venues");
//        }
//        return null;
//    }
//
//    public static void  replaceVenuesFromDataBaseValues(List<Venue> venues){
//        for(Venue venue : venues){
//            deleteVenueFromDataBase(venue.getObjectId());
//            addVenue(venue);
//        }
//    }
}
