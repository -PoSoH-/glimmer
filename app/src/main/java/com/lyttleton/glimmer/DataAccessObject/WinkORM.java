package com.lyttleton.glimmer.DataAccessObject;

import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.lyttleton.glimmer.App;
import com.lyttleton.glimmer.adapters.Wink;

import java.util.List;

/**
 * Created by Sergiy Polishuk on 08.02.2016.
 */
public class WinkORM {


    public static void logger(String places, String info){
        if(App.logger) {
            StringBuilder builder = new StringBuilder();
            builder.append("..");
            builder.append(places);
            builder.append("..");
            builder.append(info);
            builder.append("..");
            Log.d("glimmer", builder.toString());
        }
    }

    public static void saveAllWinks(List<Wink> winks){
        ActiveAndroid.beginTransaction();
        try {
            for(Wink wink : winks){
                wink.save();
                logger("wink saved..", wink.getObjectId());
            }
         ActiveAndroid.setTransactionSuccessful();
        }
        finally {
            ActiveAndroid.endTransaction();
        }
        if(App.logger) loadAllWinks();
    }

    public static void addWink(Wink wink){
        ActiveAndroid.beginTransaction();
        try {
            wink.save();
            ActiveAndroid.setTransactionSuccessful();
        }
        finally {
            ActiveAndroid.endTransaction();
        }
        logger("wink saved add..", wink.getObjectId());
    }

    public static void deleteWinkFromDataBase(String ObjectId){
        new Delete().from(Wink.class).where("glimmer_object_id = ?", ObjectId).execute();
    }

    public static void deleteWinksFromDatabase(String userId){
        new Delete().from(Wink.class).where("glimmer_user_id = ?", userId).execute();
    }

    public static void deleteAllWinks(){
        new Delete().from(Wink.class).execute();
        logger("all wink deleted", "completed");
    }

    public static List<Wink> loadAllWinks(){
        List<Wink> winks = new Select().from(Wink.class).execute();
        logger("loaded is ORM all wink...", "count - " + winks.size());
        return winks;
    }

    public static void replaceAllWinks(List<Wink> winks){
        for (Wink wink : winks) addWink(wink);
    }

    public static List<Wink> loadWinksFromDataType(int type) {
        return new Select()
                .from(Wink.class)
                .where("glimmer_type = ?", type)
                .execute();
    }

    public static List<Wink> loadWinksWithoutMatches() {
        return new Select()
                .from(Wink.class)
                .where("glimmer_type != ?", Wink.MATCH_TYPE)
                .execute();
    }

    /*
    Sugar ORM logic
     */

//    public static void saveAllWinks(List<Wink> winks){
//        for(Wink wink : winks){
//            wink.save();
//            logger("wink saved..", wink.getObjectId());
//        }
//        if(App.logger) loadAllWinks();
//    }
//
//    public static void addWink(Wink wink){
//        wink.save();
//        logger("wink saved add..", wink.getObjectId());
//    }
//
//    public static void deleteWinkFromDataBase(String ObjectId){
//        Wink.deleteAll(Wink.class, "object_id = ?", ObjectId);
//        logger("wink delete item", ObjectId);
//    }
//
//    public static void deleteAllWinks(){
//        Wink.deleteAll(Wink.class);
//        logger("all wink deleted", "completed");
//    }
//
//    public static List<Wink> loadAllWinks(){
//        List<Wink> winks = Wink.listAll(Wink.class);
//        logger("loaded is ORM all wink...", "count - " + winks.size());
//        return winks;
//    }
//
//    public static void  replaceAllWinks(List<Wink> winks){
//        for(Wink wink : winks){
//            deleteWinkFromDataBase(wink.getObjectId());
//            addWink(wink);
//        }
////        deleteAllWinks();
////        saveAllWinks(winks);
//    }
//
//    public static List<Wink> loadWinksFromDataType(int type) {
//        return Wink.findWithQuery(Wink.class, "type = ?", String.valueOf(type));
//    }
}
