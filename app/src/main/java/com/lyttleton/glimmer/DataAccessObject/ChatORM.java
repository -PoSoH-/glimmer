package com.lyttleton.glimmer.DataAccessObject;

import android.util.Log;

//import com.activeandroid.ActiveAndroid;
//import com.activeandroid.Model;
//import com.activeandroid.query.Delete;
//import com.activeandroid.query.Select;
import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.lyttleton.glimmer.App;
import com.lyttleton.glimmer.adapters.Chat;
import com.lyttleton.glimmer.adapters.Venue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergiy Polishuk on 08.02.2016.
 */
public class ChatORM {

    public static void logger(String places, String info){
        if(App.logger) {
            StringBuilder builder = new StringBuilder();
            builder.append("..");
            builder.append(places);
            builder.append("..");
            builder.append(info);
            builder.append("..");
            Log.d("glimmer", builder.toString());
        }
    }

    public static void saveAllChats(List<Chat> chats){
        ActiveAndroid.beginTransaction();
        try {
            for (Chat chat : chats) {
                chat.save();
                logger("chat saved..", chat.getObjectId());
            }
            ActiveAndroid.setTransactionSuccessful();
        }finally {
            ActiveAndroid.endTransaction();
        }
        if(App.logger) loadAllChats();
    }

    public static void addChat(Chat chat){
        ActiveAndroid.beginTransaction();
        try {
            chat.save();
            ActiveAndroid.setTransactionSuccessful();
        }finally {
            ActiveAndroid.endTransaction();
        }
        logger("chat saved add..", chat.getObjectId());
    }

    public static void deleteChatFromDataBase(String ObjectId){
        ActiveAndroid.beginTransaction();
        try {
            new Delete()
                    .from(Chat.class)
                    .where("glimmer_object_id = ?", ObjectId)
                    .execute();
            logger("chat delete item", ObjectId);
            ActiveAndroid.setTransactionSuccessful();
        }finally {
            ActiveAndroid.endTransaction();
        }
    }

    public static void deleteAllChat(){
        new Delete().from(Chat.class).execute();
        logger("all chat deleted", "completed");
    }

    public static List<Chat> loadAllChats(){
        List<Chat> chats = new Select().from(Chat.class).execute();
        logger("loaded is ORM all chat...", "count - " + chats.size());
        return chats;
    }

    public static List<Chat> loadChatFromDataBase(String senderId, String receiverId){
        List<Chat> temps = new Select().from(Chat.class).orderBy("glimmer_date ASC").execute();
        ArrayList<Chat> chats = new ArrayList<>();
        for(Chat chat : temps){
            if(senderId.equals(chat.getSenderId()) && receiverId.equals(chat.getReceiverId())){
                chats.add(chat);
            }
            if(receiverId.equals(chat.getSenderId()) && senderId.equals(chat.getReceiverId())){
                chats.add(chat);
            }
        }
        return chats;
    }

    public static void  replaceAllChats(List<Chat> chats){
        ActiveAndroid.beginTransaction();
        try {
            for(Chat chat : chats) chat.save();
            ActiveAndroid.setTransactionSuccessful();
        }finally {
            ActiveAndroid.endTransaction();
        }
    }

    /*
    Sugar ORM logic
     */

//   public static void logger(String places, String info){
//    if(App.logger) {
//        StringBuilder builder = new StringBuilder();
//        builder.append("..");
//        builder.append(places);
//        builder.append("..");
//        builder.append(info);
//        builder.append("..");
//        Log.d("glimmer", builder.toString());
//    }
//}
//
//    public static void saveAllChats(List<Chat> chats){
//        for (Chat chat : chats) {
//            chat.save();
//            logger("chat saved..", chat.getObjectId());
//        }
//    }
//
//    public static void addChat(Chat chat){
//            chat.save();
//    }
//
//    public static void deleteChatFromDataBase(String ObjectId){
//        Chat.deleteAll(Chat.class, "object_id = ?", ObjectId);
//        logger("chat delete item", ObjectId);
//    }
//
//    public static void deleteAllChat(){
//        Chat.deleteAll(Chat.class);
//        logger("all chat deleted", "completed");
//    }
//
//    public static List<Chat> loadAllChats(){
//        List<Chat> chats = Chat.listAll(Chat.class);
//        logger("loaded is ORM all chat...", "count - " + chats.size());
//        return chats;
//    }
//
//    public static List<Chat> loadChatFromDataBase(String senderId, String receiverId){
//        List<Chat> temps = Chat.findWithQuery(Chat.class, "receiver_id = ?", receiverId);
//        ArrayList<Chat> chats = new ArrayList<>();
//        for(Chat chat : temps){
//            if(senderId.equals(chat.getSenderId()) && receiverId.equals(chat.getReceiverId())){
//                chats.add(chat);
//            }
//            if(receiverId.equals(chat.getSenderId()) && senderId.equals(chat.getReceiverId())){
//                chats.add(chat);
//            }
//        }
//        return chats;
//    }
//
//    public static void  replaceAllChats(List<Chat> chats){
//        for(Chat chat : chats){
//            deleteChatFromDataBase(chat.getObjectId());
//            addChat(chat);
//        }
//    }
}
