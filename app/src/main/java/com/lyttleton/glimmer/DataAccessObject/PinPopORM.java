package com.lyttleton.glimmer.DataAccessObject;

import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.lyttleton.glimmer.App;
import com.lyttleton.glimmer.adapters.PinPop;

import java.util.List;

/**
 * Created by Sergiy Polishuk on 08.02.2016.
 */
public class PinPopORM {

    public static void logger(String places, String info){
        if(App.logger) {
            StringBuilder builder = new StringBuilder();
            builder.append("..");
            builder.append(places);
            builder.append("..");
            builder.append(info);
            builder.append("..");
            Log.d("glimmer", builder.toString());
        }
    }

//    public static void saveAllPinPops(List<PinPop> pinPops){
//        ActiveAndroid.beginTransaction();
//        try {
//            for(PinPop pinPop : pinPops){
//                pinPop.save();
//    //            logger("pinPop saved", ".." + PinPop.all(PinPop.class).size());
//            }
//            ActiveAndroid.setTransactionSuccessful();
//        } finally {
//            ActiveAndroid.endTransaction();
//        }
//    }

    public static void addPinPop(PinPop pinPop){
        ActiveAndroid.beginTransaction();
        try {
            pinPop.save();
            logger("pinPop saved add..", pinPop.getObjectId());
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    public static void deletePinPopFromDataBase(String ObjectId){
        new Delete()
                .from(PinPop.class)
                .where("glimmer_object_id = ?", ObjectId)
                .execute();
        logger("pinPop delete item", ObjectId);
    }

    public static void deleteAllPinPops(){
        new Delete().from(PinPop.class).execute();
        logger("all pinPop deleted", "completed");
    }

    public static List<PinPop> loadPinPopFromDataBase(String venueId){
        return new Select()
                .from(PinPop.class)
                .where("glimmer_venue_id = ?", venueId)
                .execute();
    }

    public static List<PinPop> loadAllPinPops(){
        List<PinPop> pinPops = new Select().from(PinPop.class).execute();
        logger("loaded is ORM all pinPop...", "count - " + pinPops.size());
        return pinPops;
    }

    public static void  replaceAllPinPops(List<PinPop> pinPops){
        logger("replaces is ORM all pinPop...", "start");
        for(PinPop pop : pinPops) {
            deletePinPopFromDataBase(pop.getObjectId());
            addPinPop(pop);
        }
    }

    /*
    Sugar ORM logik data base...
     */
//    public static void saveAllPinPops(List<PinPop> pinPops){
//        for(PinPop pinPop : pinPops){
//            pinPop.save();
////            logger("pinPop saved", ".." + PinPop.all(PinPop.class).size());
//        }
//    }
//
//    public static void addPinPop(PinPop pinPop){
//        deletePinPopFromDataBase(pinPop.getObjectId());
//        pinPop.save();
//        logger("pinPop saved add..", pinPop.getObjectId());
//    }
//
//    public static void deletePinPopFromDataBase(String ObjectId){
//       PinPop.deleteAll(PinPop.class, "object_id = ?", ObjectId);
//        logger("pinPop delete item", ObjectId);
//    }
//
//    public static void deleteAllPinPops(){
//        PinPop.deleteAll(PinPop.class);
//                logger("all pinPop deleted", "completed");
//    }
//
//    public static List<PinPop> loadPinPopFromDataBase(String venueId){
//        return PinPop.findWithQuery(PinPop.class, "venue_id = ?", venueId);
//    }
//
//    public static List<PinPop> loadAllPinPops(){
//        List<PinPop> pinPops = PinPop.listAll(PinPop.class);
//        logger("loaded is ORM all pinPop...", "count - " + pinPops.size());
//        return pinPops;
//    }
//
//    public static void  replaceAllPinPops(List<PinPop> pinPops){
//        logger("replaces is ORM all pinPop...", "start");
//        for(PinPop pop : pinPops) {
//            deletePinPopFromDataBase(pop.getObjectId());
//            addPinPop(pop);
//        }
//    }
}
