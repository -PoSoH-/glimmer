package com.lyttleton.glimmer.DataAccessObject;

import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.lyttleton.glimmer.App;
import com.lyttleton.glimmer.adapters.User;

import java.util.List;

/**
 * Created by Sergiy Polishuk on 08.02.2016.
 */
public class UserORM {


    public static void logger(String places, String info){
        if(App.logger) {
            StringBuilder builder = new StringBuilder();
            builder.append("..");
            builder.append(places);
            builder.append("..");
            builder.append(info);
            builder.append("..");
            Log.d("glimmer", builder.toString());
        }
    }

    public static void saveAllUsers(List<User> users){
        for(User user : users){
            user.save();
//            logger("user saved", ".." + User.all(User.class).size());
        }
    }

    public static void addUser(User user){
        ActiveAndroid.beginTransaction();
        try {
            user.save();
            logger("user saved add..", user.getFirstName());
            ActiveAndroid.setTransactionSuccessful();
        }
        finally {
            ActiveAndroid.endTransaction();
        }
    }

    public static void deleteUserFromDataBase(String ObjectId){
        List<User> users = new Select().from(User.class).where("glimmer_object_id = ?" , ObjectId).execute();
        if(users != null) for(User user : users) user.delete();
//        logger("user delete item", ObjectId);
    }

    public static void deleteAllUsers(){
        new Delete()
                .from(User.class)
                .execute();
        logger("all user deleted", "completed");
    }

    public static List<User> loadAllUsers(){
        List<User> users = new Select().from(User.class).execute();
        logger("loaded is ORM all user...", "count - " + users.size());
        return users;
    }

    public static User loadUsersFromDataBase(String userId){
        return new Select()
                .from(User.class)
                .where("glimmer_object_id = ?", userId)
                .executeSingle();
    }

//    public static void  replaceAllUsers(List<User> users){
//        deleteAllUsers();
//        saveAllUsers(users);
//    }

    public static void  replaceUsersWithValidation(List<User> users){
        for(User user : users){
            deleteUserFromDataBase(user.getObjectId());
            addUser(user);
        }
    }

/*
Sugar ORM logic
 */
//    public static void saveAllUsers(List<User> users){
//        for(User user : users){
//            user.save();
//            logger("user saved", ".." + User.count(User.class));
//        }
//    }
//
//    public static void addUser(User user){
//        deleteUserFromDataBase(user.getObjectId());
//        user.save();
//        logger("user saved add..", user.getFirstName());
//        logger("user count", ".." + User.count(User.class));
//    }
//
//    public static void deleteUserFromDataBase(String ObjectId){
//        User.deleteAll(User.class, "object_id = ?", ObjectId);
//        logger("user delete item", ObjectId);
//    }
//
//    public static void deleteAllUsers(){
//        User.deleteAll(User.class);
//        logger("all user deleted", "completed");
//    }
//
//    public static List<User> loadAllUsers(){
//        List<User> users = User.listAll(User.class);
//        logger("loaded is ORM all user...", "count - " + users.size());
//        return users;
//    }
//
//    public static List<User> loadUsersFromDataBase(String userId){
//        return User.find(User.class, "object_id = ?", userId);
//    }
//
//    public static void  replaceAllUsers(List<User> users){
//        deleteAllUsers();
//        saveAllUsers(users);
//    }
}
