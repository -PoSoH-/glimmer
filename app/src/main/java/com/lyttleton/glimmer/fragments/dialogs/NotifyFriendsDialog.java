package com.lyttleton.glimmer.fragments.dialogs;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.activities.BaseActivity;

public class NotifyFriendsDialog extends DialogFragment {

    public static final String VENUE_NAME_ARG = "VENUE_NAME_ARG";

    public static void show(FragmentActivity fragmentActivity, String venueName) {
        if (fragmentActivity.getSupportFragmentManager().
                findFragmentByTag(NotifyFriendsDialog.class.getSimpleName()) == null) {
            NotifyFriendsDialog dialog = new NotifyFriendsDialog();
            Bundle args = new Bundle();
            args.putString(VENUE_NAME_ARG, venueName);
            dialog.setArguments(args);
            FragmentTransaction ft = fragmentActivity.getSupportFragmentManager().beginTransaction();
            ft.add(dialog, NotifyFriendsDialog.class.getSimpleName());
            ft.commitAllowingStateLoss();
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());
        final View view = getActivity().getLayoutInflater().inflate(R.layout.cancel_or_accept_dialog_layout,
                null);

        TextView dialogTitleTv = (TextView) view.findViewById(R.id.dialog_title_tv);
        dialogTitleTv.setText(R.string.only_4_more_people);

        final String venueName = getArguments().getString(VENUE_NAME_ARG);
        TextView dialogMessageTv = (TextView) view.findViewById(R.id.dialog_message_tv);
        dialogMessageTv.setText(venueName + " " + getString(R.string.notify_friends_text_1) + " "
                + venueName + " " + getString(R.string.notify_friends_text_2));
        dialogMessageTv.setVisibility(View.VISIBLE);

        view.findViewById(R.id.cancel_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        Button acceptBt = (Button) view.findViewById(R.id.accept_bt);
        acceptBt.setText(R.string.notify_friends);
        acceptBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseActivity.sendSMS(getActivity(), getString(R.string.sms_notify_venue_1) + " "
                        + venueName + " " + getString(R.string.sms_notify_venue_2));
                getDialog().dismiss();
            }
        });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }
}
