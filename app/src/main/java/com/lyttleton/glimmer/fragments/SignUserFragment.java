package com.lyttleton.glimmer.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.lyttleton.glimmer.App;
import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.activities.BaseActivity;
import com.lyttleton.glimmer.activities.IntroActivity;
import com.lyttleton.glimmer.fragments.dialogs.ErrorMessageDialog;
import com.lyttleton.glimmer.loaders.Backend;
import com.parse.ParseObject;

import java.util.HashMap;

public class    SignUserFragment extends Fragment {

    public final static int ERROR_EMPTY_STRING = -200;
    public final static int ERROR_EMAIL_FALSE = -201;
    public final static int ERROR_GENDER_FALSE = -203;
    public final static int ERROR_INTEREST_FALSE = -204;

    private Button btnSignUp;
    private EditText editName;
//    private EditText editEmail;
    private Button btnGenderMan;
    private Button btnGenderGirl;
    private Button btnInterestMan;
    private Button btnInterestGirl;
    private Boolean _InterestMan = null;
    private Boolean _GenderMan = null;

    private OnListenerDialogProgressViewSignUp listener;

    private HashMap<String, String> _UserInfo = new HashMap<>();

    public static SignUserFragment create(){
        return new SignUserFragment();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(listener!= null) listener.onListenerChangeLabelText(getString(R.string.txt_label_sign_up));

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    checkUserName(new ResponseListener() {
                        @Override
                        public void onSuccess() {
                            checkGender(new ResponseListener() {
                                @Override
                                public void onSuccess() {
                                    checkInterest(new ResponseListener() {
                                        @Override
                                        public void onSuccess() {
                                            listener.OnListenerDialogProgressShow();
//                                            BaseActivity.setFragment(getActivity(), new SignTelephoneFragment(), R.id.fragment_container, false);
                                            _UserInfo.put(Backend.CODE, listener.OnListenerGetDataUniqueCode());
                                            Backend.signUpUser(getActivity(), _UserInfo, new Backend.ResponseListener() {
                                                @Override
                                                public void onSuccess(ParseObject parseObject) {
                                                    BaseActivity.setFragment(getActivity(), new SignTelephoneFragment(), R.id.fragment_container, false);
                                                    listener.OnListenerDialogProgressHide();
                                                }

                                                @Override
                                                public void onError(String error, int errorCode) {
                                                    listener.OnListenerDialogProgressHide();
                                                    ErrorMessageDialog.show(getActivity(), error);
                                                }
                                            });
                                        }

                                        @Override
                                        public void onError(int ErrorCode) {
                                            if (ErrorCode == ERROR_INTEREST_FALSE) {
                                                ErrorMessageDialog.show(getActivity(), getString(R.string.txt_error_interest_selected));
                                            }
                                        }
                                    });
                                }

                                @Override
                                public void onError(int ErrorCode) {
                                    if (ErrorCode == ERROR_GENDER_FALSE) {
                                        ErrorMessageDialog.show(getActivity(), getString(R.string.txt_error_gender_selected));
                                    }
                                }
                            });

                        }

                        @Override
                        public void onError(int ErrorCode) {
                            if (ErrorCode == ERROR_EMPTY_STRING)
                                ErrorMessageDialog.show(getActivity(), getString(R.string.txt_error_name));
//                            if (ErrorCode == ERROR_EMAIL_FALSE)
//                                ErrorMessageDialog.show(getActivity(), getString(R.string.txt_error_email_correctly));
                        }
                    });
                }
            }
        });

        btnGenderMan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeGenderMan();
            }
        });

        btnGenderGirl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeGenderGirl();
            }
        });

        btnInterestMan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeInterestMan();
            }
        });

        btnInterestGirl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeInterestGirl();
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout._fr_input_user_name, null);
        btnSignUp = (Button)view.findViewById(R.id.btn_sign);
        editName = (EditText)view.findViewById(R.id.et_name_user);
//        editEmail = (EditText)view.findViewById(R.id.et_email_user);
        btnGenderMan = (Button)view.findViewById(R.id.btn_user_males);
        btnGenderGirl = (Button)view.findViewById(R.id.btn_user_females);;
        btnInterestMan = (Button)view.findViewById(R.id.btn_interest_males);
        btnInterestGirl = (Button)view.findViewById(R.id.btn_interest_females);
//        imgGenderManIndicator = (ImageView) view.findViewById(R.id.img_user_males_indicator);
//        imgGenderGirlIndicator = (ImageView) view.findViewById(R.id.img_user_females_indicator);
//        imgInterestManIndicator = (ImageView) view.findViewById(R.id.img_interest_males_indicator);
//        imgInterestGirlIndicator = (ImageView) view.findViewById(R.id.img_interest_females_indicator);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof OnListenerDialogProgressViewSignUp)
            listener = (OnListenerDialogProgressViewSignUp) activity;
        else
            throw new ClassCastException("Error!.. Please implements OnListenerDialogProgressViewSignUp interface!");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void checkUserName(ResponseListener listener){
        if(TextUtils.isEmpty(editName.getText())){
            listener.onError(ERROR_EMPTY_STRING);
        }else{
            _UserInfo.put(Backend.TU_NAME_COLUMN, editName.getText().toString());
            listener.onSuccess();
        }
    }

//    private void checkEmailAddres(ResponseListener listener){
//        if(TextUtils.isEmpty(editEmail.getText())){
//            listener.onError(ERROR_EMPTY_STRING);
//        }else{
//            final String EMAIL_VALIDATION_PATTERN = "^[-\\w.]+@([A-z0-9]+\\.)+[A-z]{2,}$";
//            if(!editEmail.getText().toString().trim().matches(EMAIL_VALIDATION_PATTERN)){
//               listener.onError(ERROR_EMAIL_FALSE);
//            }else{
//                _UserInfo.put("MAIL", editEmail.getText().toString());
//                listener.onSuccess();
//            }
//        }
//    }

    private void checkGender(ResponseListener listener){
        if(_GenderMan == null){
            listener.onError(ERROR_GENDER_FALSE);
        }else{
            if(_GenderMan) {
                _UserInfo.put(Backend.TU_GENDER_COLUMN, "male");
            }else{
                _UserInfo.put(Backend.TU_GENDER_COLUMN, "female");
            }
            listener.onSuccess();
        }
    }

    private void checkInterest(ResponseListener listener){
        if(_InterestMan == null){
            listener.onError(ERROR_INTEREST_FALSE);
        }else{
            if(_InterestMan) {
                _UserInfo.put(Backend.TU_INTEREST_GENDER_COLUMN, "Males");
            }else{
                _UserInfo.put(Backend.TU_INTEREST_GENDER_COLUMN, "Females");
            }
            listener.onSuccess();
        }
    }

    private void makeGenderMan(){
        Log.d("glimmer", "PRESS BUTTON GENDER MAN");
        btnGenderMan.setBackgroundColor(getActivity().getResources().getColor(R.color.purple));
        btnGenderMan.setTextColor(getActivity().getResources().getColor(R.color.white));
        btnGenderGirl.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
        btnGenderGirl.setTextColor(getActivity().getResources().getColor(R.color.purple));
        _GenderMan = true;
        App.hideKeyboard(getActivity());
    }

    private void makeGenderGirl(){
        Log.d("glimmer", "PRESS BUTTON GENDER GIRL");
        btnGenderMan.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
        btnGenderMan.setTextColor(getActivity().getResources().getColor(R.color.purple));
        btnGenderGirl.setBackgroundColor(getActivity().getResources().getColor(R.color.purple));
        btnGenderGirl.setTextColor(getActivity().getResources().getColor(R.color.white));
        _GenderMan = false;
        App.hideKeyboard(getActivity());
    }

    private void makeInterestMan(){
        Log.d("glimmer", "PRESS BUTTON INTEREST MAN");
        btnInterestMan.setBackgroundColor(getActivity().getResources().getColor(R.color.purple));
        btnInterestMan.setTextColor(getActivity().getResources().getColor(R.color.white));
        btnInterestGirl.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
        btnInterestGirl.setTextColor(getActivity().getResources().getColor(R.color.purple));
        _InterestMan = true;
        App.hideKeyboard(getActivity());
    }

    private void makeInterestGirl(){
        Log.d("glimmer", "PRESS BUTTON INTEREST GIRL");
        btnInterestMan.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
        btnInterestMan.setTextColor(getActivity().getResources().getColor(R.color.purple));
        btnInterestGirl.setBackgroundColor(getActivity().getResources().getColor(R.color.purple));
        btnInterestGirl.setTextColor(getActivity().getResources().getColor(R.color.white    ));
        _InterestMan = false;
        App.hideKeyboard(getActivity());
    }

    interface ResponseListener{
        void onSuccess();
        void onError(int ErrorCode);
    }

    public interface OnListenerDialogProgressViewSignUp{
        void OnListenerDialogProgressShow();
        void OnListenerDialogProgressHide();
        String OnListenerGetDataUniqueCode();
        void onListenerChangeLabelText(String textLabel);
    }
}
