package com.lyttleton.glimmer.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.lyttleton.glimmer.DataAccessObject.UserORM;
import com.lyttleton.glimmer.DataAccessObject.VenueORM;
import com.lyttleton.glimmer.DataAccessObject.WinkORM;
import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.activities.ChatActivity;
import com.lyttleton.glimmer.activities.MainActivity;
import com.lyttleton.glimmer.activities.MyWinksActivity;
import com.lyttleton.glimmer.activities.VenueActivity;
import com.lyttleton.glimmer.activities.WinkActivity;
import com.lyttleton.glimmer.adapters.FeedListAdapter;
import com.lyttleton.glimmer.adapters.User;
import com.lyttleton.glimmer.adapters.Venue;
import com.lyttleton.glimmer.adapters.Wink;
import com.lyttleton.glimmer.helpers.InternetHelper;
import com.lyttleton.glimmer.helpers.SharedPrefsHelper;
import com.lyttleton.glimmer.loaders.Backend;
import com.lyttleton.glimmer.loaders.WinksLoader;
import com.lyttleton.glimmer.services.PushReceiverService;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FeedListFragment extends Fragment
        implements LoaderManager.LoaderCallbacks<List<Wink>>, AdapterView.OnItemClickListener {

    public static final int ALL_TYPE = 0;
    public static final int WINKS_TYPE = 1;
    public static final int MATCHES_TYPE = 2;
    public static final String TYPE_ARG = "TYPE_ARG";
    public static final String RELOAD_DATA_BROADCAST = "RELOAD_DATA_BROADCAST";

    private ListView listView;
    private int type;
    private FeedListAdapter adapter;
//    private View progressBar;

    private View noWinksLayout, noInternetLayout;

    // Paging
    private List<Wink> previousItems;
    private boolean hasNextPage, loadingStarted;
    private View loadingFooter;
    private int loadingStartOffset;

    public static FeedListFragment create(int type){
        FeedListFragment fragment = new FeedListFragment();
        Bundle args = new Bundle();
        args.putInt(TYPE_ARG, type);
        fragment.setArguments(args);
        return fragment;
    }

    private BroadcastReceiver pushBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String objectId = intent.getStringExtra(Backend.OBJECT_ID_COLUMN);
            final Venue venue = (Venue) intent.getSerializableExtra(PushReceiverService.VENUE_ARG);
            final User user = (User) intent.getSerializableExtra(PushReceiverService.USER_ARG);
            final int type = intent.getIntExtra(Backend.TYPE_COLUMN, 0);

            if (type == Wink.CHAT_TYPE) adapter.update();
            else if (!TextUtils.isEmpty(objectId) && venue != null && user != null) {
                Backend.loadWink(objectId, new Backend.ResponseListener() {
                    @Override
                    public void onSuccess(ParseObject parseObject) {
                        Wink wink = Backend.makeWink(parseObject, venue, user);
                        wink.setType(type);
                        if (type == Wink.MATCH_TYPE) {
                            if (FeedListFragment.this.type == WINKS_TYPE) {
                                for (Wink winkFromAdapter : adapter.getData())
                                    if (winkFromAdapter.getUser().getObjectId().equals(user.getObjectId()))
                                        winkFromAdapter.setType(Wink.WINK_MATCH_TYPE);

                                for (Wink winkFromPreviousItems : previousItems)
                                    if (winkFromPreviousItems.getUser().getObjectId().equals(user.getObjectId()))
                                        winkFromPreviousItems.setType(Wink.WINK_MATCH_TYPE);
                                adapter.notifyDataSetChanged();
                            } else removeWinksFromUser(user);
                        }
                        if ((FeedListFragment.this.type == WINKS_TYPE && type != Wink.MATCH_TYPE)
                                || (FeedListFragment.this.type == MATCHES_TYPE
                                && type == Wink.MATCH_TYPE)
                                || FeedListFragment.this.type == ALL_TYPE) {
                            if (previousItems != null && !hasWink(wink, previousItems)) {
                                adapter.insert(wink, 0);
                                previousItems.add(0, wink);
                            }
                            updateViews(false);
                        }
                    }

                    @Override
                    public void onError(String error, int errorCode) {}
                });
            }
        }
    };

    private boolean hasWink(Wink wink, List<Wink> winks) {
        for (Wink winkEl : winks) if (winkEl.getObjectId().equals(wink.getObjectId())) return true;
        return false;
    }

    public void removeWinksFromUser(User sender){
        List<Wink> data = adapter.getData(), newData = new ArrayList<>();
        if (data != null) {
            for (Wink wink : data)
                if (wink.getUser() != null && !wink.getUser().getObjectId().equals(sender.getObjectId()))
                    newData.add(wink);
            adapter.setData(newData);
        }

        if (previousItems != null){
            Iterator<Wink> iterator = previousItems.iterator();
            User user;
            while (iterator.hasNext()) {
                user = iterator.next().getUser();
                if (user != null && user.getObjectId().equals(sender.getObjectId()))
                    iterator.remove();
            }
        }
    }

    public static void sendMatchBroadcast(String winkObjectId, Venue venue, User user, int type,
                                          Context context){
        Intent intent = new Intent(Backend.PUSH_BROADCAST_ACTION);
        intent.putExtra(Backend.OBJECT_ID_COLUMN, winkObjectId);
        intent.putExtra(PushReceiverService.VENUE_ARG, venue);
        intent.putExtra(PushReceiverService.USER_ARG, user);
        intent.putExtra(Backend.TYPE_COLUMN, type);
        context.sendBroadcast(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().registerReceiver(pushBroadcastReceiver, new IntentFilter(Backend.PUSH_BROADCAST_ACTION));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(pushBroadcastReceiver);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (type == WINKS_TYPE) previousItems = WinkORM.loadWinksWithoutMatches();
        else previousItems = WinkORM.loadWinksFromDataType(type);
        for (int pos = 0, len = previousItems.size(); pos < len; pos++) {
            previousItems.get(pos).addUser(UserORM.loadUsersFromDataBase(previousItems.get(pos).getUserId()));
            previousItems.get(pos).addVenue(VenueORM.loadVenueFromDataBase(previousItems.get(pos).getVenueId()));
        }

        adapter = new FeedListAdapter(getActivity(), this);

        listView.addFooterView(loadingFooter);
        listView.setAdapter(adapter);
        if (previousItems != null) adapter.setData(previousItems);
        listView.removeFooterView(loadingFooter);
        listView.setOnItemClickListener(this);

        getLoaderManager().initLoader(0, null, this);
        updateViews(InternetHelper.checkInternetConnection(getContext()));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        type = getArguments().getInt(TYPE_ARG);

        View view = inflater.inflate(R.layout.feed_list_layout, null);
        listView = (ListView) view.findViewById(R.id.feed_lv);
//        progressBar = view.findViewById(R.id.progress_bar);
        loadingFooter = inflater.inflate(R.layout.loading_footer, null);
        noWinksLayout = view.findViewById(R.id.no_winks_layout);

        noInternetLayout = view.findViewById(R.id.no_internet_layout);
        noInternetLayout.findViewById(R.id.go_to_settings_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InternetHelper.openWifiSettings(getActivity());
            }
        });

        TextView noWinksTv = (TextView) view.findViewById(R.id.no_winks_tv);
        switch (type){
            case ALL_TYPE:
                noWinksTv.setText(R.string.no_winks_and_matches);
                break;
            case WINKS_TYPE:
                noWinksTv.setText(R.string.no_winks);
                break;
            case MATCHES_TYPE:
                noWinksTv.setText(R.string.no_matches);
                break;
        }
        View startMingleBt = view.findViewById(R.id.start_mingle_bt);
        startMingleBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity activity = getActivity();
                if (activity instanceof MainActivity)
                    ((MainActivity) getActivity()).openVenues();
                else if (activity instanceof MyWinksActivity) {
                    getActivity().onBackPressed();
                    MainActivity.openVenuesWithBroadcast(getActivity());
                }
            }
        });

        return view;
    }

    @Override
    public Loader<List<Wink>> onCreateLoader(int id, Bundle args) {
        loadingStarted = true;
        noWinksLayout.setVisibility(View.GONE);
//<<<<<<< Updated upstream
//        if (previousItems == null || previousItems.size() == 0) {
//            listView.setVisibility(View.GONE);
//            progressBar.setVisibility(View.VISIBLE);
//        } else listView.addFooterView(loadingFooter);
//=======
        if (previousItems == null || previousItems.size() == 0) {
            listView.setVisibility(View.GONE);
//            progressBar.setVisibility(View.VISIBLE);
        } else listView.addFooterView(loadingFooter);
//>>>>>>> Stashed changes
        return new WinksLoader(getActivity(), type, previousItems, loadingStartOffset);
    }

    @Override
    public void onLoadFinished(Loader<List<Wink>> loader, List<Wink> data) {
        loadingStarted = false;
        listView.removeFooterView(loadingFooter);
        WinksLoader winksLoader = ((WinksLoader)loader);
        if (!winksLoader.isNoInternet()) {
            if (data != null) adapter.setData(data);
            else adapter.clear();

            hasNextPage = winksLoader.hasNextPage();
            previousItems = data;
            loadingStartOffset = winksLoader.getLoadingStartOffset();

            // If list is empty and has next elements load them
            if (adapter.getCount() == 0 && (data == null || data.size() == 0 && hasNextPage))
                getLoaderManager().restartLoader(0, null, this);
//        else progressBar.setVisibility(View.GONE);
        }
        updateViews(winksLoader.isNoInternet());

        hasNextPage = winksLoader.hasNextPage();
        previousItems = data;
        loadingStartOffset = winksLoader.getLoadingStartOffset();

        // If list is empty and has next elements load them
        if (adapter.getCount() == 0 && (data == null || data.size() == 0 && hasNextPage))
            getLoaderManager().restartLoader(0, null, this);
//        else progressBar.setVisibility(View.GONE);
    }

    private void updateViews(boolean noInternet){
        noWinksLayout.setVisibility(View.GONE);
        noInternetLayout.setVisibility(View.GONE);
        listView.setVisibility(View.GONE);
        if (adapter.getCount() == 0) {
//            if (!noInternet) noWinksLayout.setVisibility(View.VISIBLE);
//            else noInternetLayout.setVisibility(View.VISIBLE);
            noWinksLayout.setVisibility(View.VISIBLE);
        } else listView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLoaderReset(Loader<List<Wink>> loader) {}

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (position >= adapter.getCount()) return;

        Wink wink = adapter.getItem(position);
        switch (wink.getType()){
            case Wink.CLUE_TYPE:
                VenueActivity.start(getActivity(), wink.getVenue());
                break;
            case Wink.PHOTO_TYPE:
                WinkActivity.start(getActivity(), wink.getUser(), wink.getVenue());
                break;
            case Wink.WINK_MATCH_TYPE:
            case Wink.MATCH_TYPE:
                ChatActivity.start(getActivity(), wink.getUser());
                SharedPrefsHelper.removeUserIdFromNewChatMessages(wink.getUser().getObjectId(),
                        getActivity());
                adapter.update();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.update();
        if (previousItems != null) previousItems.clear();
        getLoaderManager().restartLoader(0, null, this);
    }

    public void onGetLastElements() {
        if (hasNextPage && !loadingStarted) {
            getLoaderManager().restartLoader(0, null, this);
            listView.addFooterView(loadingFooter);
        }
    }
}
