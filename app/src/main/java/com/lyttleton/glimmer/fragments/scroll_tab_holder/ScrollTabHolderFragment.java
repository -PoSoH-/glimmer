package com.lyttleton.glimmer.fragments.scroll_tab_holder;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.AbsListView;

public abstract class ScrollTabHolderFragment extends Fragment implements ScrollChangeListener {

    // Wrong list view item getTop()
    public static final int HEIGHT_CALCULATION_INACCURACY = 0;

    public static int getScrollY(AbsListView view, int headerHeightArg) {
        View c = view.getChildAt(0);
        if (c == null) return 0;

        int firstVisiblePosition = view.getFirstVisiblePosition();
        int top = c.getTop() + HEIGHT_CALCULATION_INACCURACY;

        int headerHeight = 0;
        if (firstVisiblePosition >= 1) headerHeight = headerHeightArg;

        return -top + firstVisiblePosition * c.getHeight() + headerHeight;
    }
}
