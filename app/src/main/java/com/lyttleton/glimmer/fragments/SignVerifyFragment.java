package com.lyttleton.glimmer.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.lyttleton.glimmer.App;
import com.lyttleton.glimmer.DataAccessObject.VenueORM;
import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.activities.BaseActivity;
import com.lyttleton.glimmer.activities.IntroActivity;
import com.lyttleton.glimmer.fragments.dialogs.ErrorMessageDialog;
import com.lyttleton.glimmer.loaders.Backend;

public class SignVerifyFragment extends Fragment {

    private Button btnSignVerify;
    private EditText editTextVerify;
    private TextView txtResendCode;
    private OnListenerVerifyFragment listener;

    public static SignVerifyFragment create() {
        return new SignVerifyFragment();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(listener != null) listener.onListenerChangeLabelText(getString(R.string.txt_label_verify_mobile));

        btnSignVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VenueORM.logger("verify fragment", "verify code");
                App.hideKeyboard(getActivity());
                if(!emptyVerificationField()) return;
                if (listener != null) {
                    if (editTextVerify.getText().toString().equals(listener.onListenerVerificationCode())) {
                        Backend.verifyNumberPhone();
                        BaseActivity.setFragment(getActivity(), new SignUserPhotoFragment(), R.id.fragment_container, false);
                    }else{
                        ErrorMessageDialog.show(getActivity(), getString(R.string.txt_error_verification_no_matches));
                    }
                }
            }
        });

        txtResendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(App.logger) Log.d("glimmer", "Button Resend is pressed...");
                resendCode();
            }
        });
    }

    private boolean emptyVerificationField(){
        if(editTextVerify.getText() == null) {
            ErrorMessageDialog.show(getActivity(), getString(R.string.txt_error_verification));
            return false;
        } else if (editTextVerify.getText().toString().equals("")) {
            ErrorMessageDialog.show(getActivity(), getString(R.string.txt_error_verification));
            return false;
        } else if(editTextVerify.getText().toString().length() > 4) {
            ErrorMessageDialog.show(getActivity(), getString(R.string.txt_error_long_verification));
            return false;
        } else if(editTextVerify.getText().toString().length() < 4) {
            ErrorMessageDialog.show(getActivity(), getString(R.string.txt_error_short_verification));
            return false;
        } else return true;
    }

    public void resendCode(){
        VenueORM.logger("verify fragment", "resend verify code");
        App.hideKeyboard(getActivity());
        if (listener != null) {
            listener.onListenerCreateVerificationCode();
            listener.OnListenerDialogProgressShow();
            Backend.sendVerificationTelephoneNumber(getActivity()
                    , listener.onListenerNumberPhone()
                    , listener.onListenerVerificationCode()
                    , new SignTelephoneFragment.ResponseListener() {
                        @Override
                        public void success() {
                            listener.OnListenerDialogProgressHide();
                        }

                        @Override
                        public void error(String errorMessage) {
                            listener.OnListenerDialogProgressHide();
                            ErrorMessageDialog.show(getActivity(), errorMessage);
                        }
                    });
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout._fr_input_verification, null);
        btnSignVerify = (Button) view.findViewById(R.id.btn_sign_verify);
        editTextVerify = (EditText) view.findViewById(R.id.edit_verify_text);
        txtResendCode = (TextView) view.findViewById(R.id.txt_fragment_verify_number_code);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnListenerVerifyFragment)
            listener = (OnListenerVerifyFragment) activity;
        else
            throw new ClassCastException("Error! Please implements interface OnListenerVerifyFragment");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public interface OnListenerVerifyFragment {
        void onListenerCreateVerificationCode();
        String onListenerVerificationCode();
        String onListenerNumberPhone();
        void onListenerVerifyCode();
        void OnListenerDialogProgressShow();
        void OnListenerDialogProgressHide();
        void onListenerChangeLabelText(String textLabel);
    }

}
