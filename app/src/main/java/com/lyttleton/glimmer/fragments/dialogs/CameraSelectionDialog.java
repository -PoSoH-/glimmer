package com.lyttleton.glimmer.fragments.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.helpers.MediaSelector;

import java.io.File;

public class CameraSelectionDialog extends DialogFragment {

    private MediaSelectorListener mediaSelectorListener;
    public static final String DATA_NUMBER = "DATA_NUMBER";

    private int photoUpdate = -3;

    public interface MediaSelectorListener{
        void setMediaUri(Uri uri);
        void setNumberUpdatedPhoto(int photoUpdate);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof MediaSelectorListener)
            mediaSelectorListener = (MediaSelectorListener) activity;
        else
            throw new ClassCastException("Error! Please implement MediaSelectorListener interface!");
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mediaSelectorListener = null;
    }

    public static CameraSelectionDialog show(FragmentActivity fragmentActivity) {
        CameraSelectionDialog dialog = new CameraSelectionDialog();
        FragmentTransaction ft = fragmentActivity.getSupportFragmentManager().beginTransaction();
        ft.add(dialog, null);
        ft.commitAllowingStateLoss();
        return dialog;
    }

    public static CameraSelectionDialog show(FragmentActivity fragmentActivity, int photoForUpdated) {
        CameraSelectionDialog dialog = new CameraSelectionDialog();
        Log.d("glimmer", "start position " + photoForUpdated);
        Bundle arguments = new Bundle();
        arguments.putInt(DATA_NUMBER, photoForUpdated);
        dialog.setArguments(arguments);
        FragmentTransaction ft = fragmentActivity.getSupportFragmentManager().beginTransaction();
        ft.add(dialog, null);
        ft.commitAllowingStateLoss();
        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());
        final View view = getActivity().getLayoutInflater().inflate(R.layout.camera_selection_dialog, null);

        Bundle arguments = getArguments();
        if(arguments != null)
            photoUpdate = (int)getArguments().getInt(DATA_NUMBER, -2);

        Log.d("glimmer", "start position " + photoUpdate);

        view.findViewById(R.id.btn_select_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(photoUpdate >= 0) {
                    if (mediaSelectorListener != null)
                        mediaSelectorListener.setNumberUpdatedPhoto(photoUpdate);
                }
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                getActivity().startActivityForResult(Intent.createChooser(intent, "Select photo"),
                        MediaSelector.IMAGE_LOAD_REQUEST_CODE);
                getDialog().dismiss();
            }
        });
        view.findViewById(R.id.btn_make_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(photoUpdate >= 0) {
                    if (mediaSelectorListener != null)
                        mediaSelectorListener.setNumberUpdatedPhoto(photoUpdate);
                }
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                    // create a file to save the photo
                    File photoFile = MediaSelector.getOutputMediaFile(getActivity(), MediaSelector.
                            MEDIA_TYPE_IMAGE);
                    Uri uri = Uri.fromFile(photoFile);
                    mediaSelectorListener.setMediaUri(uri);
                    // set the photo file name
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    // start the photo capture Intent
                    getActivity().startActivityForResult(intent, MediaSelector.IMAGE_CAPTURE_REQUEST_CODE);
                }
                getDialog().dismiss();
            }
        });
        view.findViewById(R.id.btn_select_video).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("video/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                getActivity().startActivityForResult(Intent.createChooser(intent, "Select video"),
                        MediaSelector.VIDEO_CAPTURE_REQUEST_CODE);
                getDialog().dismiss();
            }
        });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        return dialog;
    }
}
