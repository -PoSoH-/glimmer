package com.lyttleton.glimmer.fragments.dialogs;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.activities.ChatActivity;
import com.lyttleton.glimmer.adapters.User;

public class AlreadyMatchedDialog extends DialogFragment {

    public static final String USER_ARG = "USER_ARG";

    public static void show(FragmentActivity fragmentActivity, User user) {
        if (fragmentActivity.getSupportFragmentManager().
                findFragmentByTag(AlreadyMatchedDialog.class.getSimpleName()) == null) {
            AlreadyMatchedDialog dialog = new AlreadyMatchedDialog();
            Bundle args = new Bundle();
            args.putSerializable(USER_ARG, user);
            dialog.setArguments(args);
            FragmentTransaction ft = fragmentActivity.getSupportFragmentManager().beginTransaction();
            ft.add(dialog, AlreadyMatchedDialog.class.getSimpleName());
            ft.commitAllowingStateLoss();
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());
        final View view = getActivity().getLayoutInflater().inflate(R.layout.
                        cancel_or_accept_dialog_layout, null);

        final User user = (User) getArguments().getSerializable(USER_ARG);

        final String title = getString(R.string.ooops),
                message = getString(R.string.already_matched) + " " + user.getFirstName();

        TextView dialogTitleTv = (TextView) view.findViewById(R.id.dialog_title_tv);
        dialogTitleTv.setText(title);

        TextView dialogMessageTv = (TextView) view.findViewById(R.id.dialog_message_tv);
        dialogMessageTv.setText(message);
        dialogMessageTv.setVisibility(View.VISIBLE);

        view.findViewById(R.id.cancel_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        Button acceptBt = (Button) view.findViewById(R.id.accept_bt);
        acceptBt.setText(R.string.chat);
        acceptBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
                ChatActivity.start(getActivity(), user);
            }
        });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }
}
