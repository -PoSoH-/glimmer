package com.lyttleton.glimmer.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.activities.BaseActivity;
import com.lyttleton.glimmer.activities.MainActivity;
import com.lyttleton.glimmer.activities.MyProfilePhotosActivity;
import com.lyttleton.glimmer.activities.MyWinksActivity;
import com.lyttleton.glimmer.activities.PinPopListActivity;
import com.lyttleton.glimmer.adapters.MyProfileListAdapter;
import com.lyttleton.glimmer.fragments.dialogs.RequestVenueDialog;
import com.lyttleton.glimmer.helpers.InternetHelper;
import com.lyttleton.glimmer.loaders.Backend;

public class MyProfileFragment extends Fragment {

    public static final int WINKS_RECEIVED = 0;
    public static final int MY_MATCHES = 1;
    public static final int MY_POPS = 2;
    public static final int PROFILE_PHOTOS = 3;
    public static final int INVITE_A_FRIEND = 4;
    public static final int SEND_FEEDBACK = 5;
    public static final int REQUEST_A_NEW_VENUE = 6;
    public static final int LOGOUT = 7;

    private ListView myProfileLv;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        myProfileLv.setAdapter(new MyProfileListAdapter(getActivity().getApplicationContext(),
                getResources().getStringArray(R.array.my_profile_titles)));

        myProfileLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case WINKS_RECEIVED:
                        MyWinksActivity.start(getActivity(), FeedListFragment.WINKS_TYPE);
                        break;
                    case MY_MATCHES:
                        MyWinksActivity.start(getActivity(), FeedListFragment.MATCHES_TYPE);
                        break;
                    case MY_POPS:
                        PinPopListActivity.start(getActivity(), PinPopListFragment.POP_FRAGMENT_TYPE);
                        break;
                    case INVITE_A_FRIEND:
                        BaseActivity.sendSMS(getActivity(), getString(R.string.message_invitation));
                        break;
                    case SEND_FEEDBACK:
                        InternetHelper.sendEmail(getString(R.string.glimmer_feedback), null, getActivity());
                        break;
                    case REQUEST_A_NEW_VENUE:
                        ((MainActivity)getActivity()).openVenueSearch();
                        break;
                    case PROFILE_PHOTOS:
                        MyProfilePhotosActivity.start(getActivity());
                        break;
                    case LOGOUT:
                        Backend.logout();
                        getActivity().finish();
                        break;
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_profile_layout, null);
        myProfileLv = (ListView) view.findViewById(R.id.my_profile_lv);
        return view;
    }
}
