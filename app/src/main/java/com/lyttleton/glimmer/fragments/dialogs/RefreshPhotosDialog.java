package com.lyttleton.glimmer.fragments.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.activities.BaseActivity;

public class RefreshPhotosDialog extends DialogFragment {

    private RefreshPhotosDialogListener listener;

    public interface RefreshPhotosDialogListener{
        void onRefreshClicked();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (RefreshPhotosDialogListener) activity;
    }

    public static void show(FragmentActivity fragmentActivity) {
        if (fragmentActivity.getSupportFragmentManager().
                findFragmentByTag(RefreshPhotosDialog.class.getSimpleName()) == null) {
            RefreshPhotosDialog dialog = new RefreshPhotosDialog();
//            Bundle args = new Bundle();
//            args.putString(VENUE_NAME_ARG, venueName);
//            dialog.setArguments(args);
            FragmentTransaction ft = fragmentActivity.getSupportFragmentManager().beginTransaction();
            ft.add(dialog, RefreshPhotosDialog.class.getSimpleName());
            ft.commitAllowingStateLoss();
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());
        final View view = getActivity().getLayoutInflater().inflate(R.layout.cancel_or_accept_dialog_layout,
                null);

        TextView dialogTitleTv = (TextView) view.findViewById(R.id.dialog_title_tv);
        dialogTitleTv.setText(R.string.refresh_photos);

        TextView dialogMessageTv = (TextView) view.findViewById(R.id.dialog_message_tv);
        dialogMessageTv.setText(R.string.refresh_photos_info);
        dialogMessageTv.setVisibility(View.VISIBLE);

        view.findViewById(R.id.cancel_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        Button acceptBt = (Button) view.findViewById(R.id.accept_bt);
        acceptBt.setText(R.string.refresh);
        acceptBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (listener != null) listener.onRefreshClicked();
            }
        });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }
}
