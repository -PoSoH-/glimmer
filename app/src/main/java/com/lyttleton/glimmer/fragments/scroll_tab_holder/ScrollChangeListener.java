package com.lyttleton.glimmer.fragments.scroll_tab_holder;

import android.widget.AbsListView;

public interface ScrollChangeListener {
    void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount,
                  int pagePosition);
}