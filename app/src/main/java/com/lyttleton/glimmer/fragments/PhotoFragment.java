package com.lyttleton.glimmer.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.lyttleton.glimmer.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class PhotoFragment extends Fragment {

    public static final String IMAGE_URL_ARG = "IMAGE_URL_ARG";
    public static final String CREATED_FOR_PROFILER = "CREATED_FOR_PROFILER";
    private View progressBar;
    private String photoUrl;
    private int imageWidth;
    private ImageView photoIv;
    private boolean createdForProfiler;
    private View uploadDailyProfilerTv;

    public static Fragment create(String imageUrl){
        return create(imageUrl, false);
    }

    public static Fragment create(String imageUrl, boolean createdForProfiler){
        Fragment fragment = new PhotoFragment();
        Bundle args = new Bundle();
        args.putString(IMAGE_URL_ARG, imageUrl);
        args.putBoolean(CREATED_FOR_PROFILER, createdForProfiler);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createdForProfiler = getArguments().getBoolean(CREATED_FOR_PROFILER);
        if (savedInstanceState != null && savedInstanceState.getString(IMAGE_URL_ARG) != null)
            photoUrl = savedInstanceState.getString(IMAGE_URL_ARG);
        else photoUrl = getArguments().getString(IMAGE_URL_ARG);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.photo_layout, null);
        progressBar = view.findViewById(R.id.progress_bar);
        photoIv = (ImageView) view.findViewById(R.id.photo_iv);
        imageWidth = getActivity().getResources().getDisplayMetrics().widthPixels;
        uploadDailyProfilerTv = view.findViewById(R.id.upload_daily_profiler_tv);

        updatePhoto(photoUrl);
        return view;
    }

    public void updatePhoto(String photoUrl) {
        this.photoUrl = photoUrl;
        uploadDailyProfilerTv.setVisibility(View.GONE);
        if (this.photoUrl != null) {
            progressBar.setVisibility(View.VISIBLE);
            Picasso.with(getActivity()).load(photoUrl).resize(imageWidth, 0).into(photoIv, new Callback() {
                @Override
                public void onSuccess() {
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    progressBar.setVisibility(View.GONE);
                }
            });
        } else if (createdForProfiler) uploadDailyProfilerTv.setVisibility(View.VISIBLE);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(IMAGE_URL_ARG, photoUrl);
        super.onSaveInstanceState(outState);
    }
}
