package com.lyttleton.glimmer.fragments;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.lyttleton.glimmer.DataAccessObject.VenueORM;
import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.activities.VenueActivity;
import com.lyttleton.glimmer.adapters.DiscoverViewPagerAdapter;
import com.lyttleton.glimmer.adapters.Venue;
import com.lyttleton.glimmer.helpers.InternetHelper;
import com.lyttleton.glimmer.helpers.LocationHelper;
import com.lyttleton.glimmer.loaders.Backend;
import com.lyttleton.glimmer.loaders.VenueLoader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DiscoverFragment extends Fragment implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerClickListener, LocationListener, LoaderManager.LoaderCallbacks<List<Venue>>,
        ListPaging {

    public static final int MAP_ZOOM_METERS = 10000;
    public static final int POPULAR_VENUES_FRAGMENT_INDEX = 0;
    public static final int SEARCH_VENUES_FRAGMENT_INDEX = 1;
    public static final int LOADER_ID = 0;
    private ViewPager viewPager;
    private DiscoverViewPagerAdapter adapter;
    private View popularVenuesTab, venueSearchTab;

    private GoogleMap googleMap;
    private HashMap<Marker, Venue> venueHashMap = new HashMap<>();

    // Paging
    private List<Venue> previousItems;
    private boolean hasNextPage, loadingStarted;

    // Used for location
    public static final int LOCATION_UPDATES_INTERVAL = 60000;
    public static final int LOCATION_UPDATES_FASTEST_INTERVAL = 60000;

    private GoogleApiClient googleApiClient;
    private Location lastLocation;
    private boolean cameraMovedToLastLocation, requestingLocationUpdates;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter = new DiscoverViewPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(adapter);
        initializeClickListeners();
        ((SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        previousItems = VenueORM.loadAllVenues();
        buildGoogleApiClient();
        googleApiClient.connect();
    }

    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void initializeClickListeners() {
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                updateTabBackgrounds();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        popularVenuesTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(POPULAR_VENUES_FRAGMENT_INDEX);
            }
        });

        venueSearchTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(SEARCH_VENUES_FRAGMENT_INDEX);
            }
        });
    }

    private void updateTabBackgrounds(){
        int darkGreyColor = getResources().getColor(R.color.dark_grey),
                lightGreyColor = getResources().getColor(R.color.light_grey);
        if (viewPager.getCurrentItem() == POPULAR_VENUES_FRAGMENT_INDEX) {
            popularVenuesTab.setBackgroundColor(darkGreyColor);
            venueSearchTab.setBackgroundColor(lightGreyColor);
        } else {
            popularVenuesTab.setBackgroundColor(lightGreyColor);
            venueSearchTab.setBackgroundColor(darkGreyColor);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.discover_layout, null);
        popularVenuesTab = view.findViewById(R.id.popular_venues_tv);
        venueSearchTab = view.findViewById(R.id.venue_search_tv);
        viewPager = (ViewPager) view.findViewById(R.id.discover_view_pager);
        return view;
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        this.googleMap = googleMap;
        moveCameraToLastLocation();
        googleMap.setOnMarkerClickListener(this);
        getLoaderManager().initLoader(LOADER_ID, null, this);
    }

    private void reloadVenuesOnFragmentShow(){
        if (googleMap != null && InternetHelper.checkInternetConnection(getActivity().getApplicationContext())) {
            hasNextPage = false;
            previousItems = null;
            getLoaderManager().restartLoader(LOADER_ID, null, this);
        }
    }

    private PopularVenuesFragment getPopularVenuesFragment(){
        return (PopularVenuesFragment)adapter.getRegisteredFragment(POPULAR_VENUES_FRAGMENT_INDEX);
    }

    private VenueSearchFragment getVenueSearchFragment(){
        return (VenueSearchFragment) adapter.getRegisteredFragment(SEARCH_VENUES_FRAGMENT_INDEX);
    }

    @Override
    public void onConnected(Bundle bundle) {
        lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        moveCameraToLastLocation();
        startLocationUpdates();
        saveLastLocation();
    }

    private void moveCameraToLastLocation(){
        if (googleMap != null && lastLocation != null)
            setUpMapWithBounds(googleMap, lastLocation, MAP_ZOOM_METERS, 0);
    }

    public static void setUpMapWithBounds(GoogleMap googleMap, Location centerOfMap,
                                          int radiusOfAreaInMeters, int paddingInPx) {
        CameraUpdate cameraUpdate;
        if (radiusOfAreaInMeters != -1)
            cameraUpdate = CameraUpdateFactory.newLatLngBounds(LocationHelper.
                    getMapBounds(LocationHelper.getCoordinatesOfBounds(centerOfMap,
                            radiusOfAreaInMeters)), paddingInPx);
        else
            cameraUpdate = CameraUpdateFactory.newLatLng(new LatLng(centerOfMap.getLatitude(),
                    centerOfMap.getLongitude()));
        googleMap.moveCamera(cameraUpdate);
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {}

    private void startLocationUpdates() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(LOCATION_UPDATES_INTERVAL);
        mLocationRequest.setFastestInterval(LOCATION_UPDATES_FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, mLocationRequest,
                this);
        requestingLocationUpdates = true;
    }

    private void stopLocationUpdates() {
        if (googleApiClient != null && googleApiClient.isConnected())
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        requestingLocationUpdates = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (googleApiClient != null && googleApiClient.isConnected() && !requestingLocationUpdates)
            startLocationUpdates();
        reloadVenuesOnFragmentShow();
    }

    @Override
    public void onLocationChanged(Location location) {
        lastLocation = location;
        if (!cameraMovedToLastLocation) {
            moveCameraToLastLocation();
            saveLastLocation();
        }
        cameraMovedToLastLocation = true;
        PopularVenuesFragment popularVenuesFragment = getPopularVenuesFragment();
        if (popularVenuesFragment != null) popularVenuesFragment.updateDistanceToVenues(lastLocation);
        VenueSearchFragment venueSearchFragment = getVenueSearchFragment();
        if (venueSearchFragment != null) venueSearchFragment.updateDistanceToVenues(lastLocation);
        stopLocationUpdates();
    }

    private void saveLastLocation(){
        if (lastLocation != null) Backend.saveUserLocation(lastLocation);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopLocationUpdates();
        if (googleApiClient != null && googleApiClient.isConnected()) googleApiClient.disconnect();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        VenueActivity.start(getActivity(), venueHashMap.get(marker));
        return true;
    }

    public Location getLastUserLocation(){
            return lastLocation;
    }

    public void openVenues() {
        viewPager.setCurrentItem(POPULAR_VENUES_FRAGMENT_INDEX);
    }

    public void openVenueSearch() {
        viewPager.setCurrentItem(SEARCH_VENUES_FRAGMENT_INDEX);
    }

    public void onFragmentSelected(){
        reloadVenuesOnFragmentShow();
    }

    @Override
    public Loader<List<Venue>> onCreateLoader(int id, Bundle args) {
        loadingStarted = true;
        if (hasNextPage) {
            PopularVenuesFragment popularVenuesFragment = getPopularVenuesFragment();
            if (popularVenuesFragment != null) popularVenuesFragment.showLoadingFooter();
        }
        return new VenueLoader(getActivity().getApplicationContext(), null, false, previousItems);
    }

    @Override
    public void onLoadFinished(Loader<List<Venue>> loader, List<Venue> data) {
        loadingStarted = false;
        VenueLoader venueLoader = (VenueLoader)loader;
        if (googleMap == null || getActivity() == null) return;
        if (data != null) {
            venueHashMap.clear();
            googleMap.clear();
            Marker marker;
            ArrayList<BitmapDescriptor> markerIcons = venueLoader.getMarkerIcons();
            int i = 0;
            for (Venue venue : data) {
                marker = googleMap.addMarker(new MarkerOptions().
                        position(new LatLng(venue.getLocation().getLatitude(),
                                venue.getLocation().getLongitude())).
                        icon(markerIcons.get(i)));
                venueHashMap.put(marker, venue);
                i++;
            }
        }
        PopularVenuesFragment popularVenuesFragment = getPopularVenuesFragment();
        if (popularVenuesFragment != null) popularVenuesFragment.onVenuesLoaded(data, (VenueLoader)loader);
        VenueSearchFragment venueSearchFragment = getVenueSearchFragment();
        if (venueSearchFragment != null) venueSearchFragment.onVenuesLoaded(data);

        hasNextPage = venueLoader.hasNextPage();
        previousItems = data;
    }

    @Override
    public void onLoaderReset(Loader<List<Venue>> loader) {}

    public void onGetLastElements() {
        if (hasNextPage && !loadingStarted) getLoaderManager().restartLoader(0, null, this);
    }
}