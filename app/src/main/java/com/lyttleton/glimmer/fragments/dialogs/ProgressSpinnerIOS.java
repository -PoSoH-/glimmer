package com.lyttleton.glimmer.fragments.dialogs;


import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;

import com.lyttleton.glimmer.R;

public class ProgressSpinnerIOS extends DialogFragment {

    private Dialog dialog;
    public static final int TYPE_SPINNER = 0;
    public static final int TYPE_LOAD = 1;
    public static final int TYPE_REGISTERED = 2;
    public static final String TYPE_KEY = "DIALOG-TYPE-KEY";
    private int _DialogType = 0;

    public static ProgressSpinnerIOS show(FragmentActivity activity, int typeVisible) {
        ProgressSpinnerIOS dialogFragment = new ProgressSpinnerIOS();
        Bundle arguments = new Bundle();
        arguments.putInt(TYPE_KEY, typeVisible);
        dialogFragment.setArguments(arguments);
        dialogFragment.show(activity.getSupportFragmentManager(),
                ProgressSpinnerIOS.class.getSimpleName());
        return dialogFragment;
    }

    public static ProgressSpinnerIOS show(FragmentActivity activity) {
        ProgressSpinnerIOS dialogFragment = new ProgressSpinnerIOS();
        dialogFragment.show(activity.getSupportFragmentManager(),
                ProgressSpinnerIOS.class.getSimpleName());
        return dialogFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = new Dialog(getActivity());
        View view = null;

        Bundle arguments = getArguments();
        if(arguments != null)
            _DialogType = (int)getArguments().getInt(TYPE_KEY, _DialogType);

        switch (_DialogType) {
            case TYPE_SPINNER:
                    view = getActivity().getLayoutInflater().inflate(R.layout.fr_progress_spinner_ios,
                        null);
                break;
            case TYPE_REGISTERED:
                    view = getActivity().getLayoutInflater().inflate(R.layout.fr_progress_registered,
                    null);
                break;
            case TYPE_LOAD:
                view = getActivity().getLayoutInflater().inflate(R.layout.fr_progress_loading,
                        null);
                break;
        }

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(view);

        return dialog;
    }

    public void DialogDismiss() {dialog.dismiss();}

}
