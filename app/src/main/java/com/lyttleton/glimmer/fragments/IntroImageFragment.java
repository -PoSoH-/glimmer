package com.lyttleton.glimmer.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lyttleton.glimmer.R;
import com.squareup.picasso.Picasso;

public class IntroImageFragment extends Fragment {

    public static final String INDEX_ARG = "INDEX_ARG";
    private static int[] IMAGES = {R.drawable.discover_phone,
            R.drawable.clue_phone, R.drawable.match_phone};

    public static IntroImageFragment create(int index){
        IntroImageFragment fragment = new IntroImageFragment();
        Bundle args = new Bundle();
        args.putInt(INDEX_ARG, index);
        fragment.setArguments(args);
        return fragment;
    }

    public IntroImageFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        int index = getArguments().getInt(INDEX_ARG);

        View view = inflater.inflate(R.layout.intro_image_fragment, container, false);

        String introTitles[] = getResources().getStringArray(R.array.intro_titles),
                introText[] = getResources().getStringArray(R.array.intro_text);

        ((TextView)view.findViewById(R.id.intro_title_tv)).setText(introTitles[index]);
        ((TextView)view.findViewById(R.id.intro_text_tv)).setText(introText[index]);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        Picasso.with(getActivity().getApplicationContext()).load(IMAGES[index]).
                into((ImageView) view.findViewById(R.id.intro_iv));

        return view;
    }
}