package com.lyttleton.glimmer.fragments.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.lyttleton.glimmer.R;

public class ErrorMessageDialog extends DialogFragment {

    public static final String DATA_STRING = "DATA_STRING";

    public static ErrorMessageDialog show(FragmentActivity fragmentActivity, String messagesError) {
        ErrorMessageDialog dialog = new ErrorMessageDialog();
        Bundle arguments = new Bundle();
        arguments.putString(DATA_STRING, messagesError);
        dialog.setArguments(arguments);
        FragmentTransaction ft = fragmentActivity.getSupportFragmentManager().beginTransaction();
        ft.add(dialog, null);
        ft.commitAllowingStateLoss();
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());

        final View view = getActivity().getLayoutInflater().inflate(R.layout._dialog_error_messages, null);

        TextView textError = (TextView) view.findViewById(R.id.text_dialog_message);

        Bundle arguments = getArguments();
        if(arguments != null){
            textError.setText(arguments.getString(DATA_STRING));
        }

        view.findViewById(R.id.btn_dialog_error_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(view);

        return dialog;
    }
}

