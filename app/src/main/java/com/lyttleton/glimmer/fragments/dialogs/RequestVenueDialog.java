package com.lyttleton.glimmer.fragments.dialogs;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.helpers.ToastHelper;
import com.lyttleton.glimmer.loaders.Backend;
import com.parse.ParseObject;

public class RequestVenueDialog extends DialogFragment {

    public static final String VENUE_NAME_ARG = "VENUE_NAME_ARG";

    // Request venue from user profile
    public static void show(FragmentActivity fragmentActivity) {
        if (fragmentActivity.getSupportFragmentManager().
                findFragmentByTag(RequestVenueDialog.class.getSimpleName()) == null) {
            RequestVenueDialog dialog = new RequestVenueDialog();
            FragmentTransaction ft = fragmentActivity.getSupportFragmentManager().beginTransaction();
            ft.add(dialog, RequestVenueDialog.class.getSimpleName());
            ft.commitAllowingStateLoss();
            dialog.setCancelable(false);
        }
    }

    // Search venue
    public static void show(FragmentActivity fragmentActivity, String venueName) {
        if (fragmentActivity.getSupportFragmentManager().
                findFragmentByTag(RequestVenueDialog.class.getSimpleName()) == null) {
            RequestVenueDialog dialog = new RequestVenueDialog();
            Bundle args = new Bundle();
            args.putString(VENUE_NAME_ARG, venueName);
            dialog.setArguments(args);
            FragmentTransaction ft = fragmentActivity.getSupportFragmentManager().beginTransaction();
            ft.add(dialog, RequestVenueDialog.class.getSimpleName());
            ft.commitAllowingStateLoss();
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());
        final View view = getActivity().getLayoutInflater().inflate(R.layout.
                cancel_or_accept_dialog_layout, null);

        Bundle args = getArguments();
        String venueName = null;
        if (args != null) venueName = args.getString(VENUE_NAME_ARG);
        final EditText venueNameEt = (EditText) view.findViewById(R.id.venue_name_et);

        if (!TextUtils.isEmpty(venueName)) {
            TextView dialogMessage = (TextView) view.findViewById(R.id.dialog_message_tv);
            dialogMessage.setVisibility(View.VISIBLE);
            dialogMessage.setText(venueName + " " + getString(R.string.venue_not_on_glimmer));
        } else venueNameEt.setVisibility(View.VISIBLE);

        TextView title = (TextView) view.findViewById(R.id.dialog_title_tv);
        title.setText(R.string.request_venue);

        final String finalVenueName = venueName;

        view.findViewById(R.id.cancel_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        Button acceptBt = (Button) view.findViewById(R.id.accept_bt);
        acceptBt.setText(R.string.request);
        acceptBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(finalVenueName)) { // Request venue from user profile
                    final String enteredVenueName = venueNameEt.getText().toString();
                    if (!TextUtils.isEmpty(enteredVenueName)) {
                        // Search if venue exists
                        Backend.searchVenue(enteredVenueName, getActivity(), new Backend.
                                OnSuccessResponseListener(getActivity()) {
                            @Override
                            public void onSuccess(ParseObject parseObject) {
                                ToastHelper.showErrorToast(getActivity(), R.string.venue_exists);
                            }

                            @Override
                            public void onError(String error, int errorCode) {
                                if (errorCode == Backend.OBJECT_NOT_FOUND_ERROR_CODE) {
                                    // Search if request for that user exists
                                    Backend.searchRequest(enteredVenueName, getActivity(),
                                            new Backend.OnSuccessResponseListener(getActivity()) {
                                                @Override
                                                public void onSuccess(ParseObject parseObject) {
                                                    ToastHelper.showErrorToast(getActivity(),
                                                            R.string.request_exists);
                                                }

                                                @Override
                                                public void onError(String error, int errorCode) {
                                                    if (errorCode == Backend.OBJECT_NOT_FOUND_ERROR_CODE) {
                                                        NotifyFriendsDialog.show(getActivity(),
                                                                enteredVenueName);
                                                        getDialog().dismiss();
                                                        Backend.saveRequest(enteredVenueName);
                                                    } else super.onError(error, errorCode);
                                                }
                                            });
                                } else super.onError(error, errorCode);
                            }
                        });
                    }
                } else { // Search venue
                    // Search if request for that user exists
                    Backend.searchRequest(finalVenueName, getActivity(),
                            new Backend.OnSuccessResponseListener(getActivity()) {
                                @Override
                                public void onSuccess(ParseObject parseObject) {
                                    ToastHelper.showErrorToast(getActivity(),
                                            R.string.request_exists);
                                    getDialog().dismiss();
                                }

                                @Override
                                public void onError(String error, int errorCode) {
                                    if (errorCode == Backend.OBJECT_NOT_FOUND_ERROR_CODE) {
                                        NotifyFriendsDialog.show(getActivity(), finalVenueName);
                                        getDialog().dismiss();
                                        Backend.saveRequest(finalVenueName);
                                    } else super.onError(error, errorCode);
                                }
                            });
                }
            }
        });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }
}
