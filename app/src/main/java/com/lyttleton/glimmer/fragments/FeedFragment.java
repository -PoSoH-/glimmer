package com.lyttleton.glimmer.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.adapters.FeedViewPagerAdapter;

public class FeedFragment extends Fragment {

    private ViewPager viewPager;
    private View tabs[], triangles[];

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        viewPager.setAdapter(new FeedViewPagerAdapter(getChildFragmentManager()));
        initializeClickListeners();
        updateTabs();
    }

    private void initializeClickListeners() {
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                updateTabs();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        for (int i = 0; i < tabs.length; i++) {
            final int finalI = i;
            tabs[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewPager.setCurrentItem(finalI);
                }
            });
        }
    }

    private void updateTabs(){
        for (int i = 0; i < tabs.length; i++){
            if (i == viewPager.getCurrentItem()){
                tabs[i].setBackgroundColor(getResources().getColor(R.color.dark_grey));
                triangles[i].setVisibility(View.VISIBLE);
            } else {
                tabs[i].setBackgroundColor(getResources().getColor(R.color.light_grey));
                triangles[i].setVisibility(View.GONE);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.feed_layout, null);
        View winksTab = view.findViewById(R.id.winks_tv);
        View matchesTab = view.findViewById(R.id.matches_tv);
        tabs = new View[]{winksTab, matchesTab};

        View winksTriangle = view.findViewById(R.id.winks_triangle);
        View matchesTriangle = view.findViewById(R.id.matches_triangle);
        triangles = new View[]{winksTriangle, matchesTriangle};

        viewPager = (ViewPager) view.findViewById(R.id.feed_view_pager);
        return view;
    }
}
