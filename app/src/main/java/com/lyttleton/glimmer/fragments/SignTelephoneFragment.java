package com.lyttleton.glimmer.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.lyttleton.glimmer.App;
import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.activities.BaseActivity;
import com.lyttleton.glimmer.activities.IntroActivity;
import com.lyttleton.glimmer.fragments.dialogs.ErrorMessageDialog;
import com.lyttleton.glimmer.loaders.Backend;

public class SignTelephoneFragment extends Fragment {

    private Button btnSignTelephone;
    private Spinner spinnerSelectedCode;
    private EditText editTelephone;
    private OnClickListenerDialogProgress listener;
    public static final String NOT_NUMBER_PATTERN = "[^0-9]";

    public static SignTelephoneFragment create(){
        return new SignTelephoneFragment();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(listener != null) listener.onListenerChangeLabelText(getString(R.string.txt_label_mobile_number));

        initializationOnClickListener();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout._fr_input_telephone, null);
        spinnerSelectedCode = (Spinner) view.findViewById(R.id.spinner_prefix_mobile_number);
        String []infoCodeNumber = getActivity().getResources().getStringArray(R.array.countryCodes);
        Log.d("glimmer", "...fragment telephone - list to prefix ... ");
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, infoCodeNumber);
        spinnerAdapter.setDropDownViewResource(R.layout.spinner_item);
        Log.d("glimmer", "...fragment telephone - list to prefix ... crash to add adapter");
        spinnerSelectedCode.setAdapter(spinnerAdapter);
        spinnerSelectedCode.setSelection(84); //Ireland (+353) State telephone code
        btnSignTelephone = (Button) view.findViewById(R.id.btn_sign_telephone);
        editTelephone = (EditText) view.findViewById(R.id.edit_telephone);
        return view;
    }

    private void initializationOnClickListener(){
        btnSignTelephone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendRegisteredTelephoneNUmber();
            }
        });

    }

    private void sendRegisteredTelephoneNUmber(){
        if(listener!= null) {
            App.hideKeyboard(getActivity());
            if(!emptyNumberPhone()) return;
            listener.OnListenerDialogProgressShow();
            int _PrefixCodeNumber = Integer.parseInt(((String) spinnerSelectedCode.getSelectedItem()).replaceAll(NOT_NUMBER_PATTERN, ""));
            String _TelephoneNumber = editTelephone.getText().toString();
            if (!_TelephoneNumber.matches(NOT_NUMBER_PATTERN)) {
                final String telephoneNumber = _PrefixCodeNumber + _TelephoneNumber;
                listener.OnListenerTelephoneNumber(telephoneNumber);
                listener.onListenerCreateVerificationCode();

                Backend.addedNumberPhone(String.valueOf(_PrefixCodeNumber), _TelephoneNumber, new Backend.Response() {
                    @Override
                    public void onSuccess() {
                        Backend.sendVerificationTelephoneNumber(getActivity(), telephoneNumber, listener.onListenerVerificationCode(), new ResponseListener() {
                            @Override
                            public void success() {
                                listener.OnListenerDialogProgressHide();
                                BaseActivity.setFragment(getActivity(), new SignVerifyFragment(), R.id.fragment_container, false);
                            }

                            @Override
                            public void error(String errorMessage) {
                                listener.OnListenerDialogProgressHide();
                                ErrorMessageDialog.show(getActivity(), errorMessage);
                            }
                        });
                    }

                    @Override
                    public void onError(String error, int errorCode) {
                        listener.OnListenerDialogProgressHide();
                        ErrorMessageDialog.show(getActivity(), error);
                    }
                });
            }
        }
    }

    private boolean emptyNumberPhone(){
        if(editTelephone.getText() == null){
            ErrorMessageDialog.show(getActivity(), getString(R.string.txt_error_telephone));
            return false;
        }
        else if (editTelephone.getText().toString().equals("")){
            ErrorMessageDialog.show(getActivity(), getString(R.string.txt_error_telephone));
            return false;
        }
        else if (editTelephone.getText().toString().matches(NOT_NUMBER_PATTERN)){
            ErrorMessageDialog.show(getActivity(), getString(R.string.txt_error_telephone));
            return false;
        }
        return true;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof OnClickListenerDialogProgress)
            listener = (OnClickListenerDialogProgress)activity;
        else
            throw new ClassCastException("Error!.. Please implements OnClickListenerDialogProgress interface!");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public interface ResponseListener {
        void success();
        void error(String codeMessage);
    }

    public interface OnClickListenerDialogProgress{
        void onListenerCreateVerificationCode();
        String onListenerVerificationCode();
        void OnListenerDialogProgressShow();
        void OnListenerDialogProgressHide();
        void OnListenerTelephoneNumber(String telephoneNumber);
        void onListenerChangeLabelText(String textLabel);
    }
}
