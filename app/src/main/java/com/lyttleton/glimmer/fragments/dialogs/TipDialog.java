package com.lyttleton.glimmer.fragments.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.helpers.SharedPrefsHelper;

public class TipDialog extends DialogFragment {

    public static final String TITLE_ARG = "TITLE_ARG";
    public static final String MESSAGE_ARG = "MESSAGE_ARG";
    public static final String TIP_ID_ARG = "TIP_ID_ARG";

//    public static interface TipDialogListener{
//        void onAcceptTip();
//    }
//
//    private TipDialogListener tipDialogListener;
//
//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        tipDialogListener = (TipDialogListener) activity;
//    }

    public static void show(FragmentActivity fragmentActivity, int titleResId, String message,
                            String tipId) {
        show(fragmentActivity, fragmentActivity.getString(titleResId), message, tipId);
    }

    public static void show(FragmentActivity fragmentActivity, int titleResId, int messageResId,
                            String tipId) {
        show(fragmentActivity, fragmentActivity.getString(titleResId),
                fragmentActivity.getString(messageResId), tipId);
    }

    public static void show(FragmentActivity fragmentActivity, String titleResId, String messageResId,
                            String tipId) {
        if (fragmentActivity.getSupportFragmentManager().
                findFragmentByTag(TipDialog.class.getSimpleName()) == null) {
            TipDialog dialog = new TipDialog();
            Bundle args = new Bundle();
            args.putString(TITLE_ARG, titleResId);
            args.putString(MESSAGE_ARG, messageResId);
            args.putString(TIP_ID_ARG, tipId);
            dialog.setArguments(args);
            FragmentTransaction ft = fragmentActivity.getSupportFragmentManager().beginTransaction();
            ft.add(dialog, TipDialog.class.getSimpleName());
            ft.commitAllowingStateLoss();
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());
        final View view = getActivity().getLayoutInflater().inflate(R.layout.accept_dialog_layout,
                null);

        final String title = getArguments().getString(TITLE_ARG),
                message = getArguments().getString(MESSAGE_ARG),
                tipId = getArguments().getString(TIP_ID_ARG);

        TextView dialogTitleTv = (TextView) view.findViewById(R.id.dialog_title_tv);
        dialogTitleTv.setText(title);

        TextView dialogMessageTv = (TextView) view.findViewById(R.id.dialog_message_tv);
        dialogMessageTv.setText(message);

        view.findViewById(R.id.accept_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
                SharedPrefsHelper.saveTip(getActivity(), tipId);
            }
        });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }
}
