package com.lyttleton.glimmer.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.activities.MainActivity;
import com.lyttleton.glimmer.helpers.ToastHelper;
import com.lyttleton.glimmer.loaders.Backend;

public class GenderInterestFragment extends Fragment {

    private Button femalesBt, malesBt;
    private View femalesIndicator, malesIndicator, readyBt, loadingLayout;
    public static final int UNDEFINED = -1, FEMALES = 0, MALES = 1;
    private int interest = UNDEFINED;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initializeClickListeners();

//        String gender = Backend.getCurrentUser().getString(Backend.TU_GENDER_COLUMN);
//        if (gender != null && gender.equals(Backend.FEMALE)) malesBt.performClick();
    }

    private void initializeClickListeners() {
        femalesBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interest = FEMALES;
                femalesBt.setBackgroundColor(getResources().getColor(R.color.purple));
                femalesBt.setTextColor(Color.WHITE);
                femalesIndicator.setVisibility(View.VISIBLE);

                malesBt.setBackgroundResource(R.drawable.gender_interest_selector);
                malesBt.setTextColor(getResources().getColorStateList(R.color.gender_text_selector));
                malesIndicator.setVisibility(View.GONE);
            }
        });

        malesBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interest = MALES;
                malesBt.setBackgroundColor(getResources().getColor(R.color.purple));
                malesBt.setTextColor(Color.WHITE);
                malesIndicator.setVisibility(View.VISIBLE);

                femalesBt.setBackgroundResource(R.drawable.gender_interest_selector);
                femalesBt.setTextColor(getResources().getColorStateList(R.color.gender_text_selector));
                femalesIndicator.setVisibility(View.GONE);
            }
        });

        readyBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (interest == UNDEFINED)
                    ToastHelper.showErrorToast(getActivity().getApplicationContext(), R.string.choose_gender);
                else {
                    loadingLayout.setVisibility(View.VISIBLE);
                    Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
                    animation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {}

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            getActivity().finish();
                            MainActivity.start(getActivity());
                            Backend.saveUserInterest(interest);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {}
                    });
                    loadingLayout.startAnimation(animation);
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.gender_interest_layout, null);

        femalesBt = (Button) view.findViewById(R.id.females_bt);
        malesBt = (Button) view.findViewById(R.id.males_bt);
        femalesIndicator = view.findViewById(R.id.females_indicator);
        malesIndicator = view.findViewById(R.id.males_indicator);
        readyBt = view.findViewById(R.id.ready_bt);
        loadingLayout = view.findViewById(R.id.loading_layout);
        ((TextView)loadingLayout.findViewById(R.id.loading_text_tv)).setText(R.string.just_a_tick);

        return view;
    }
}
