package com.lyttleton.glimmer.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import com.lyttleton.glimmer.DataAccessObject.ChatORM;
import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.adapters.Chat;
import com.lyttleton.glimmer.adapters.ChatAdapter;
import com.lyttleton.glimmer.adapters.User;
import com.lyttleton.glimmer.adapters.Wink;
import com.lyttleton.glimmer.fragments.dialogs.CameraSelectionDialog;
import com.lyttleton.glimmer.helpers.InternetHelper;
import com.lyttleton.glimmer.helpers.MediaSelector;
import com.lyttleton.glimmer.helpers.SharedPrefsHelper;
import com.lyttleton.glimmer.helpers.ToastHelper;
import com.lyttleton.glimmer.loaders.Backend;
import com.lyttleton.glimmer.loaders.ChatLoader;
import com.parse.ParseObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;

public class ChatFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<Chat>> {

    public static final String USER_ARG = "USER_ARG";
    private ListView listView;
    private EditText messageEt;
    private View sendTv
//            , progressBar
            , cameraIv;
    private User user, currentUser;
    private ChatAdapter adapter;
    private MediaSelector mediaSelector;
    private View noInternetLayout;

    // Paging
    private boolean hasNextPage, loading, scrolledToBottom;
    private List<Chat> previousItems;
    private View loadingHeader;

    public static ChatFragment create(User user){
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putSerializable(USER_ARG, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().registerReceiver(newMessageBroadcastReceiver,
                new IntentFilter(Backend.PARSE_PUSH_ACTION));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(newMessageBroadcastReceiver);
        SharedPrefsHelper.removeUserIdOfOpenedChat(getActivity());
    }

    private BroadcastReceiver newMessageBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                JSONObject data = new JSONObject(intent.getExtras().getString(Backend.PARSE_PUSH_DATA));
                if (data.has(Backend.TYPE_COLUMN) && data.getInt(Backend.TYPE_COLUMN) == Wink.CHAT_TYPE){
                    String objectId = data.getString(Backend.OBJECT_ID_COLUMN);
                    Backend.loadChatMessage(objectId, new Backend.ResponseListener() {
                        @Override
                        public void onSuccess(ParseObject parseObject) {
                            if (adapter != null) {
                                Chat chat = Backend.makeChat(parseObject, user);
                                addNewChatMessage(chat);
//                                if (previousItems != null) {
//                                    previousItems.add(0, chat);
//                                    Collections.sort(previousItems, new Comparator<Chat>() {
//                                        @Override
//                                        public int compare(Chat lhs, Chat rhs) {
//                                            if (lhs.getDate().getTime() < rhs.getDate().getTime())
//                                                return 1;
//                                            else if (lhs.getDate().getTime() > rhs.getDate().getTime())
//                                                return -1;
//                                            else return 0;
//                                        }
//                                    });
//                                    adapter.setData(previousItems);
//                                }
                            }
                        }

                        @Override
                        public void onError(String error, int errorCode) {

                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        user = (User) getArguments().getSerializable(USER_ARG);
        currentUser = Backend.makeUser(Backend.getCurrentUser());
        previousItems = ChatORM.loadChatFromDataBase(user.getObjectId(), currentUser.getObjectId());
        adapter = new ChatAdapter(getActivity(), this);
        if (previousItems != null) {
            for(int pos=0, len=previousItems.size(); pos<len; pos++){
                if(previousItems.get(pos).getSenderId().equals(user.getObjectId())) {
                    previousItems.get(pos).addUserSender(user);
                    previousItems.get(pos).addUserReceiver(currentUser);
                }else{
                    previousItems.get(pos).addUserSender(currentUser);
                    previousItems.get(pos).addUserReceiver(user);
                }
            }
            adapter.setData(previousItems);
            scrollToBottom(false);
        }

        listView.addHeaderView(loadingHeader);
        listView.setAdapter(adapter);
        listView.removeHeaderView(loadingHeader);
        mediaSelector = new MediaSelector(getActivity());

        SharedPrefsHelper.removeUserIdFromNewChatMessages(user.getObjectId(), getActivity());
        SharedPrefsHelper.saveUserIdOfOpenedChat(user.getObjectId(), getActivity());

        initializeClickListeners();
        getLoaderManager().initLoader(0, null, this);
    }

    private void initializeClickListeners() {
        sendTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = getActivity().getApplicationContext();
                if (!InternetHelper.checkInternetConnection(context))
                    ToastHelper.showErrorToast(context, R.string.check_your_internet_connection);
                else if (TextUtils.isEmpty(messageEt.getText()) && !mediaSelector.isMediaSelected())
                    messageEt.requestFocus();
                else {
                    final Chat chat = new Chat(null, messageEt.getText().toString(), currentUser, user,
                            new Date());
                    chat.setMediaPath(mediaSelector.getMediaPath());
                    messageEt.setText("");
                    mediaSelector.removeSelection();

                    Backend.saveChatMessage(chat, getActivity(),
                            new Backend.OnSuccessResponseListener(getActivity().getApplicationContext()) {
                                @Override
                                public void onSuccess(ParseObject parseObject) {
                                    chat.setObjectId(parseObject.getObjectId());
                                    addNewChatMessage(chat);
                                }
                            });
                }
            }
        });

        cameraIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CameraSelectionDialog.show(getActivity());
            }
        });

        noInternetLayout.findViewById(R.id.go_to_settings_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InternetHelper.openWifiSettings(getActivity());
            }
        });
    }

    private void addNewChatMessage(Chat chatMessage) {
        ChatORM.addChat(chatMessage);
        adapter.add(chatMessage);
        if (previousItems != null) previousItems.add(chatMessage);
        scrollToBottom(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_layout, null);
        listView = (ListView) view.findViewById(R.id.chat_lv);
        messageEt = (EditText) view.findViewById(R.id.message_et);
        sendTv = view.findViewById(R.id.send_tv);
//        progressBar = view.findViewById(R.id.progress_bar);
        loadingHeader = inflater.inflate(R.layout.loading_footer, null);
        cameraIv = view.findViewById(R.id.camera_iv);
        noInternetLayout = view.findViewById(R.id.no_internet_layout);
        return view;
    }

    @Override
    public Loader<List<Chat>> onCreateLoader(int id, Bundle args) {
        loading = true;
        if (previousItems == null || previousItems.size() == 0) {
            listView.setVisibility(View.GONE);
//            progressBar.setVisibility(View.VISIBLE);
        }
        return new ChatLoader(getActivity(), user, previousItems);
    }

    @Override
    public void onLoadFinished(Loader<List<Chat>> loader, final List<Chat> data) {
        loading = false;
        listView.setVisibility(View.VISIBLE);
//        progressBar.setVisibility(View.GONE);
        listView.removeHeaderView(loadingHeader);

        if (data != null) adapter.setData(data);

        ChatLoader chatLoader = (ChatLoader) loader;
//        noInternetLayout.setVisibility(View.GONE);
//        if ((data == null || data.size() == 0) && adapter.getCount() == 0 && chatLoader.isNoInternet())
//            noInternetLayout.setVisibility(View.VISIBLE);

        hasNextPage = ((ChatLoader)loader).hasNextPage();
        if (previousItems == null || previousItems.size() == 0) scrollToBottom(false);
        else if (data != null && data.size() != 0 && data.size() - 1 < adapter.getCount()) {
            listView.post(new Runnable() {
                @Override
                public void run() {
                    listView.setSelection(data.size() - 1);
                    scrolledToBottom = true;
                }
            });
        }
        previousItems = data;
    }

    private void scrollToBottom(boolean animated) {
        final int lastItemIndex = adapter.getCount() - 1;
        if (animated) {
            if (listView.getLastVisiblePosition() >= lastItemIndex - 1)
                listView.smoothScrollToPosition(lastItemIndex);
        } else {
            listView.post(new Runnable() {
                @Override
                public void run() {
                    listView.setSelection(lastItemIndex);
                    scrolledToBottom = true;
                }
            });
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Chat>> loader) {}

    public void onGetFirstElements() {
        if (hasNextPage && !loading && scrolledToBottom)
            getLoaderManager().restartLoader(0, null, this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mediaSelector.onActivityResult(requestCode, resultCode, data);
    }

    public void setMediaUri(Uri uri) {
        mediaSelector.setMediaUri(uri);
    }

}