package com.lyttleton.glimmer.fragments.scroll_tab_holder;

public interface AdjustScrollListener {
    void adjustScroll(int scrollHeight, int visibleHeaderHeight);
}
