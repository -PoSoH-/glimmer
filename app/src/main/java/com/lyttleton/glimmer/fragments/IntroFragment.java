package com.lyttleton.glimmer.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.activities.BaseActivity;
import com.lyttleton.glimmer.activities.IntroActivity;
import com.lyttleton.glimmer.activities.MyProfilePhotosActivity;
import com.lyttleton.glimmer.adapters.IntroImagesPagerAdapter;
import com.lyttleton.glimmer.loaders.Backend;

public class IntroFragment extends Fragment {

    private ViewPager viewPager;
    private ImageView tabs[];
    private View loginButton;
    private String _PageSelected;
    private TextView txtLabel;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        viewPager.setAdapter(new IntroImagesPagerAdapter(getChildFragmentManager(), tabs.length));
        initializeClickListeners();

        _PageSelected = getActivity().getIntent().getStringExtra(IntroActivity.PAGE_SELECTED_IN_START);

        switch (_PageSelected){
            case IntroActivity.PAGE_INTRO:
                Log.d("glimmer", "...fragment Intro - PAGE_INTRO");
                BaseActivity.setFragment(getActivity(), new IntroFragment(), R.id.fragment_container, false);
//                ((IntroActivity)getActivity()).getLabel().setText("Sign Up");
//                ((IntroActivity)getActivity()).showLabel();
                break;
            case IntroActivity.PAGE_SIGN_UP:
                Log.d("glimmer", "...fragment Intro - PAGE_SIGN_UP");
                BaseActivity.setFragment(getActivity(), new SignUserFragment(), R.id.fragment_container, false);
                break;
            case IntroActivity.PAGE_TELEPHONE:
                Log.d("glimmer", "...fragment Intro - PAGE_TELEPHONE");
                BaseActivity.setFragment(getActivity(), new SignTelephoneFragment(), R.id.fragment_container, false);
                break;
            case IntroActivity.PAGE_VERIFIED:
                Log.d("glimmer", "...fragment Intro - PAGE_VERIFIED");
                BaseActivity.setFragment(getActivity(), new SignVerifyFragment(), R.id.fragment_container, false);
                break;
            case IntroActivity.PAGE_ADD_IMAGE:
                Log.d("glimmer", "...fragment Intro - PAGE_ADD_IMAGE");
                BaseActivity.setFragment(getActivity(), new SignUserPhotoFragment(), R.id.fragment_container, false);
//                MyProfilePhotosActivity.start(getActivity());
                break;
        }
//        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
    }

    private void initializeClickListeners() {
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {}
            @Override
            public void onPageSelected(int i) {
                updateImageView(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {}
        });

        for (int i = 0; i < tabs.length; i++) {
            final View tab = tabs[i];
            final int finalI = i;
            tab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewPager.setCurrentItem(finalI);
                }
            });
            tab.performClick();
        }
        tabs[0].performClick();

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    private void updateImageView(int selectedFragment){
        for (int i = 0; i < tabs.length; i++) {
            if (i != selectedFragment) tabs[i].setImageResource(R.drawable.scroller_circle_default);
            else tabs[i].setImageResource(R.drawable.scroller_circle_selected);
        }
    }

    public void login() {
        Log.i("Fragment intro","...Button GET STARTED press");
        BaseActivity.setFragment(getActivity(), new SignUserFragment(), R.id.fragment_container, false);
//        Backend.login(getActivity(), new Backend.OnSuccessResponseListener(getActivity()) {
//            @Override
//            public void onSuccess(ParseObject parseObject) {
//                BaseActivity.setFragment(getActivity(), new GenderInterestFragment(),
//                        R.id.fragment_container, true);
//            }
//        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.intro_layout, null);
        viewPager = (ViewPager) view.findViewById(R.id.view_pager);
        tabs = new ImageView[]{
                (ImageView) view.findViewById(R.id.image_view_2),
                (ImageView) view.findViewById(R.id.image_view_3),
                (ImageView) view.findViewById(R.id.image_view_4)};
        loginButton = view.findViewById(R.id.login_bt);
        txtLabel = (TextView) view.findViewById(R.id.label_tv);
        return view;
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
////        Backend.onActivityResult(requestCode, resultCode, data);
//    }
}
