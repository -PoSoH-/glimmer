package com.lyttleton.glimmer.fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.activities.MainActivity;
import com.lyttleton.glimmer.fragments.dialogs.CameraSelectionDialog;
import com.lyttleton.glimmer.fragments.dialogs.ProgressSpinnerIOS;
import com.lyttleton.glimmer.helpers.FileHelper;
import com.lyttleton.glimmer.helpers.SquareTransform;
import com.lyttleton.glimmer.loaders.Backend;
import com.squareup.picasso.Picasso;

import java.io.File;


/**
 * Created by Sergiy Polishuk on 08.02.2016.
 */

public class SignUserPhotoFragment extends Fragment {

    private ImageView imgCenterPhoto;
    private MediaFragmentListener listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (MediaFragmentListener) activity;
    }

    public static SignUserPhotoFragment create(){
        return new SignUserPhotoFragment();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(listener != null) listener.onListenerChangeLabelText(getString(R.string.txt_label_selected_photo));

        imgCenterPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //                MainActivity.start(getActivity());
                listener.onClickListenerShowMediaDialog();
//                CameraSelectionDialog.show(SignUserPhotoFragment.);
//                CameraSelectionDialog.show(this);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_profile_photos_layout, null);
        imgCenterPhoto = (ImageView) view.findViewById(R.id.dailyProfilerIv);
        view.findViewById(R.id.edit_daily_profiler_bt).setVisibility(View.GONE);
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void addPhoto(String uri){

        Picasso.with(getActivity())
                .load(new File(uri))
//                .centerCrop()
                .transform(new SquareTransform())
                .fit()
                .into(imgCenterPhoto);
        getActivity().findViewById(R.id.progress_bar).setVisibility(View.GONE);
        ProgressSpinnerIOS.show(getActivity(), ProgressSpinnerIOS.TYPE_LOAD);
        Backend.addedImageProfile(uri, new Backend.Response() {
            @Override
            public void onSuccess() {
                MainActivity.start(getActivity());
                getActivity().finish();
//                spinner.dismiss();
            }

            @Override
            public void onError(String error, int errorCode) {
//                spinner.dismiss();
            }
        });
    }

    public interface MediaFragmentListener{
        void onClickListenerShowMediaDialog();
        void onListenerChangeLabelText(String textLabel);
    }
}
