package com.lyttleton.glimmer.fragments;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.lyttleton.glimmer.App;
import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.activities.BaseActivity;
import com.lyttleton.glimmer.adapters.PinPop;
import com.lyttleton.glimmer.adapters.Venue;
import com.lyttleton.glimmer.fragments.dialogs.GpsCheckDialog;
import com.lyttleton.glimmer.fragments.scroll_tab_holder.ScrollChangeListener;
import com.lyttleton.glimmer.helpers.InternetHelper;
import com.lyttleton.glimmer.helpers.TimeHelper;
import com.lyttleton.glimmer.helpers.ToastHelper;
import com.lyttleton.glimmer.loaders.Backend;
import com.parse.ParseObject;

import java.util.Date;

public class VenueFragment extends Fragment implements ScrollChangeListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    public static final String VENUE_ARG = "VENUE_ARG";
    public static final int LOCATION_UPDATES_INTERVAL = 10000;
    public static final int LOCATION_UPDATES_FASTEST_INTERVAL = 5000;
    public static final int DISTANCE_FOR_POP = 500;
    public static final long MAKE_PIN_POP_INTERVAL = TimeHelper.HOUR_MILLIS;
    public static final long CHECK_PIN_POP_BUTTON_INTERVAL = TimeHelper.SECOND_MILLIS;

    private TextView addressTv;
    private Button pinBt;
    private Button popBt;
    private View header;
    private View venueHeader;
    private int headerHeight, visibleHeaderHeight, invisibleHeaderHeight;
    private PinPopListFragment pinPopListFragment;

//    private TextView tabInfoTv;
//    private View todayTv, everTv;
//    private ViewPager viewPager;

    private Venue venue;
//    private VenueViewPagerAdapter adapter;

    private View loadingImage;
    private TextView loadingTextTv;

    // Used for location
    private GoogleApiClient googleApiClient;

    public static VenueFragment create(Venue venue){
        VenueFragment fragment = new VenueFragment();
        Bundle args = new Bundle();
        args.putSerializable(VENUE_ARG, venue);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        venue = (Venue) getArguments().getSerializable(VENUE_ARG);
        addressTv.setText(venue.getAddress());
//        adapter = new VenueViewPagerAdapter(getChildFragmentManager(), venue);

/*        if (header.getViewTreeObserver().isAlive())
            header.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    com.lyttleton.glimmer.helpers.ViewHelper.removeOnGlobalLayoutListener(header, this);
                    init();
                }
            });
        else */
        init();

        pinPopListFragment = PinPopListFragment.create(PinPopListFragment.ALL_DATA_TYPE, venue);

        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.pin_pop_layout, pinPopListFragment, pinPopListFragment.
                getClass().getSimpleName()).commit();

        updatePinPopButtons();
    }

    private void savePin(){
        Backend.savePinPop(venue, getActivity(), Backend.PIN_TYPE, new Backend.
                OnSuccessResponseListener(getActivity()) {
            @Override
            public void onSuccess(ParseObject parseObject) {
                addPinPopToFragments(parseObject);
                Date date = parseObject.getDate(Backend.TIME_STAMP_COLUMN);
                showLoadingImage(R.string.i_am_going);
                App.get(getActivity()).getPinnedDates().put(venue.getObjectId(),
                        parseObject.getDate(Backend.TIME_STAMP_COLUMN));
                updatePinPopButtons();
            }
        });
    }

    private void init(){
//        headerHeight = header.getHeight();
//        int tabLayoutHeight, tabInfoTvHeight;
//        tabLayoutHeight = header.findViewById(R.id.tabs_layout).getHeight();
//        tabInfoTvHeight = header.findViewById(R.id.tab_info_tv).getHeight();
//        visibleHeaderHeight = tabLayoutHeight + tabInfoTvHeight;
//        invisibleHeaderHeight = -headerHeight + visibleHeaderHeight;
//        viewPager.setAdapter(adapter);
//        updateTabs();
        initializeClickListeners();
    }

    private void initializeClickListeners() {
        pinBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!InternetHelper.checkInternetConnection(getActivity()))
                    ToastHelper.showErrorToast(getActivity(), R.string.check_your_internet_connection);
                else savePin();
            }
        });

        popBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!InternetHelper.checkInternetConnection(getActivity()))
                    ToastHelper.showErrorToast(getActivity(), R.string.check_your_internet_connection);
                else {
                    if (!GpsCheckDialog.isGpsEnabled(getActivity()) || googleApiClient == null
                            || !googleApiClient.isConnected())
                        GpsCheckDialog.show(getActivity());
                    else {
//                        Backend.beginAsyncOperation(getActivity(), getString(R.string.pop).toLowerCase());
                        BaseActivity.blockOrientationChange(getActivity());
                        startLocationUpdates();
                    }
                }
            }
        });

//        todayTv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                viewPager.setCurrentItem(0);
//            }
//        });
//
//        everTv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                viewPager.setCurrentItem(1);
//            }
//        });
//
//        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
//
//            @Override
//            public void onPageSelected(int position) {
//                updateTabs();
//                int scroll = (int) (headerHeight + ViewHelper.getTranslationY(header));
//                adapter.getRegisteredFragment(position).adjustScroll(scroll, visibleHeaderHeight);
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {}
//        });
    }

    private void showLoadingImage(int textResId){
        Activity activity = getActivity();
        if (activity != null) {
            Animation animation = AnimationUtils.loadAnimation(activity, R.anim.loading_circle_scale);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {}

                @Override
                public void onAnimationEnd(Animation animation) {
                    loadingImage.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {}
            });
            loadingImage.setVisibility(View.VISIBLE);
            loadingTextTv.setText(getString(textResId));
            loadingImage.startAnimation(animation);
        }
    }

    private void addPinPopToFragments(ParseObject parseObject){
        PinPop pinPop = Backend.makePinPop(parseObject, Backend.getCurrentUser(), venue);
        pinPopListFragment.addPinPop(pinPop);
//        adapter.getRegisteredFragment(PinPopListFragment.TODAY_TYPE).addPinPop(pinPop);
//        adapter.getRegisteredFragment(PinPopListFragment.EVER_TYPE).addPinPop(pinPop);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.venue_layout, null);

//        header = view.findViewById(R.id.venue_header);
        venueHeader = inflater.inflate(R.layout.venue_header, null);
        addressTv = (TextView) venueHeader.findViewById(R.id.address_tv);
        pinBt = (Button) venueHeader.findViewById(R.id.pin_bt);
        popBt = (Button) venueHeader.findViewById(R.id.pop_bt);
//        todayTv = view.findViewById(R.id.today_tv);
//        everTv = view.findViewById(R.id.ever_tv);
//        tabInfoTv = (TextView) view.findViewById(R.id.tab_info_tv);
//        viewPager = (ViewPager) view.findViewById(R.id.venue_view_pager);
        loadingImage = view.findViewById(R.id.loading_image);
        loadingTextTv = (TextView) view.findViewById(R.id.loading_text_tv);
        return view;
    }

    private void updateTabs(){
//        int darkGreyColor = getResources().getColor(R.color.dark_grey),
//                lightGreyColor = getResources().getColor(R.color.light_grey);
//        if (viewPager.getCurrentItem() == 0) {
//            todayTv.setBackgroundColor(darkGreyColor);
//            everTv.setBackgroundColor(lightGreyColor);
//            tabInfoTv.setText(getString(R.string.today_pin_pop_tab_info));
//        } else {
//            todayTv.setBackgroundColor(lightGreyColor);
//            everTv.setBackgroundColor(darkGreyColor);
//            tabInfoTv.setText(getString(R.string.ever_pin_pop_tab_info));
//        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                         int totalItemCount, int pagePosition) {
//        if (viewPager.getCurrentItem() == pagePosition) {
//            int scrollY = ScrollTabHolderFragment.getScrollY(view, headerHeight);
//            ViewHelper.setTranslationY(header, Math.max(-scrollY, invisibleHeaderHeight));
//        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        buildGoogleApiClient();
        googleApiClient.connect();
    }

    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {}
    @Override
    public void onConnectionSuspended(int i) {}
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {}

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopLocationUpdates();
        if (googleApiClient != null && googleApiClient.isConnected()) googleApiClient.disconnect();
    }

    private void startLocationUpdates() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(LOCATION_UPDATES_INTERVAL);
        mLocationRequest.setFastestInterval(LOCATION_UPDATES_FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, mLocationRequest,
                this);
    }

    private void stopLocationUpdates() {
        if (googleApiClient != null && googleApiClient.isConnected())
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            if (location.distanceTo(venue.getLocation()) > DISTANCE_FOR_POP) {
                ToastHelper.showErrorToast(getActivity(), R.string.you_must_be_inside_venue);
                Backend.finishAsyncOperation(getActivity(), null);
            } else {
                Backend.savePinPop(venue, getActivity(), Backend.POP_TYPE, new Backend.
                        OnSuccessResponseListener(getActivity()) {
                    @Override
                    public void onSuccess(ParseObject parseObject) {
                        addPinPopToFragments(parseObject);
                        showLoadingImage(R.string.i_am_here);
                        App.get(getActivity()).getPoppedDates().put(venue.getObjectId(),
                                parseObject.getDate(Backend.TIME_STAMP_COLUMN));
                    }
                });
            }
            stopLocationUpdates();
        }
    }

    private void updatePinPopButtons() {
        Date pinnedDate = App.get(getActivity()).getPinnedDates().get(venue.getObjectId());
        if (pinnedDate != null) {
            if (new Date().getTime() > pinnedDate.getTime() + MAKE_PIN_POP_INTERVAL) {
                enableButton(pinBt);
                App.get(getActivity()).getPinnedDates().remove(venue.getObjectId());
            } else disableButton(pinBt);
        } else enableButton(pinBt);

        Date poppedDate = App.get(getActivity()).getPoppedDates().get(venue.getObjectId());
        if (poppedDate != null) {
            if (new Date().getTime() > poppedDate.getTime() + MAKE_PIN_POP_INTERVAL) {
                enableButton(popBt);
                App.get(getActivity()).getPoppedDates().remove(venue.getObjectId());
            } else disableButton(popBt);
        } else enableButton(popBt);
    }

    private void enableButton(Button button) {
        button.setBackgroundResource(R.drawable.green_button);
        button.setEnabled(true);
    }

    private void disableButton(Button button) {
        button.setBackgroundResource(R.drawable.grey_button);
        button.setEnabled(false);
    }

    Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            updatePinPopButtons();
            timerHandler.postDelayed(this, CHECK_PIN_POP_BUTTON_INTERVAL);
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        timerHandler.post(timerRunnable);
    }

    @Override
    public void onPause() {
        super.onPause();
        timerHandler.removeCallbacks(timerRunnable);
    }

    public View getVenueHeader() {
        return venueHeader;
    }
}
