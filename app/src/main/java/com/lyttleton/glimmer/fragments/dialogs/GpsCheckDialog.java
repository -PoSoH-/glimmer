package com.lyttleton.glimmer.fragments.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.lyttleton.glimmer.R;

public class GpsCheckDialog extends DialogFragment {

    private GpsDialogListener gpsDialogListener;

    public static interface GpsDialogListener {
        void onCancelGpsDialog();
        void onGoToGpsSettings();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        gpsDialogListener = (GpsDialogListener) activity;
    }

    public static boolean isGpsEnabled(Context context){
        boolean /*gpsProviderEnabled = ((LocationManager) context.getSystemService(Context.
                LOCATION_SERVICE)).isProviderEnabled(LocationManager.GPS_PROVIDER),*/
        networkProviderEnabled = ((LocationManager) context.getSystemService(Context.
                LOCATION_SERVICE)).isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        return /*gpsProviderEnabled || */networkProviderEnabled;
    }

    public static void show(FragmentActivity fragmentActivity) {
        if (fragmentActivity.getSupportFragmentManager().
                findFragmentByTag(GpsCheckDialog.class.getSimpleName()) == null) {
            GpsCheckDialog dialog = new GpsCheckDialog();
            dialog.setCancelable(false);
            FragmentTransaction ft = fragmentActivity.getSupportFragmentManager().beginTransaction();
            ft.add(dialog, GpsCheckDialog.class.getSimpleName());
            ft.commitAllowingStateLoss();
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());
        final View view = getActivity().getLayoutInflater().inflate(R.layout.
                        cancel_or_accept_dialog_layout, null);

        ((TextView)view.findViewById(R.id.dialog_title_tv)).setText(R.string.gps);
        TextView dialogMessageTv = (TextView) view.findViewById(R.id.dialog_message_tv);
        dialogMessageTv.setText(R.string.enable_gps);
        dialogMessageTv.setVisibility(View.VISIBLE);

        view.findViewById(R.id.cancel_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gpsDialogListener.onCancelGpsDialog();
                getDialog().dismiss();
            }
        });

        Button acceptBt = (Button) view.findViewById(R.id.accept_bt);
        acceptBt.setText(R.string.go_to_settings);
        acceptBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gpsDialogListener.onGoToGpsSettings();
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                getDialog().dismiss();
            }
        });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(view);

        return dialog;
    }
}
