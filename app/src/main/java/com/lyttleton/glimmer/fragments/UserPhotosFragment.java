package com.lyttleton.glimmer.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.activities.WinkActivity;
import com.lyttleton.glimmer.adapters.User;
import com.lyttleton.glimmer.adapters.UserPhotosViewPagerAdapter;
import com.lyttleton.glimmer.helpers.InternetHelper;
import com.lyttleton.glimmer.loaders.Backend;
import com.parse.ParseObject;

import java.util.ArrayList;

public class UserPhotosFragment extends Fragment {

    public static final String USER_ARG = "USER_ARG";
    private ViewPager viewPager;
    private ViewGroup indicatorLayout;
    private ImageView photoIndicators[];
    private User user;
    private UserPhotosViewPagerAdapter adapter;

    private View mutualFriendsIv; //, toolbarProgressBar;
    private TextView mutualFriendsTv;

    public static Fragment create(User user){
        Fragment fragment = new UserPhotosFragment();
        Bundle args = new Bundle();
        args.putSerializable(USER_ARG, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        adapter = new UserPhotosViewPagerAdapter(getChildFragmentManager(), user.getPhotos());
        viewPager.setAdapter(adapter);
        initializePhotoIndicators();
        updateCurrentPhotoIndicator();
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                updateCurrentPhotoIndicator();
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });

        if (!Backend.getCurrentUser().getObjectId().equals(user.getObjectId())
                && InternetHelper.checkInternetConnection(getActivity().getApplicationContext())) {
            loadMutualFriendsNumber();
            checkIfUserMatched();
//            toolbarProgressBar.setVisibility(View.VISIBLE);
        }
    }

    private void checkIfUserMatched() {
        if (!user.isMatched()) {
            Backend.isUserMatched(user, getActivity(), new Backend.ResponseListener() {
                @Override
                public void onSuccess(ParseObject parseObject) {
                    if (parseObject != null) {
                        user.setMatched(true);
                        ((WinkActivity) getActivity()).setSelectedUserMatched(user);
                    }
                }

                @Override
                public void onError(String error, int errorCode) {}
            });
        }
    }

    private void loadMutualFriendsNumber() {
//        Backend.getMutualFriendsCount(user, getActivity(), new Backend.ResponseListener() {
//            @Override
//            public void onSuccess(ParseObject parseObject) {
//                mutualFriendsIv.setVisibility(View.VISIBLE);
//                mutualFriendsTv.setVisibility(View.VISIBLE);
//                mutualFriendsTv.setText(String.valueOf(parseObject.getInt(Backend.
//                        MUTUAL_FRIENDS_COUNT)));
//                toolbarProgressBar.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onError(String error, int errorCode) {
//                toolbarProgressBar.setVisibility(View.GONE);
//            }
//        });
    }

    private void initializePhotoIndicators() {
        indicatorLayout.removeAllViews();
        photoIndicators = new ImageView[user.getPhotos().size()];
        ImageView imageView;
        LinearLayout.LayoutParams layoutParams;
        int indicatorSize = (int) getResources().getDimension(R.dimen.photo_indicator_size),
                indicatorMargin = (int) getResources().getDimension(R.dimen.photo_indicator_margin);
        for (int i = 0; i < photoIndicators.length; i++) {
            imageView = new ImageView(getActivity());
            layoutParams = new LinearLayout.LayoutParams(indicatorSize, indicatorSize);
            layoutParams.setMargins(indicatorMargin, indicatorMargin, indicatorMargin,
                    indicatorMargin);
            imageView.setLayoutParams(layoutParams);
            imageView.setPadding(indicatorMargin, indicatorMargin, indicatorMargin, indicatorMargin);
            imageView.setImageResource(R.drawable.grey_circle);
            indicatorLayout.addView(imageView);
            final int finalI = i;
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewPager.setCurrentItem(finalI);
                }
            });
            photoIndicators[i] = imageView;
        }
    }

    private void updateCurrentPhotoIndicator() {
        for (int i = 0; i < photoIndicators.length; i++){
            if (i == viewPager.getCurrentItem())
                photoIndicators[i].setImageResource(R.drawable.purple_circle);
            else photoIndicators[i].setImageResource(R.drawable.grey_circle);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_photos_layout, null);
        viewPager = (ViewPager) view.findViewById(R.id.user_photos_view_pager);
        indicatorLayout = (ViewGroup) view.findViewById(R.id.photo_indicator_layout);

        View backBt = view.findViewById(R.id.back_bt);
        backBt.setVisibility(View.VISIBLE);
        backBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        user = (User) getArguments().getSerializable(USER_ARG);
        TextView labelTv = (TextView) view.findViewById(R.id.label_tv);
        labelTv.setVisibility(View.VISIBLE);
        labelTv.setText(user.getFirstName());

        mutualFriendsIv = view.findViewById(R.id.mutual_friends_iv);
        mutualFriendsTv = (TextView) view.findViewById(R.id.mutual_friends_tv);
//        toolbarProgressBar = view.findViewById(R.id.toolbar_progress_bar);

        return view;
    }

    public void updatePhotos(ArrayList<String> photos){
        user.setPhotos(photos);
        adapter.setImageUrls(photos);
        adapter.notifyDataSetChanged();
        initializePhotoIndicators();
        updateCurrentPhotoIndicator();
    }

    public int getCurrentPhotoIndex() {
        return viewPager.getCurrentItem();
    }

    public void setCurrentPhoto(int index) {
        viewPager.setCurrentItem(index);
    }
}
