package com.lyttleton.glimmer.fragments;

public interface ListPaging {
    void onGetLastElements();
}
