package com.lyttleton.glimmer.fragments;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.activities.MainActivity;
import com.lyttleton.glimmer.activities.VenueActivity;
import com.lyttleton.glimmer.adapters.Venue;
import com.lyttleton.glimmer.adapters.VenuesAdapter;
import com.lyttleton.glimmer.fragments.dialogs.RequestVenueDialog;
import com.lyttleton.glimmer.helpers.InternetHelper;
import com.lyttleton.glimmer.helpers.ToastHelper;
import com.lyttleton.glimmer.loaders.VenueLoader;

import java.util.List;

public class VenueSearchFragment extends Fragment
        implements LoaderManager.LoaderCallbacks<List<Venue>>, AdapterView.OnItemClickListener,
        ListPaging {

    public static final int LOADER_ID = 1;
    private EditText venueNameEt;
    private View searchBt;
    private ListView listView;
    private VenuesAdapter adapter;
    private boolean searchButtonClicked;

    // Paging
    private List<Venue> previousItems;
    private boolean hasNextPage, loadingStarted;
    private View loadingFooter;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        adapter = new VenuesAdapter(getActivity(), this);
        listView.addFooterView(loadingFooter);
        listView.setAdapter(adapter);
        listView.removeFooterView(loadingFooter);

        initializeClickListeners();
    }

    private void initializeClickListeners() {
        searchBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String venueName = venueNameEt.getText().toString();
                if (!TextUtils.isEmpty(venueName)) {
                    if (!InternetHelper.checkInternetConnection(getActivity()))
                        ToastHelper.showErrorToast(getActivity(), R.string.check_your_internet_connection);
                    else {
                        adapter.clear();
                        previousItems = null;
                        hasNextPage = false;
                        getLoaderManager().restartLoader(LOADER_ID, null, VenueSearchFragment.this);
                        InputMethodManager imm = (InputMethodManager) getActivity().
                                getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(venueNameEt.getWindowToken(), 0);
                        searchButtonClicked = true;
                    }
                }
            }
        });

        listView.setOnItemClickListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.venue_search_layout, null);
        listView = (ListView) view.findViewById(R.id.venue_search_lv);
        View header = inflater.inflate(R.layout.venue_search_header, null);
        venueNameEt = (EditText) header.findViewById(R.id.venue_name_et);
        searchBt = header.findViewById(R.id.search_bt);
        listView.addHeaderView(header);
        loadingFooter = inflater.inflate(R.layout.loading_footer, null);
        return view;
    }

    @Override
    public Loader<List<Venue>> onCreateLoader(int id, Bundle args) {
        loadingStarted = true;
        listView.removeFooterView(loadingFooter);
        listView.addFooterView(loadingFooter);
        return new VenueLoader(getActivity(), venueNameEt.getText().toString(), true, previousItems);
    }

    @Override
    public void onLoadFinished(Loader<List<Venue>> loader, List<Venue> data) {
        loadingStarted = false;
        listView.removeFooterView(loadingFooter);
        if (data != null) {
            adapter.setData(data, ((MainActivity)getActivity()).getLastUserLocation());
            if (data.size() > 0 && searchButtonClicked) listView.setSelection(1);
        }

        if (data == null || data.size() == 0 && searchButtonClicked)
            RequestVenueDialog.show(getActivity(), venueNameEt.getText().toString());
        searchButtonClicked = false;

        VenueLoader venueLoader = (VenueLoader) loader;
        hasNextPage = venueLoader.hasNextPage();
        previousItems = data;
    }

    @Override
    public void onLoaderReset(Loader<List<Venue>> loader) {}

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        int itemIndex = position - 1; // First item is header
        if (itemIndex < adapter.getCount())
            VenueActivity.start(getActivity(), adapter.getItem(itemIndex));
    }

    public void updateDistanceToVenues(Location userLocation){
        adapter.setUserLocation(userLocation);
        adapter.notifyDataSetChanged();
    }

    public void onVenuesLoaded(List<Venue> venues) {
        if (!TextUtils.isEmpty(venueNameEt.getText()))
            getLoaderManager().restartLoader(LOADER_ID, null, this);
//        if (venues != null) {
//            final String venueName = venueNameEt.getText().toString();
//            if (!TextUtils.isEmpty(venueName)) {
//                List<Venue> searchVenues = new ArrayList<>();
//                for (Venue venue : venues)
//                    if (venue.getName().matches("(?i).*" + venueName + ".*"))
//                        searchVenues.add(venue);
//                adapter.setData(searchVenues, ((MainActivity) getActivity()).getLastUserLocation());
//            }
//        }
    }

    @Override
    public void onGetLastElements() {
        if (hasNextPage && !loadingStarted) getLoaderManager().restartLoader(0, null, this);
    }
}
