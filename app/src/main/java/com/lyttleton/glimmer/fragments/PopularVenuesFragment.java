package com.lyttleton.glimmer.fragments;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.lyttleton.glimmer.DataAccessObject.VenueORM;
import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.activities.MainActivity;
import com.lyttleton.glimmer.activities.VenueActivity;
import com.lyttleton.glimmer.adapters.Venue;
import com.lyttleton.glimmer.adapters.VenuesAdapter;
//import com.lyttleton.glimmer.helpers.HelpersORM;
//import com.lyttleton.glimmer.helpers.DAO;
import com.lyttleton.glimmer.helpers.InternetHelper;
import com.lyttleton.glimmer.loaders.VenueLoader;

import java.util.List;

public class PopularVenuesFragment extends Fragment implements AdapterView.OnItemClickListener,
        ListPaging {

    private ListView listView;
    private VenuesAdapter adapter;
//    private View progressBar;
    private View noInternetLayout;
    private View loadingFooter;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        adapter = new VenuesAdapter(getActivity().getApplicationContext(), this);
        listView.addFooterView(loadingFooter);
        listView.setAdapter(adapter);
        listView.removeFooterView(loadingFooter);
        listView.setOnItemClickListener(this);
        List<Venue> v = VenueORM.loadAllVenues(); //HelpersORM.getAllVenues();
        test(v, "onCreate popularVenuesFragment");
        adapter.setData(v, ((MainActivity) getActivity()).getLastUserLocation());
    }

    public static void test(List<Venue> venue, String name){
        if(venue.size()==0){
            Log.d("glimmer", "test view..." + "list is empty..." + name);
        }else {
            for (Venue v : venue) {
                Log.d("glimmer", "test view..." + v.getName() + "..." + name);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.popular_venues_layout, null);
        listView = (ListView) view.findViewById(R.id.popular_venues_lv);
//        progressBar = view.findViewById(R.id.progress_bar);

        noInternetLayout = view.findViewById(R.id.no_internet_layout);
        noInternetLayout.findViewById(R.id.go_to_settings_bt).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        InternetHelper.openWifiSettings(getActivity());
                    }
                });

        loadingFooter = inflater.inflate(R.layout.loading_footer, null);

        return view;
    }

    public void onVenuesLoaded(List<Venue> venues, VenueLoader loader){
        if (venues != null) {
            adapter.setData(venues, ((MainActivity)getActivity()).getLastUserLocation());
        }
        else {
            adapter.clear();
            adapter.setData(VenueORM.loadAllVenues()/*HelpersORM.getAllVenues()*/, ((MainActivity)getActivity()).getLastUserLocation());
        }

        noInternetLayout.setVisibility(View.GONE);
        if ((venues == null || venues.size() == 0) && adapter.getCount() == 0 && loader.isNoInternet())
            noInternetLayout.setVisibility(View.VISIBLE);

//        progressBar.setVisibility(View.GONE);
        listView.removeFooterView(loadingFooter);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        VenueActivity.start(getActivity(), adapter.getItem(position));
    }

    public void updateDistanceToVenues(Location userLocation){
        adapter.setUserLocation(userLocation);
        adapter.notifyDataSetChanged();
    }

    public void showLoadingFooter(){
        listView.addFooterView(loadingFooter);
    }

    public void onGetLastElements() {
        DiscoverFragment fragment = ((MainActivity)getActivity()).getDiscoverFragment();
        if (fragment != null) fragment.onGetLastElements();
    }
}
