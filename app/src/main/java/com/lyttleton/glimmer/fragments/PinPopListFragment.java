package com.lyttleton.glimmer.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lyttleton.glimmer.DataAccessObject.PinPopORM;
import com.lyttleton.glimmer.R;
import com.lyttleton.glimmer.activities.MainActivity;
import com.lyttleton.glimmer.activities.PinPopListActivity;
import com.lyttleton.glimmer.activities.VenueActivity;
import com.lyttleton.glimmer.activities.WinkActivity;
import com.lyttleton.glimmer.adapters.PinPop;
import com.lyttleton.glimmer.adapters.PinPopAdapter;
import com.lyttleton.glimmer.adapters.User;
import com.lyttleton.glimmer.adapters.Venue;
import com.lyttleton.glimmer.adapters.Wink;
import com.lyttleton.glimmer.fragments.scroll_tab_holder.AdjustScrollListener;
import com.lyttleton.glimmer.fragments.scroll_tab_holder.ScrollChangeListener;
import com.lyttleton.glimmer.helpers.InternetHelper;
import com.lyttleton.glimmer.helpers.ViewHelper;
import com.lyttleton.glimmer.loaders.Backend;
import com.lyttleton.glimmer.loaders.PinPopLoader;
import com.lyttleton.glimmer.services.PushReceiverService;

import java.util.ArrayList;
import java.util.List;

public class PinPopListFragment extends Fragment
        implements LoaderManager.LoaderCallbacks<List<PinPop>>, AbsListView.OnScrollListener,
        AdjustScrollListener, AdapterView.OnItemClickListener {

    public static final String LOAD_TYPE_ARG = "LOAD_TYPE_ARG";
    public static final String VENUE_ARG = "VENUE_ARG";
    public static final String FRAGMENT_TYPE_ARG = "FRAGMENT_TYPE_ARG";
    public static final String PIN_POP_ARG = "PIN_POP_ARG";
    public static final String NEW_PIN_POP_BROADCAST_ACTION = "com.lyttleton.glimmer.NEW_PIN_POP";
    public static int TODAY_TYPE = 0, EVER_TYPE = 1;  // Old
    public static int ALL_DATA_TYPE = 2;

    public static final int PIN_FRAGMENT_TYPE = 0;
    public static final int POP_FRAGMENT_TYPE = 1;
    public static final int VENUE_FRAGMENT_TYPE = 2;
    public static final int VENUE_FRAGMENT_PAGER_TYPE = 3; // Old

    private ListView listView;
    private PinPopAdapter adapter;
    private int loadType, fragmentType;
    private View tabInfo;
//    private View progressBarLayout, progressBar;
    private View noPinPopsLayout, noInternetLayout;

    private Venue venue;

    // Paging
    private List<PinPop> previousItems; // = PinPopORM.loadAllPinPops();
    private boolean hasNextPage, loadingStarted;
//    private View loadingFooter;
    private int loadingStartOffset;

    public static PinPopListFragment create(int loadType, Venue venue){
        PinPopListFragment fragment = new PinPopListFragment();
        Bundle args = new Bundle();
        args.putInt(LOAD_TYPE_ARG, loadType);
        args.putSerializable(VENUE_ARG, venue);
        fragment.setArguments(args);
        return fragment;
    }

    public static PinPopListFragment create(int fragmentType) {
        PinPopListFragment fragment = new PinPopListFragment();
        Bundle args = new Bundle();
        args.putInt(FRAGMENT_TYPE_ARG, fragmentType);
        fragment.setArguments(args);
        return fragment;
    }

    private BroadcastReceiver pinPopBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            PinPop pinPop = (PinPop) intent.getSerializableExtra(PIN_POP_ARG);
            if (fragmentType == POP_FRAGMENT_TYPE && !pinPop.isPin()) {
                boolean exists = false;
                for (int i = 0; i < adapter.getCount(); i++)
                    if (adapter.getItem(i).getObjectId().equals(pinPop.getObjectId()))
                        exists = true;

                if (!exists) {
                    adapter.insert(pinPop, 0);
                    if (previousItems != null) previousItems.add(0, pinPop);
                }
            }
        }
    };

    private BroadcastReceiver matchBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final User user = (User) intent.getSerializableExtra(PushReceiverService.USER_ARG);
            final int type = intent.getIntExtra(Backend.TYPE_COLUMN, 0);
            if (type == Wink.MATCH_TYPE && user != null){
                for (int i = 0; i < adapter.getCount(); i++)
                    if (adapter.getItem(i).getUser().getObjectId().equals(user.getObjectId()))
                        adapter.getItem(i).getUser().setMatched(true);
            }
        }
    };

    public static void sendPinPopBroadcast(PinPop pinPop, Context context){
        Intent intent = new Intent(NEW_PIN_POP_BROADCAST_ACTION);
        intent.putExtra(PIN_POP_ARG, pinPop);
        context.sendBroadcast(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().registerReceiver(pinPopBroadcastReceiver,
                new IntentFilter(NEW_PIN_POP_BROADCAST_ACTION));
        getActivity().registerReceiver(matchBroadcastReceiver,
                new IntentFilter(Backend.PUSH_BROADCAST_ACTION));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(pinPopBroadcastReceiver);
        getActivity().unregisterReceiver(matchBroadcastReceiver);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        venue = (Venue) getArguments().getSerializable(VENUE_ARG);
        if(venue != null) previousItems = PinPopORM.loadPinPopFromDataBase(venue.getObjectId());
        final View venueHeader = getActivity().findViewById(R.id.venue_header);
        if (fragmentType == VENUE_FRAGMENT_PAGER_TYPE && venueHeader.getHeight() == 0
                && venueHeader.getViewTreeObserver().isAlive())
            venueHeader.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.
                    OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    ViewHelper.removeOnGlobalLayoutListener(venueHeader, this);
                    init(venueHeader);
                }
            });
        else init(venueHeader);
    }

    private void init(View venueHeader){
        if (fragmentType == VENUE_FRAGMENT_PAGER_TYPE) {
            tabInfo.setPadding(0, venueHeader.getHeight(), 0, 0);
            listView.addHeaderView(tabInfo);
            listView.setOnScrollListener(this);
//            progressBarLayout.setPadding(0, venueHeader.getHeight(), 0, 0);

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.
                    LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);

            noInternetLayout.setLayoutParams(layoutParams);
            int padding = (int) getResources().getDimension(R.dimen.info_list_layout_padding);
            noInternetLayout.setPadding(padding, venueHeader.getHeight() + padding, padding, padding);
        }

        adapter = new PinPopAdapter(getActivity(), fragmentType, this);
//        listView.addFooterView(loadingFooter);
        listView.setAdapter(adapter);
//        listView.removeFooterView(loadingFooter);
        listView.setOnItemClickListener(this);
        getLoaderManager().initLoader(0, null, PinPopListFragment.this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pin_pop_list_layout, null);
        loadType = getArguments().getInt(LOAD_TYPE_ARG);

        listView = (ListView) view.findViewById(R.id.pin_pop_lv);
        if (loadType == PinPopListFragment.ALL_DATA_TYPE && getParentFragment() instanceof VenueFragment)
            listView.addHeaderView(((VenueFragment) getParentFragment()).getVenueHeader());

//        loadingFooter = inflater.inflate(R.layout.loading_footer, null);
//        progressBarLayout = view.findViewById(R.id.progress_bar_layout);
//        progressBar = view.findViewById(R.id.progress_bar);
        noPinPopsLayout = view.findViewById(R.id.no_pin_pops_layout);
        TextView noPinPopsTv = (TextView) view.findViewById(R.id.no_pin_pops_tv);

        noInternetLayout = view.findViewById(R.id.no_internet_layout);
        noInternetLayout.findViewById(R.id.go_to_settings_bt).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        InternetHelper.openWifiSettings(getActivity());
                    }
                });

//        fragmentType = getArguments().getInt(FRAGMENT_TYPE_ARG, VENUE_FRAGMENT_PAGER_TYPE);
        fragmentType = getArguments().getInt(FRAGMENT_TYPE_ARG, VENUE_FRAGMENT_TYPE);
        switch (fragmentType){
            case PIN_FRAGMENT_TYPE:
                noPinPopsTv.setText(R.string.no_pins);
                break;
            case POP_FRAGMENT_TYPE:
                noPinPopsTv.setText(R.string.no_pops);
                break;
        }

        View startMingleBt = view.findViewById(R.id.start_mingle_bt);
        startMingleBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity activity = getActivity();
                if (activity instanceof MainActivity)
                    ((MainActivity) getActivity()).openVenues();
                else if (activity instanceof PinPopListActivity) {
                    activity.onBackPressed();
                    MainActivity.openVenuesWithBroadcast(getActivity());
                }
            }
        });

        tabInfo = inflater.inflate(R.layout.tab_info_layout, listView, false);

        return view;
    }

    @Override
    public Loader<List<PinPop>> onCreateLoader(int id, Bundle args) {
        loadingStarted = true;
        if (previousItems == null || previousItems.size() == 0) {
            listView.setVisibility(View.GONE);
//            progressBar.setVisibility(View.VISIBLE);
        }
        return new PinPopLoader(getActivity(), loadType, fragmentType, venue, previousItems,
                loadingStartOffset);
    }

    @Override
    public void onLoadFinished(Loader<List<PinPop>> loader, List<PinPop> data) {
        loadingStarted = false;
        listView.setVisibility(View.VISIBLE);
//        listView.removeFooterView(loadingFooter);
        if (data != null){
//            LinkedList<PinPop> pin = new LinkedList<>();
//            for(PinPop pop : data) {
//                if (venue.getObjectId().equals(pop.getVenue().getObjectId())) {
//                    pin.add(pop);
//                }
//            }
            adapter.setData(data);
        }
        if (needAdjustScroll) adjustScroll(adjustScrollHeight, visibleHeaderHeight);

        PinPopLoader pinPopLoader = ((PinPopLoader)loader);
        hasNextPage = pinPopLoader.hasNextPage();
        previousItems = data;
        loadingStartOffset = pinPopLoader.getLoadingStartOffset();

        noPinPopsLayout.setVisibility(View.GONE);
        noInternetLayout.setVisibility(View.GONE);
        if ((data == null || data.size() == 0) && adapter.getCount() == 0) {
            if (!pinPopLoader.isNoInternet() && (fragmentType == PIN_FRAGMENT_TYPE
                    || fragmentType == POP_FRAGMENT_TYPE))
                noPinPopsLayout.setVisibility(View.VISIBLE);
            else if (pinPopLoader.isNoInternet()) noInternetLayout.setVisibility(View.VISIBLE);
        }

        // If list is empty and has next elements load them
        if (adapter.getCount() == 0 && (data == null || data.size() == 0 && hasNextPage))
            getLoaderManager().restartLoader(0, null, this);
//        else progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onLoaderReset(Loader<List<PinPop>> loader) {}

    private boolean needAdjustScroll;
    private int adjustScrollHeight, visibleHeaderHeight;

    @Override
    public void adjustScroll(int scrollHeight, int visibleHeaderHeight) {
        if (fragmentType != VENUE_FRAGMENT_PAGER_TYPE) return;
        if (scrollHeight == visibleHeaderHeight && listView.getFirstVisiblePosition() >= 1) return;
        if (hasItems()) {
            needAdjustScroll = false;
            listView.setSelectionFromTop(1, scrollHeight);
        } else {
            needAdjustScroll = true;
            adjustScrollHeight = scrollHeight;
            this.visibleHeaderHeight = visibleHeaderHeight;
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {}

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                         int totalItemCount) {
        if (hasItems())
            ((ScrollChangeListener)getActivity()).onScroll(view, firstVisibleItem, visibleItemCount,
                    totalItemCount, loadType);
    }

    private boolean hasItems(){
        return listView != null && listView.getCount() > 1;
    }

    public void addPinPop(PinPop pinPop){
        if (pinPop.getUser().getGender() != null &&
                !pinPop.getUser().getGender().equals(Backend.getUserGenderInterestConverted())) return;
        PinPop adapterPinPop;
        for (int i = 0; i < adapter.getCount(); i++) {
            adapterPinPop = adapter.getItem(i);
            if (adapterPinPop.getUser().getObjectId().equals(pinPop.getUser().getObjectId())) {
                adapter.remove(adapterPinPop);
                break;
            }
        }
        adapter.insert(pinPop, 0);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        int clickIndex = position;
        if (fragmentType == VENUE_FRAGMENT_TYPE
                || fragmentType == VENUE_FRAGMENT_PAGER_TYPE) clickIndex --; // First item is header
        if (clickIndex < 0 || clickIndex >= adapter.getCount()) return;

        if (fragmentType == VENUE_FRAGMENT_TYPE) {
            // Delete duplicated users in Wink activity
//            ArrayList<User> users = new ArrayList<>();
//            User user;
////            int selectedItemIndex = position - 1; // First item is header
//            for (int i = 0; i < adapter.getCount(); i++) {
//                user = adapter.getItem(i).getUser();
//                if (!isUserExists(users, user)) users.add(user);
//            }
//
//            int selectedItemIndex = 0;
//            for (int i = 0; i < users.size(); i++)
//                if (users.get(i).getObjectId().equals(adapter.getItem(position).getUser().getObjectId()))
//                    selectedItemIndex = i;
//
//            if (selectedItemIndex >= 0)
//                WinkActivity.start(getActivity(), users, selectedItemIndex,
//                        adapter.getItem(selectedItemIndex).getVenue());

            ArrayList<User> users = new ArrayList<>();
            User user;
            for (int i = 0; i < adapter.getCount(); i++) {
                user = adapter.getItem(i).getUser();
                users.add(user);
            }

            WinkActivity.start(getActivity(), users, clickIndex, adapter.getItem(clickIndex).getVenue());
        } else VenueActivity.start(getActivity(), adapter.getItem(clickIndex).getVenue());
    }

    public void onGetLastElements() {
        if (hasNextPage && !loadingStarted) getLoaderManager().restartLoader(0, null, this);
    }

    private boolean isUserExists(ArrayList<User> users, User user){
        for (User userEl : users)
            if (userEl.getObjectId().equals(user.getObjectId())) return true;
        return false;
    }
}
