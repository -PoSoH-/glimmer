package com.lyttleton.glimmer;

import android.app.Activity;
import android.content.Context;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.activeandroid.ActiveAndroid;
import com.lyttleton.glimmer.loaders.Backend;
//import com.orm.SugarApp;
//import com.orm.SugarContext;

import java.util.Date;
import java.util.HashMap;

public class App extends /*SugarApp { //*/com.activeandroid.app.Application {

    public final static boolean logger = BuildConfig.DEBUG;
    private HashMap<String, Date> pinnedDates = new HashMap<>(), poppedDates = new HashMap<>();

    @Override
    public void onCreate() {
        super.onCreate();
        Backend.initialize(this);
        ActiveAndroid.initialize(getApplicationContext());
//        SugarContext.init(getApplicationContext());
    }

    public HashMap<String, Date> getPinnedDates() {
        return pinnedDates;
    }

    public HashMap<String, Date> getPoppedDates() {
        return poppedDates;
    }

    public static App get(Activity activity) {
        return (App) activity.getApplication();
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View currentFocus = activity.getCurrentFocus();
        if (currentFocus != null) {
            IBinder iBinder = currentFocus.getWindowToken();
            if (iBinder != null) inputMethodManager.hideSoftInputFromWindow(iBinder, 0);
        }
    }

    public static void logOut(String places, String info){
        if(App.logger) {
            String builder = ".." +
                    places +
                    ".." +
                    info +
                    "..";
            Log.d("glimmer", builder);
        }
    }

}
